#include <ros/ros.h>
#include <turtlebot_node/TurtlebotSensorState.h>
#include <geometry_msgs/Twist.h>

class Governator
{
  ros::NodeHandle nh_;
  ros::Publisher pub_;
  ros::Subscriber sub_;
  ros::Subscriber sub_sensor_;
  unsigned char breaker;

public:
  Governator(){
    pub_ = nh_.advertise<geometry_msgs::Twist>("/turtlebot_node/cmd_vel",50);
    sub_sensor_ = nh_.subscribe("/turtlebot_node/sensor_state", 10, &Governator::StoreSensor, this);
    sub_ = nh_.subscribe("cmd_vel",50,&Governator::CallBack, this);
  }

  void StoreSensor(turtlebot_node::TurtlebotSensorState msg){
    breaker = msg.bumps_wheeldrops;
  }
 
  void CallBack(geometry_msgs::Twist msg){
    geometry_msgs::Twist result;
    if (breaker){
      result.linear.x=0.0;
      result.linear.y=0.0;
      result.linear.z=0.0;
      result.angular.x=0.0;
      result.angular.y=0.0;
      result.angular.z=0.0;
    }
    else {
      result = msg;
    }
    pub_.publish(result);
  }

};   


int main(int argc, char** argv)
{
  ros::init(argc, argv, "Governator");
  Governator gv;
  ros::spin();
  return 0;
}

  
