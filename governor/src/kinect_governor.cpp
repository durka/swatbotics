#include <ros/ros.h>
#include <turtlebot_node/TurtlebotSensorState.h>
#include <geometry_msgs/Twist.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>

typedef pcl::PointCloud<pcl::PointXYZ> PointCLoud;

class Governator
{
  ros::NodeHandle nh_;
  ros::Publisher pub_;
  ros::Subscriber sub_;
  ros::Subscriber sub_sensor_;
  ros::Subscriber sub_kinect_;
  unsigned char breaker;
  bool kinect_breaker;

public:
  Governator(){
    pub_ = nh_.advertise<geometry_msgs::Twist>("/turtlebot_node/cmd_vel",50);
    sub_sensor_ = nh_.subscribe("/turtlebot_node/sensor_state", 10, &Governator::StoreSensor, this);
    sub_kinect_ = nh_.subscribe("/camera/rgb/points",1,&Governator::StoreKinect,this);
    sub_ = nh_.subscribe("cmd_vel",50,&Governator::CallBack, this);
  }

  void StoreSensor(turtlebot_node::TurtlebotSensorState msg){
    breaker = msg.bumps_wheeldrops;
  }

  void StoreKinect(const PointCLoud::ConstPtr& msg){
    unsigned int count_nan = 0;
    unsigned int count_close = 0;
    for (unsigned int i=0; i<(msg->height)*(msg->width); i++){
      const pcl::PointXYZ pt = msg->points[i];
      if (isnan(pt.z)){
        count_nan++;
      }
      else if (pt.z<1.0 && pt.y<0.3){
        count_close++;
      }
    }
    if (count_nan<90000){count_nan=90000;}
    kinect_breaker = ((count_nan+count_close)>100000);
    ROS_INFO("%d nan, %d close, add up to %d",count_nan, count_close, (count_nan+count_close));
  }
    
 
  void CallBack(geometry_msgs::Twist msg){
    geometry_msgs::Twist result;
    if (breaker){
      result.linear.x=0.0;
      result.linear.y=0.0;
      result.linear.z=0.0;
      result.angular.x=0.0;
      result.angular.y=0.0;
      result.angular.z=0.0;
    }
    else {
      result = msg;
      if (kinect_breaker && msg.linear.x>0.1){
        result.linear.x = 0.1;
      }
    }
    pub_.publish(result);
  }

};   


int main(int argc, char** argv)
{
  ros::init(argc, argv, "Governator");
  Governator gv;
  ros::spin();
  return 0;
}

  
