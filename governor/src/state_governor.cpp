#include <ros/ros.h>
#include <turtlebot_node/TurtlebotSensorState.h>
#include <geometry_msgs/Twist.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <opencv2/opencv.hpp>

typedef pcl::PointCloud<pcl::PointXYZ> PointCLoud;

enum {
  UNKNOWN=0,
  CLOSE=127,
  FAR=255};

class Governator
{
  ros::NodeHandle nh_;
  ros::Publisher pub_;
  ros::Subscriber sub_;
  ros::Subscriber sub_sensor_;
  ros::Subscriber sub_kinect_;
  unsigned char breaker;
  cv::Mat kState;
  bool slow;

public:
  Governator(){
    pub_ = nh_.advertise<geometry_msgs::Twist>("/turtlebot_node/cmd_vel",50);
    sub_sensor_ = nh_.subscribe("/turtlebot_node/sensor_state", 10, &Governator::StoreSensor, this);
    sub_kinect_ = nh_.subscribe("/camera/rgb/points",1,&Governator::StoreKinect,this);
    sub_ = nh_.subscribe("cmd_vel",50,&Governator::CallBack, this);
    slow = false;
  }

  void StoreSensor(turtlebot_node::TurtlebotSensorState msg){
    breaker = msg.bumps_wheeldrops;
  }

  void StoreKinect(const PointCLoud::ConstPtr& msg){
    if (kState.empty()){
      kState = cv::Mat::zeros(msg->height, msg->width, CV_8UC1);
    }

    double ratio;
    if (nh_.getParam("slow_thresh",ratio)){}
    else { 
      nh_.setParam("slow_thresh",0.3);
    }

    int offset = 0;
    for (uint32_t y=0; y<msg->height; ++y) {
      for (uint32_t x=0; x<msg->width; ++x) {
        const pcl::PointXYZ& pt = msg->points[offset];
        if (isnan(pt.z)){}
        else if (pt.y>0.3){
          kState.at<unsigned char>(y,x) = UNKNOWN;
        }
        else {
          if (pt.z<1.0){
            kState.at<unsigned char>(y,x) = CLOSE;
          }
          else {
            kState.at<unsigned char>(y,x) = FAR;
          }
        }
        ++offset;
      }
    }

 //   cv::imshow("window",kState);
 //   cv::waitKey(5);

    int total_known=0;
    int total_close=0;
    for (uint32_t y=0; y<msg->height; ++y) {
      for (uint32_t x=0; x<msg->width; ++x) {
        if (kState.at<unsigned char>(y,x)==FAR){ total_known++;}
        if (kState.at<unsigned char>(y,x)==CLOSE){ total_known++; total_close++;}
      }
    }
    slow = (total_close > total_known * ratio);
  }


      
  void CallBack(geometry_msgs::Twist msg){
    geometry_msgs::Twist result;
    if (breaker){
      if (msg.linear.x>0.0) {result.linear.x=0.0;}
      else {result.linear.x = msg.linear.x;}
      result.linear.y=0.0;
      result.linear.z=0.0;
      result.angular.x=0.0;
      result.angular.y=0.0;
      result.angular.z=0.0;
    }
    else {
      result = msg;
      if (slow && msg.linear.x>0.1){
        result.linear.x = 0.1;
      }
    }
    pub_.publish(result);
  }

};   


int main(int argc, char** argv)
{
 // cv::namedWindow("Window");
  ros::init(argc, argv, "Governator");
  Governator gv;
  ros::spin();
  return 0;
}

  
