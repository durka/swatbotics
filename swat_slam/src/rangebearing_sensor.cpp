#include <opencv2/opencv.hpp>
#include "swat_slam/Feature.h"
#include "transform2d.h"
#include "sensor.h"

using swat_slam::Feature;

Feature measurement(Transform2D x, Transform2D b)
{
    Feature z;

    z.range = sqrt(pow(x.tx - b.tx, 2) + pow(x.ty - b.ty, 2));
    z.bearing = fmod(atan2(b.ty - x.ty,
                           b.tx - x.tx) - x.theta,
                     float(2*M_PI));

    return z;
}

cv::Mat_<float> meas_diff(Feature z1, Feature z2)
{
    cv::Mat_<float> zdiff(2, 1);
    zdiff(0) = z1.range - z2.range;
    zdiff(1) = fmod(z1.bearing - z2.bearing, (float)(2*M_PI));
    if (zdiff(1) > M_PI)
    {
        zdiff(1) -= 2*M_PI;
    }
    else if (zdiff(1) < -M_PI)
    {
        zdiff(1) += 2*M_PI;
    }

    return zdiff;
}

cv::Mat_<float> meas_jacob(Transform2D x, Transform2D b)
{
    cv::Mat_<float> H(2, 2);

    float u = b.tx - x.tx,
          v = b.ty - x.ty,
          denom = pow(u, 2) + pow(v, 2);

    H(0,0) = -u/sqrt(denom);
    H(0,1) = -v/sqrt(denom);

    H(1,0) = v/denom;
    H(1,1) = -u/denom;

    return H;
}

