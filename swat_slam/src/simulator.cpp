#include <ros/ros.h>
#include <opencv2/opencv.hpp>
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Point.h"
#include "swat_slam/Feature.h"
#include "swat_slam/Map.h"
#include "swat_slam/SensorReading.h"
#include "swat_slam/PointWithCovariance.h"
#include "kdl/frames.hpp"
#include "transform2d.h"
#include "particle.h"
#include "parameters.h"
#include "sensor.h"

using namespace ros;
using nav_msgs::Odometry;
using geometry_msgs::Point;
using swat_slam::Feature;
using swat_slam::Map;
using swat_slam::SensorReading;
using swat_slam::PointWithCovariance;
using KDL::Rotation;

#define ZOOM 5

class Simulator
{
    protected:
        NodeHandle& nh_;
        Subscriber loc_sub_, map_sub_;
        Publisher odom_pub_, sensor_pub_;
        Timer odom_timer_, sensor_timer_;

        Particle sim_, robot_;
        Transform2D u_;

        // source: http://www.earth-time.org/projects/upb/public_docs/ErrorEllipses.pdf
        static void draw_ellipse(cv::Mat img, Transform2D mu, cv::Mat_<float> sigma, cv::Scalar color, int thickness)
        {
            cv::Mat_<float> evals, evecs;

            cv::eigen(sigma, evals, evecs);
            cv::sqrt(evals, evals); // lengths of ellipse axes are square root of eigenvalues

            float theta = 0.5 * atan(2 * sigma(0,1) / (sigma(0,0) - sigma(1,1)));

            cv::Size axes;
            if (sigma(0,0) < sigma(1,1))
            {
                axes = cv::Size(evals(0), evals(1));
            }
            else
            {
                axes = cv::Size(evals(1), evals(0));
            }

            cv::ellipse(img, cv::Point(ZOOM*mu.tx, ZOOM*mu.ty), axes, theta*180/M_PI, 0, 360, color, thickness);
        }

        void display()
        {
            cv::Mat img(ZOOM*100, ZOOM*100, CV_8UC3);
            img = CV_RGB(255, 255, 255);

            // local copies
            Transform2D x = robot_.x();
            vector<Landmark> map = robot_.map(), sim_map = sim_.map();

            // DRAWING
            cv::circle(img, cv::Point(ZOOM*sim_.x().tx, ZOOM*sim_.x().ty), 3, CV_RGB(255, 0, 0));
            cv::line(img, cv::Point(ZOOM*sim_.x().tx,
                                    ZOOM*sim_.x().ty),
                          cv::Point(ZOOM*(sim_.x().tx + 2*cos(sim_.x().theta)),
                                    ZOOM*(sim_.x().ty + 2*sin(sim_.x().theta))),
                          CV_RGB(255, 0, 0));
            cv::circle(img, cv::Point(ZOOM*x.tx, ZOOM*x.ty), 2, CV_RGB(0, 0, 255));
            for (unsigned i = 0; i < sim_map.size(); ++i)
            {
                cv::circle(img, cv::Point(ZOOM*sim_map[i].mu.tx, ZOOM*sim_map[i].mu.ty), 3, CV_RGB(255, 0, 0));
            }
            for (unsigned i = 0; i < map.size(); ++i)
            {
                cv::circle(img, cv::Point(ZOOM*map[i].mu.tx, ZOOM*map[i].mu.ty), 2, CV_RGB(0, 255, 0));
                draw_ellipse(img, map[i].mu, map[i].sigma*ZOOM, CV_RGB(0, 255, 0), map[i].odds);
            }

            cv::flip(img, img, 0);
            cv::imshow("simulator", img);
            cv::waitKey(1);
        }

        void fix_origin()
        {
            vector<Landmark> map = robot_.map();

            Transform2D delta = sim_.x() * robot_.x().inv(),
                        rot(1, 0, 0, delta.theta),
                        trans(1, delta.tx, delta.ty, 0);
            cv::Mat_<float> rotmat(2,2);
            rotmat(0,0) = cos(rot.theta);    rotmat(0,1) = -sin(rot.theta);
            rotmat(1,0) = sin(rot.theta);    rotmat(1,1) = cos(rot.theta);

            robot_.clear_map();
            for (unsigned i = 0; i < map.size(); ++i)
            {
                robot_.add_landmark(trans * rot * map[i].mu, rotmat.inv() * map[i].sigma * rotmat, map[i].odds);
            }
            robot_.moveTo(sim_.x());
        }

    public:
        Simulator(NodeHandle& nh) : nh_(nh)
        {
            cv::namedWindow("simulator");

            // initial conditions
            sim_.clear_map();
            sim_.moveTo(Transform2D(1, xo, yo, tho));
            robot_.clear_map();

            sim_.add_landmark(Transform2D(1, 10, 10, 0), cv::Mat::eye(2, 2, CV_32FC1));
            sim_.add_landmark(Transform2D(1, 90, 90, 0), cv::Mat::eye(2, 2, CV_32FC1));
            sim_.add_landmark(Transform2D(1, 20, 40, 0), cv::Mat::eye(2, 2, CV_32FC1));
            sim_.add_landmark(Transform2D(1, 70, 30, 0), cv::Mat::eye(2, 2, CV_32FC1));
            sim_.add_landmark(Transform2D(1, 20,  5, 0), cv::Mat::eye(2, 2, CV_32FC1));
            sim_.add_landmark(Transform2D(1, 10, 80, 0), cv::Mat::eye(2, 2, CV_32FC1));

            // ROS plumbing
            loc_sub_ = nh_.subscribe("loc", 10, &Simulator::loc_callback, this);
            map_sub_ = nh_.subscribe("map", 10, &Simulator::map_callback, this);

            odom_pub_ = nh_.advertise<Odometry>("odom", 10);
            sensor_pub_ = nh_.advertise<SensorReading>("sensor", 10);

            // simulated hardware
            odom_timer_ = nh_.createTimer(Duration(1.0/odom_hz), &Simulator::simulate_odom, this);
            sensor_timer_ = nh_.createTimer(Duration(1.0/sensor_hz), &Simulator::simulate_sensor, this);
            ROS_INFO("odometry will be sent at %g hz", odom_hz);
            ROS_INFO("sensor readings will be sent at %g hz", sensor_hz);

            display();
        }
        ~Simulator()
        {
            loc_sub_.shutdown();
            map_sub_.shutdown();
        }

        void simulate_odom(const TimerEvent& evt)
        {
            ROS_INFO("sending odometry");

            //Transform2D u = sim_.x() * sim_.prev().inv();
            //Transform2D u(1,
                          //sim_.x().tx - sim_.prev().tx,
                          //sim_.x().ty - sim_.prev().ty,
                          //fmod(sim_.x().theta - sim_.prev().theta, float(2*M_PI)));

            Odometry odom;
            odom.pose.pose.position.x = u_.tx;
            odom.pose.pose.position.y = u_.ty;
            Rotation::RPY(0,
                          0,
                          u_.theta)
                .GetQuaternion(odom.pose.pose.orientation.x,
                               odom.pose.pose.orientation.y,
                               odom.pose.pose.orientation.z,
                               odom.pose.pose.orientation.w);
            ROS_INFO("u (%g, %g, %g)", u_.tx, u_.ty, u_.theta*180/M_PI);
            odom_pub_.publish(odom);
            spinOnce();
        }

        void simulate_sensor(const TimerEvent& evt)
        {
            ROS_INFO("sending sensor reading");

            SensorReading sr;
            Feature feat;
            vector<Landmark> map = sim_.map();
            Landmark lm;
            for (unsigned i = 0; i < map.size(); ++i)
            {
                lm = map[i];
                feat = measurement(sim_.x(), lm.mu);

                ROS_INFO("\t%d (%g, %g) => (%g, %g)", i, lm.mu.tx, lm.mu.ty, feat.range, feat.bearing*180/M_PI);

                if (feat.range <= sensor_range && fabs(feat.bearing) <= sensor_fov)
                {
                    ROS_INFO("\t\tyes");
                    // add noise
                    feat.range += cv::theRNG().gaussian(sqrt(R[0][0]));
                    feat.bearing += cv::theRNG().gaussian(sqrt(R[1][1]));

                    sr.features.push_back(feat);
                }
                else
                {
                    ROS_INFO("\t\tno");
                }
            }
            sensor_pub_.publish(sr);
            spinOnce();
        }

        void loc_callback(const Odometry::ConstPtr& msg)
        {
            double r, p, t;
            Rotation::Quaternion(msg->pose.pose.orientation.x,
                                 msg->pose.pose.orientation.y,
                                 msg->pose.pose.orientation.z,
                                 msg->pose.pose.orientation.w).GetRPY(r, p, t);

            robot_.moveTo(Transform2D(1,
                                      msg->pose.pose.position.x,
                                      msg->pose.pose.position.y,
                                      t));
        }

        void map_callback(const Map::ConstPtr& msg)
        {
            robot_.clear_map();
            for (unsigned i = 0; i < msg->features.size(); ++i)
            {
                cv::Mat_<float> covar(2, 2);
                for (int r = 0; r < 2; ++r)
                {
                    for (int c = 0; c < 2; ++c)
                    {
                        covar(r, c) = msg->features[i].covar[r*2+c];
                    }
                }
                robot_.add_landmark(Transform2D(1,
                                                msg->features[i].x,
                                                msg->features[i].y,
                                                0),
                                    covar);
            }

            printf("(%g, %g, %g)\t{", robot_.x().tx, robot_.x().ty, robot_.x().theta*180/M_PI);
            vector<Landmark> map = robot_.map();
            for (unsigned j = 0; j < map.size(); ++j)
            {
                printf(" (%g, %g)", map[j].mu.tx, map[j].mu.ty);
            }
            printf(" }\n");

            fix_origin();
            display();
        }

        void key(int k)
        {
            switch (k)
            {
                // right (left)
                case 63234:
                case 65363:
                    sim_.moveTo(Transform2D(1,
                                            sim_.x().tx,
                                            sim_.x().ty,
                                            fmod(float(sim_.x().theta + 3*M_PI/180),
                                                 float(2*M_PI))));
                    break;
                // left (right)
                case 63235:
                case 65361:
                    sim_.moveTo(Transform2D(1,
                                            sim_.x().tx,
                                            sim_.x().ty,
                                            fmod(float(sim_.x().theta - 3*M_PI/180),
                                                 float(2*M_PI))));
                    break;
                // up (forward)
                default:
                case 63232:
                case 65362:
                    //sim_.moveTo(Transform2D(1,
                                            //sim_.x().tx + 2*cos(sim_.x().theta),
                                            //sim_.x().ty + 2*sin(sim_.x().theta),
                                            //sim_.x().theta));
                    u_ = Transform2D(1, 2, 0, 0);
                    break;
                // down (backward)
                case 63233:
                case 65364:
                    sim_.moveTo(Transform2D(1,
                                            sim_.x().tx - 2*cos(sim_.x().theta),
                                            sim_.x().ty - 2*sin(sim_.x().theta),
                                            sim_.x().theta));
                    break;
            }

            sim_.moveTo(sim_.x() * u_);
            sim_.moveTo(Transform2D(1,
                                    sim_.x().tx + cv::theRNG().gaussian(sqrt(S[0][0])),
                                    sim_.x().ty + cv::theRNG().gaussian(sqrt(S[1][1])),
                                    sim_.x().theta + cv::theRNG().gaussian(sqrt(S[2][2]))));

            display();
        }
};

int main(int argc, char *argv[])
{
    init(argc, argv, "simulator");
    NodeHandle nh;

    load_params();
    Simulator sim(nh);

    int k, kk;

    while (ok())
    {
        kk = -1;
        for (int t = 0; t < 200; ++t)
        {
            k = cv::waitKey(10);
            if (k != -1) kk = k;
            spinOnce();
        }
        sim.key(kk);
    }

    return 0;
}

