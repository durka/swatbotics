#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>
#include <cstdarg>
#include <ctime>
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "nav_msgs/Odometry.h"
#include "sensor_msgs/Image.h"
#include "sensor_msgs/PointCloud2.h"
#include "kdl/frames.hpp"

using namespace ros;
using namespace rosbag;
using namespace cv;
using namespace std;
using geometry_msgs::Pose;
using geometry_msgs::PoseWithCovarianceStamped;
using nav_msgs::Odometry;
namespace enc = sensor_msgs::image_encodings;
using sensor_msgs::Image;
using sensor_msgs::PointCloud2;
using KDL::Rotation;

typedef pcl::PointCloud<pcl::PointXYZRGB> PointCLoud;

class Bagger
{
    private:
        NodeHandle& nh_;
        Bag bag_;
        Subscriber sub_;
        string topic_;
        Time start_;
        int nbag_;
        bool split_, compress_;
        string fnprefix_;

    public:
        Bagger(NodeHandle& nh, string prefix, string topic)
            : nh_(nh), topic_(topic), split_(false), compress_(false)
        {
            nbag_ = 0;

            time_t rawtime;
            tm *t;
            time(&rawtime);
            t = localtime(&rawtime);

            char timestr[100] = "", fn[100] = "";
            sprintf(timestr, "%spbag_%4d-%02d-%02d-%02d-%02d-%02d.pbag",
                prefix.c_str(),
                t->tm_year + 1900,
                t->tm_mon + 1,
                t->tm_mday,
                t->tm_hour,
                t->tm_min,
                t->tm_sec);

            fnprefix_ = timestr;
            sprintf(fn, "%s.%d", timestr, nbag_);

            bag_.open(fn, bagmode::Write);
            if (compress_)
            {
                bag_.setCompression(compression::BZ2);
            }
            ROS_INFO("opened bag %s", fn);

            start_ = Time::now();
            sub_ = nh_.subscribe(topic_, 1, &Bagger::callback, this);
        }

        void callback(const PointCloud2::ConstPtr& msg)
        {
            ROS_INFO("received message");

            Duration elapsed = Time::now() - start_;
            if (split_ && elapsed > Duration(15.0))
            {
                bag_.close();
                ++nbag_;

                char fn[100] = "";
                sprintf(fn, "%s.%d", fnprefix_.c_str(), nbag_);
                bag_.open(fn, bagmode::Write);
                if (compress_)
                {
                    bag_.setCompression(compression::BZ2);
                }
                ROS_INFO("split bag");
                start_ = Time::now();
            }
            
            bag_.write(topic_, Time::now(), *msg);
            ROS_INFO("wrote message");
        }
        
        ~Bagger()
        {
            bag_.close();
            ROS_INFO("closing bag");
        }
};

Time mintime(int n, ...)
{
    va_list args;
    va_start(args, n);

    Time m = *va_arg(args, Time*);

    for (int i = 1; i < n; ++i)
    {
        Time next = *va_arg(args, Time*);
        if (next < m)
        {
            m = next;
        }
    }
    
    return m;
}

Time maxtime(int n, ...)
{
    va_list args;
    va_start(args, n);

    Time m = *va_arg(args, Time*);

    for (int i = 1; i < n; ++i)
    {
        Time next = *va_arg(args, Time*);
        if (next > m)
        {
            m = next;
        }
    }
    
    return m;
}

string pose2str(Pose p)
{
    ostringstream s;

    s << "(" << p.position.x << ", " << p.position.y << ", ";

    double roll, pitch, yaw;
    Rotation::Quaternion(p.orientation.x,
                         p.orientation.y,
                         p.orientation.z,
                         p.orientation.w).GetRPY(roll, pitch, yaw);

    s << yaw << ")";

    return s.str();
}

Mat cloud2mat(PointCloud2 pc)
{
    PointCLoud cloud;
    pcl::fromROSMsg(pc, cloud);

    Mat img(cloud.height, cloud.width, CV_8UC3);
    for (unsigned w = 0; w < cloud.width; ++w)
    {
        for (unsigned h = 0; h < cloud.height; ++h)
        {
            img.at<Vec3b>(h,w) = *(Vec3b*)(&cloud.at(w,h).rgb);
        }
    }

    return img;
}

void play(NodeHandle& nh, string dir, string timestamp, string rgbd, string cam, string odom, string ekf)
{
    Bag bag, pbag;
    bag.open(dir + "bag_" + timestamp + ".bag", bagmode::Read);
    pbag.open(dir + "pbag_" + timestamp  + ".pbag", bagmode::Read);

    vector<string> btopics, ptopics;
    btopics.push_back(cam);
    btopics.push_back(odom);
    btopics.push_back(ekf);
    ptopics.push_back(rgbd);

    View bview(bag, TopicQuery(btopics)), pview(pbag, TopicQuery(ptopics));

    vector<Image> images;
    vector<Odometry> odoms;
    vector<PoseWithCovarianceStamped> ekfs;
    vector<PointCloud2> clouds;

#define handle_msg(e,top,typ,vec) \
        e if (m.getTopic() == (top) || "/" + m.getTopic() == (top)) \
        { \
            typ::ConstPtr msg = m.instantiate<typ>(); \
            if (msg != NULL) \
            { \
                vec.push_back(*msg); \
            } \
        }

    BOOST_FOREACH(MessageInstance const m, bview)
    {

        if (m.getTopic() == cam || "/" + m.getTopic() == cam)
        {
            Image::ConstPtr msg = m.instantiate<Image>();
            if (msg != NULL)
            {
                Image img = *msg;
                img.header.stamp = m.getTime();
                images.push_back(img);
            }
        }

        handle_msg(else, odom, Odometry,                  odoms)
        handle_msg(else, ekf,  PoseWithCovarianceStamped, ekfs)
    }

    BOOST_FOREACH(MessageInstance const m, pview)
    {
        handle_msg(,     rgbd, PointCloud2,               clouds)
    }

    ROS_INFO("Gathered %d images, %d odoms, %d ekfs, %d clouds", (int)images.size(), (int)odoms.size(), (int)ekfs.size(), (int)clouds.size());
    ROS_INFO("Time ranges:");
    ROS_INFO_STREAM("\timages:\t" << images[0].header.stamp << "\tto\t" << images[images.size()-1].header.stamp);
    ROS_INFO_STREAM("\todoms:\t" << odoms[0].header.stamp << "\tto\t" << odoms[odoms.size()-1].header.stamp);
    ROS_INFO_STREAM("\tekfs:\t" << ekfs[0].header.stamp << "\tto\t" << ekfs[ekfs.size()-1].header.stamp);
    ROS_INFO_STREAM("\tclouds:\t" << clouds[0].header.stamp << "\tto\t" << clouds[clouds.size()-1].header.stamp);

    int begin = floor(mintime(4,
                              &images[0].header.stamp,
                               &odoms[0].header.stamp,
                                &ekfs[0].header.stamp,
                              &clouds[0].header.stamp).toSec());
    int end = ceil(maxtime(4,
                           &images[images.size()-1].header.stamp,
                             &odoms[odoms.size()-1].header.stamp,
                               &ekfs[ekfs.size()-1].header.stamp,
                           &clouds[clouds.size()-1].header.stamp).toSec());

    ROS_INFO_STREAM("\tall:\t" << begin << "\t\tto\t" << end << "\t(" << (end-begin) << " s)");

    vector<Image>::iterator ii = images.begin();
    vector<Odometry>::iterator oi = odoms.begin();
    vector<PoseWithCovarianceStamped>::iterator ei = ekfs.begin();
    vector<PointCloud2>::iterator ci = clouds.begin();

    namedWindow("webcam");
    namedWindow("kinect");

#define maybe_inc(i,v,a) if (i != v.end() && i->header.stamp < oi->header.stamp) { ++i; if (i != v.end()) { a; } }
    for (; oi != odoms.end(); ++oi)
    {
        cout << pose2str(oi->pose.pose);

        maybe_inc(ii, images, imshow("webcam", cv_bridge::toCvCopy(*ii, enc::RGB8)->image));

        maybe_inc(ei, ekfs,   cout << "\t" << pose2str(ei->pose.pose));

        maybe_inc(ci, clouds, imshow("kinect", cloud2mat(*ci)));

        cout << endl;
        waitKey(10);
    }

    bag.close();
    pbag.close();
}

// rosrun swat_slam pbag record {bagfile dir} {rgbd topic} : record /camera/rgb/points into a pbagfile
// rosrun swat_slam pbag play {bagfile dir} {timestamp} {rgbd topic} {cam topic} {raw odom topic} {ekf odom topic} : play stuff from synced bagfile+pbagfile
int main(int argc, char *argv[])
{
    init(argc, argv, "pbag");

    NodeHandle nh;

    if (strcmp(argv[1], "record") == 0)
    {
        Bagger bagger(nh, argv[2], argv[3]);
        spin();
    }
    else if (strcmp(argv[1], "play") == 0)
    {
        play(nh, argv[2], argv[3], argv[4], argv[5], argv[6], argv[7]);
    }

    return 0;
}

