#include <ros/ros.h>
#include <fstream>
#include "swat_slam/SensorReading.h"
#include "parameters.h"

using swat_slam::SensorReading;
using namespace ros::param;
using namespace std;

int N = 100;
double S[3][3] = {{1, 0, 0},
                  {0, 1, 0},
                  {0, 1, pow(2*M_PI/180, 2)}},
       R[2][2] = {{pow(2.0, 2), 0},
                  {0          , pow(3*M_PI/180, 2)}},
       Q[2][2] = {{3, 0},
                  {0, 3}};
double po = 0.001;
double sensor_range = 60;
double sensor_fov = 45*M_PI/180;

double sensor_hz = 1;
double odom_hz = 1;

double xo = 0, yo = 0, tho = 0;

#define PARDEF(t,p,d) param<t>("/swat_slam/" #p, p, d)
#define PARAM(t,p) PARDEF(t,p,p)

void load_params()
{
    PARAM(int, N);

    PARAM(double, sensor_range);
    PARAM(double, sensor_fov);
    sensor_fov *= M_PI/180;

    PARAM(double, po);

    PARAM(double, sensor_hz);
    PARAM(double, odom_hz);

    double Sx, Sy, Sth, Rr, Rth, Qx, Qy;
    PARDEF(double, Sx, sqrt(S[0][0]));
    PARDEF(double, Sy, sqrt(S[1][1]));
    PARDEF(double, Sth, sqrt(S[2][2]));
    PARDEF(double, Rr, sqrt(R[0][0]));
    PARDEF(double, Rth, sqrt(R[1][1]));
    PARDEF(double, Qx, sqrt(Q[0][0]));
    PARDEF(double, Qy, sqrt(Q[1][1]));
    S[0][0] = pow(Sx, 2);
    S[1][1] = pow(Sy, 2);
    S[2][2] = pow(Sth*M_PI/180, 2);
    R[0][0] = pow(Rr, 2);
    R[1][1] = pow(Rth*M_PI/180, 2);
    Q[0][0] = pow(Qx, 2);
    Q[1][1] = pow(Qy, 2);

    PARAM(double, xo);
    PARAM(double, yo);
    PARAM(double, tho);
    tho *= M_PI/180;
}

// Matlab traces
void trace::load(string filenames[4], trace::Particles& particles, trace::Sensors& sensors, trace::Samples& samples, trace::Commands& commands)
{
    ifstream in;
    string line;
    istringstream ss;

    in.open(filenames[0].c_str());
    if (!in.good())
    {
        ROS_ERROR("failed to open particles trace");
    }
    else
    {
        trace::P p;
        int t, i, Nb;
        while (getline(in, line))
        {
            ss.clear();
            ss.str(line);

            ss >> t >> i >> p.weight
               >> p.x.tx >> p.x.ty >> p.x.theta
               >> Nb;
            p.map.resize(Nb);
            for (int j = 0; j < Nb; ++j)
            {
                p.map[j].sigma = cv::Mat_<float>(2,2);
                ss >> p.map[j].mu.tx >> p.map[j].mu.ty
                   >> p.map[j].sigma(0,0) >> p.map[j].sigma(0,1) >> p.map[j].sigma(1,0) >> p.map[j].sigma(1,1);
            }

            if (particles[t].size() == 0)
            {
                particles[t].resize(N);
            }
            particles[t][i] = p;
        }
        in.close();
    }

    in.open(filenames[1].c_str());
    if (!in.good())
    {
        ROS_ERROR("failed to open sensors trace");
    }
    else
    {
        SensorReading s;
        int t, Nb;
        while (getline(in, line))
        {
            ss.clear();
            ss.str(line);

            ss >> t >> Nb;
            s.features.resize(Nb);
            for (int i = 0; i < Nb; ++i)
            {
                ss >> s.features[i].range >> s.features[i].bearing;
            }

            sensors[t] = s;
        }
        in.close();
    }

    in.open(filenames[2].c_str());
    if (!in.good())
    {
        ROS_ERROR("failed to open samples trace");
    }
    else
    {
        trace::S s;
        int t, i, k;
        while (getline(in, line))
        {
            ss.clear();
            ss.str(line);

            ss >> t;
            for (int j = 0; j < N; ++j)
            {
                ss >> i >> k;
                s[i] = k;
            }
            samples[t] = s;
        }
        in.close();
    }

    in.open(filenames[3].c_str());
    if (!in.good())
    {
        ROS_ERROR("failed to open commands trace");
    }
    else
    {
        Transform2D c;
        int t;
        while (getline(in, line))
        {
            ss.clear();
            ss.str(line);

            ss >> t >> c.tx >> c.ty;

            commands[t] = c;
        }
        in.close();
    }
}

