#include <ros/ros.h>
#include <opencv2/opencv.hpp>
#include <vector>
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Point.h"
#include "swat_slam/Feature.h"
#include "swat_slam/Map.h"
#include "swat_slam/SensorReading.h"
#include "swat_slam/PointWithCovariance.h"
#include "kdl/frames.hpp"
#include "transform2d.h"
#include "particle.h"
#include "parameters.h"
#include "sensor.h"

using namespace ros;
using namespace std;
using nav_msgs::Odometry;
using geometry_msgs::Point;
using swat_slam::Feature;
using swat_slam::Map;
using swat_slam::SensorReading;
using swat_slam::PointWithCovariance;
using KDL::Rotation;
        
class Slammer
{
    protected:
        NodeHandle& nh_;

        Subscriber odom_sub_, sensor_sub_;
        Publisher loc_pub_, map_pub_;

        trace::Particles tparticles;
        trace::Sensors tsensors;
        trace::Samples tsamples;
        trace::Commands tcommands;

        cv::Mat_<float> Qm, Rm, Sm, Sms;

        vector<Particle> particles_;
        vector<float> weights_;
        Transform2D prev_;

        static int sample_weighted(float *w, int n)
        {
            float r = rand()/float(RAND_MAX), cumsum = 0;
            for (int j = 0; j < n; ++j)
            {
                cumsum += w[j];
                if (cumsum > r)
                {
                    return j;
                }
            }
            return n-1;
        }

        template<typename T>
        static int maxi(T *arr, int n)
        {
            int mi = 0;
            T m = arr[0];
            for (int i = 1; i < n; ++i)
            {
                if (arr[i] > m)
                {
                    mi = i;
                    m = arr[i];
                }
            }
            return mi;
        }


        void incorporate_feature(vector<Feature> zz)
        {
            for (int i = 0; i < N; ++i)
            {
                vector<Landmark> map = particles_[i].map();
                Transform2D x = particles_[i].x();

                vector<int> obs(zz.size());
                Feature z;

                weights_[i] = 1;
                for (unsigned k = 0; k < zz.size(); ++k)
                {
                    z = zz[k];

                    // decide which feature it is, by max likelihood
                    vector<float> likelihood(map.size() + 1);
                    cv::Mat_<float> H[map.size()], zdiff[map.size()];
                    Feature z_predict;
                    for (unsigned j = 0; j < map.size(); ++j)
                    {
                        // predict measurement of this particular landmark
                        z_predict = measurement(x, map[j].mu);

                        // how far off is the real measurement?
                        zdiff[j] = meas_diff(z, z_predict);
                        
                        // jacobian and likelihood
                        H[j] = -meas_jacob(x, map[j].mu);
                        cv::Mat sigma = H[j]*map[j].sigma*H[j].t() + Rm;
                        cv::Mat_<float> op = zdiff[j].t() * sigma.inv() * zdiff[j];
                        likelihood[j] = 1.0/(2*M_PI * sqrt(cv::determinant(sigma))) * exp(-0.5 * op(0)); // mvnpdf(zdiff[j], sigma)
                    }
                    likelihood[map.size()] = po; // feature creation probability
                    unsigned maxj = maxi(&likelihood[0], likelihood.size());

                    if (maxj == map.size())
                    {
                        // new feature!
                        
                        // invert the measurement function
                        Transform2D mu;
                        float theta = fmod(z.bearing + x.theta, (float)(2*M_PI));
                        mu.tx = x.tx + z.range*cos(theta);
                        mu.ty = x.ty + z.range*sin(theta);
			
                        // new jacobian
                        cv::Mat_<float> Hnew = -meas_jacob(x, mu);
                        cv::Mat_<float> sigma = Hnew.inv() * Rm * Hnew.inv().t();

                        Landmark lm = {mu, sigma, 1};
                        map.push_back(lm);
                    }
                    else
                    {
                        // old feature
                        int j = maxj;

                        // measurement covariance
                        cv::Mat meas_covar = H[j] * map[j].sigma * H[j].t() + Rm;

                        // kalman gain
                        cv::Mat K = map[j].sigma * H[j].t() * meas_covar.inv();

                        // update landmark
                        cv::Mat_<float> dmu = K * zdiff[j];
                        map[j].mu.tx += dmu(0);
                        map[j].mu.ty += dmu(1);
                        map[j].sigma = (cv::Mat_<float>::eye(cv::Size(2, 2)) - K*H[j]) * map[j].sigma + Sms;
                        ++map[j].odds;
                    }

                    weights_[i] *= likelihood[maxj];
                    obs[k] = maxj;
                }

                // prune bad landmarks
                particles_[i].clear_map();
                for (unsigned j = 0; j < map.size(); ++j)
                {
                    if (find(obs.begin(), obs.end(), j) == obs.end())
                    {
                        // expand covariance of unobserved features
                        map[j].sigma += 0.25 * Sms;

                        // decrease confidence of features that we should have seen
                        Feature z_predict = measurement(x, map[j].mu);
                        if (z_predict.range <= sensor_range && fabs(z_predict.bearing) <= sensor_fov)
                        {
                            --map[j].odds;
                        }
                    }

                    // only keep features that exist
                    if (map[j].odds >= 0)
                    {
                        particles_[i].add_landmark(map[j]);
                    }
                }
            }
        }

        void resample()
        {
            float weightsum = 0.0;

            // normalize weights
            for (int i = 0; i < N; ++i)
            {
                weightsum += weights_[i];
            }
            for (int i = 0; i < N; ++i)
            {
                weights_[i] /= weightsum;

                /*
                printf("%d(%g)\t(%g, %g, %g)\t{", i, weights_[i], particles_[i].x().tx, particles_[i].x().ty, particles_[i].x().theta*180/M_PI);
                vector<Landmark> map = particles_[i].map();
                for (int j = 0; j < map.size(); ++j)
                {
                    printf(" (%g, %g)", map[j].mu.tx, map[j].mu.ty);
                }
                printf(" }\n");
                */
            }

            // resample
            vector<Particle> old_particles = particles_;
            vector<float> old_weights = weights_;
            int j;
            for (int i = 0; i < N; ++i)
            {
                j = sample_weighted(&old_weights[0], N);

                particles_[i] = old_particles[j];
                weights_[i] = old_weights[j];
            }
        }

        void publish()
        {
            int j = maxi(&weights_[0], N);
            Particle best = particles_[j];
            Odometry loc;
            loc.pose.pose.position.x = best.x().tx;
            loc.pose.pose.position.y = best.x().ty;
            Rotation::RPY(0,
                          0,
                          best.x().theta).GetQuaternion(loc.pose.pose.orientation.x,
                                                        loc.pose.pose.orientation.y,
                                                        loc.pose.pose.orientation.z,
                                                        loc.pose.pose.orientation.w);
            Map m;
            PointWithCovariance p;
            vector<Landmark> bm = best.map();
            for (unsigned i = 0; i < bm.size(); ++i)
            {
                p.x = bm[i].mu.tx;
                p.y = bm[i].mu.ty;
                for (int r = 0; r < 2; ++r)
                {
                    for (int c = 0; c < 2; ++c)
                    {
                        p.covar[r*2+c] = bm[i].sigma(r, c);
                    }
                }
                m.features.push_back(p);
            }
            loc_pub_.publish(loc);
            map_pub_.publish(m);
        }

    public:
        Slammer(NodeHandle nh) : nh_(nh)
        {
            init();

            // ROS plumbing
            odom_sub_ = nh_.subscribe("odom", 10, &Slammer::odom_callback, this);
            sensor_sub_ = nh_.subscribe("sensor", 10, &Slammer::sensor_callback, this);
            
            loc_pub_ = nh_.advertise<Odometry>("loc", 10);
            map_pub_ = nh_.advertise<Map>("map", 10);
        }
        Slammer(NodeHandle nh, string fns[4]) : nh_(nh)
        {
            init();

            trace::load(fns, tparticles, tsensors, tsamples, tcommands);

            int t = 10;
            for (int i = 0; i < N; ++i)
            {
                weights_[i] = tparticles[t][i].weight;
                particles_[i].moveTo(tparticles[t][i].x);
                particles_[i].clear_map();
                for (unsigned j = 0; j < tparticles[t][i].map.size(); ++j)
                {
                    particles_[i].add_landmark(tparticles[t][i].map[j]);
                }
            }
            print_particles(t);
            motion_update(tcommands[t].tx, tcommands[t].ty, tcommands[t].theta);
            incorporate_feature(tsensors[t].features);
            for (int i = 0; i < N; ++i)
            {
                ROS_ERROR("resampling %d from %d", i, tsamples[t][i]);
                particles_[i] = particles_[tsamples[t][i]];
                weights_[i] = weights_[tsamples[t][i]];
            }
            print_particles(t);
        }

        void print_particles(int t)
        {
            for (int i = 0; i < N; ++i)
            {
                vector<Landmark> map = particles_[i].map();
                printf("%d %d %f %f %f %f %d", t+1, i, weights_[i], particles_[i].x().tx, particles_[i].x().ty, particles_[i].x().theta, (int)map.size());
                for (unsigned j = 0; j < map.size(); ++j)
                {
                    printf(" %f %f %f %f %f %f", map[j].mu.tx, map[j].mu.ty, map[j].sigma(0,0), map[j].sigma(0,1), map[j].sigma(1,0), map[j].sigma(1,1));
                }
                printf("\n");
            }
        }

        void init()
        {
            // make cv::Mats from parameters
            Qm = cv::Mat_<float>(2, 2);
            for (int i = 0; i < 2; ++i)
            {
                for (int j = 0; j < 2; ++j)
                {
                    Qm(i,j) = Q[i][j];
                }
            }
            Rm = cv::Mat_<float>(2, 2);
            for (int i = 0; i < 2; ++i)
            {
                for (int j = 0; j < 2; ++j)
                {
                    Rm(i,j) = R[i][j];
                }
            }
            Sm = cv::Mat_<float>(3, 3);
            for (int i = 0; i < 3; ++i)
            {
                for (int j = 0; j < 3; ++j)
                {
                    Sm(i,j) = S[i][j];
                }
            }
            Sms = cv::Mat_<float>(2, 2);
            for (int i = 0; i < 2; ++i)
            {
                for (int j = 0; j < 2; ++j)
                {
                    Sms(i,j) = S[i][j];
                }
            }

            // distribute particles
            particles_.resize(N);
            weights_.resize(N);
            float x, y, t;
            for (int i = 0; i < N; ++i)
            {
                x = cv::theRNG().uniform(0., 100.);
                y = cv::theRNG().uniform(0., 100.);
                t = cv::theRNG().uniform(0., 2*M_PI);

                particles_[i].moveTo(Transform2D(1, x, y, t));
                particles_[i].clear_map();

                weights_[i] = 1.0/N;
            }

            prev_ = Transform2D(1, 0, 0, 0);
        }
        ~Slammer()
        {
            odom_sub_.shutdown();
            map_pub_.shutdown();
        }

        void odom_callback(const Odometry::ConstPtr& msg)
        {
            double tx, ty, theta, r, p;
            tx = msg->pose.pose.position.x;
            ty = msg->pose.pose.position.y;
            Rotation::Quaternion(msg->pose.pose.orientation.x,
                                 msg->pose.pose.orientation.y,
                                 msg->pose.pose.orientation.z,
                                 msg->pose.pose.orientation.w).GetRPY(r, p, theta);

            motion_update(tx, ty, theta);
        }

        void motion_update(float tx, float ty, float theta)
        {
            ROS_INFO("move by (%g, %g, %g)", tx, ty, theta);

            // motion update particles
            for (int i = 0; i < N; ++i)
            {
                particles_[i].move(Transform2D(1,
                                               tx + cv::theRNG().gaussian(sqrt(S[0][0])),
                                               ty + cv::theRNG().gaussian(sqrt(S[1][1])),
                                               theta + cv::theRNG().gaussian(sqrt(S[2][2]))));
            }
        }

        void sensor_callback(const SensorReading::ConstPtr& msg)
        {
            if (msg->features.size() > 0)
            {
                incorporate_feature(msg->features);
                resample();
            }

            publish();
        }
};

int main(int argc, char *argv[])
{
    init(argc, argv, "slammer");
    NodeHandle nh;

    srand(time(NULL));

    load_params();

    if (argc == 2) // run from traces
    {
        string prefix = argv[1];
        string fns[4] = {prefix + "particles.trace",
                         prefix + "sensors.trace",
                         prefix + "samples.trace",
                         prefix + "commands.trace"};

        Slammer slam(nh, fns);
    }
    else
    {
        Slammer slam(nh);

        spin();
    }

    return 0;
}

