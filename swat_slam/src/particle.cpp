#include <ros/ros.h>
#include "particle.h"

Particle::Particle()
{
    map_.clear();
}

Particle::Particle(const Particle& rhs)
{
    *this = rhs;
}

Particle& Particle::operator=(const Particle& rhs)
{
    if (this == &rhs)
    {
        return *this;
    }

    x_ = rhs.x_;
    map_ = rhs.map_;

    return *this;
}

Transform2D Particle::x() const
{
    return x_;
}

Transform2D Particle::prev() const
{
    return prev_;
}

vector<Landmark> Particle::map()
{
    return map_;
}

void Particle::move(const Transform2D u)
{
    prev_ = x_;
    x_ = u * x_;
}

void Particle::moveTo(const Transform2D x)
{
    prev_ = x_;
    x_ = x;
}

void Particle::clear_map()
{
    map_.clear();
}

void Particle::add_landmark(Transform2D mu, cv::Mat sigma, int odds)
{
    Landmark lm;

    lm.mu = mu;
    lm.sigma = sigma;
    lm.odds = odds;

    add_landmark(lm);
}
void Particle::add_landmark(Landmark lm)
{
    map_.push_back(lm);
}

