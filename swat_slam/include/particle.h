#ifndef _SWATSLAM_PARTICLE_H_
#define _SWATSLAM_PARTICLE_H_

#include "transform2d.h"

struct Landmark
{
    Transform2D mu;
    cv::Mat_<float> sigma;
    int odds;
};

class Particle
{
    protected:
        Transform2D x_, prev_;
        vector<Landmark> map_;

    public:
        Transform2D x() const;
        Transform2D prev() const;
        vector<Landmark> map();
        
        Particle();
        Particle(const Particle& rhs);
        Particle& operator=(const Particle& rhs);

        void move(const Transform2D u);
        void moveTo(const Transform2D x);
        void clear_map();
        void add_landmark(Landmark lm);
        void add_landmark(Transform2D mu, cv::Mat sigma, int odds=1);
};

#endif

