#ifndef _SWATSLAM_SENSOR_H_
#define _SWATSLAM_SENSOR_H_

#include <opencv2/opencv.hpp>
#include "swat_slam/Feature.h"
#include "transform2d.h"

swat_slam::Feature measurement(Transform2D x, Transform2D b);
cv::Mat_<float> meas_diff(swat_slam::Feature z1, swat_slam::Feature z2);
cv::Mat_<float> meas_jacob(Transform2D x, Transform2D b);

#endif

