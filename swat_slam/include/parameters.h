#ifndef _SWATSLAM_PARAMETERS_H_
#define _SWATSLAM_PARAMETERS_H_

#include <map>
#include "particle.h"
#include "swat_slam/SensorReading.h"

extern int N;
extern double S[3][3], R[2][2], Q[2][2];
extern double po;
extern double sensor_range;
extern double sensor_fov;

extern double sensor_hz;
extern double odom_hz;

extern double xo, yo, tho;

void load_params();

// Matlab traces
namespace trace
{
    struct P
    {
        float weight;
        Transform2D x;
        vector<Landmark> map;
    };

    typedef map<int, vector<P> > Particles;

    typedef map<int, swat_slam::SensorReading> Sensors;

    typedef map<int, int> S;
    typedef map<int, S> Samples;

    typedef map<int, Transform2D> Commands;

    void load(string filenames[4], Particles& particles, Sensors& sensors, Samples& samples, Commands& commands);
}

#endif

