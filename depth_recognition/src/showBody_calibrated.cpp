#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <opencv2/opencv.hpp>
#include <math.h>

typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloud;

void callback(const PointCloud::ConstPtr& msg)
{
  cv::Mat image = cv::Mat::zeros(msg->height, msg->width, CV_8UC3);

  int offset = 0;
  for (uint32_t y=0; y<msg->height; ++y) {
   for (uint32_t x=0; x<msg->width; ++x) {
     const pcl::PointXYZRGB& pt = msg->points[offset];
     if (pt.z > 1 && pt.z < 2) {
	cv::Vec3b* v = (cv::Vec3b*)(&pt.rgb);
	image.at<cv::Vec3b>(y, x) = *v;
     }
     ++offset;
   }
  }

  cv::imshow("window", image);
  cv::waitKey(5);

}

int main(int argc, char** argv)
{
  cv::namedWindow("window");
  ros::init(argc, argv, "sub_pcl");
  ros::NodeHandle nh;
  ros::Subscriber sub = nh.subscribe<PointCloud>("/camera/rgb/points", 1, callback);
  ros::spin();

}
