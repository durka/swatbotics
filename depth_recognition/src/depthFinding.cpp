#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/image_encodings.h>
#include <math.h>
#include <vector>

class DepthConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;

public:
  DepthConverter()
    : it_(nh_)
  {
    image_pub_ = it_.advertise("depth_image", 1);
    image_sub_ = it_.subscribe("/camera/depth/image", 1, &DepthConverter::CallBack, this);
  }

  ~DepthConverter()
  {
  }

void CallBack(const sensor_msgs::ImageConstPtr& msg)
{
  int size = (msg->height)*(msg->width);
  float max = 0.0;
  float* data_float = (float*)&(msg->data)[0];
  float buff = 0.0;
  sensor_msgs::Image new_image;
  new_image.header = msg->header;
  new_image.height = msg->height;
  new_image.width = msg->width;
  new_image.encoding = msg->encoding;
  new_image.is_bigendian = msg->is_bigendian;
  new_image.step = msg->step;

  for (int i=0; i<size; i++)
  {
    if (!isnan(data_float[i]))
    {
      if (data_float[i]>max)
      {
        max=data_float[i];
      }
    }
    else
    {
      data_float[i]=buff;
    }
  }
  for (int i=0; i<size; i++)
  {
    data_float[i] = 255*data_float[i]/max;
  }

  unsigned char *data_char = (unsigned char*)data_float;
  new_image.data = std::vector<unsigned char>(&data_char[0], &data_char[4*size-1]);


  unsigned int max_c=0;
  for (int i=0; i<4*size; i++)
  {
    if (data_char[i]>max_c)
    {
      max_c=data_char[i];
    }
  }

  image_pub_.publish(new_image);
}

};

int main(int argc, char** argv)
{
  ros::init(argc,argv,"depth_finder");
  DepthConverter dc;
  ros::spin();
  return 0;
}  
