#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>

class ImageSmush
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber rgb_sub_;
  image_transport::Subscriber depth_sub_;
  image_transport::Publisher pub_;
  cv::Mat rgb;


public:
  ImageSmush()
    :it_(nh_)
  {
    pub_ = it_.advertise("/body_image",1);
    rgb_sub_ = it_.subscribe("/camera/rgb/image_color",1,&ImageSmush::StoreRGB, this);
    depth_sub_ = it_.subscribe("/camera/depth/image",1,&ImageSmush::DepthCallBack, this);
    cv::namedWindow("window");
  }

  ~ImageSmush()
  {
    cv::destroyWindow("window");
  }

  void StoreRGB(const sensor_msgs::ImageConstPtr& msg)
  {
   try
    {
      cv_bridge::CvImagePtr rgb_img;
      rgb_img = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
      rgb = rgb_img->image;
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
  }

  void DepthCallBack(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr depth_img;
    try
    {
      depth_img = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_32FC1);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    cv::Mat mask;
    cv::inRange(depth_img->image, 1, 2, mask);

    cv::Mat result;
    rgb.copyTo(result, mask);

    cv::imshow("window", result);
    cv::waitKey(3);
  } 

};

int main(int argc, char** argv)
{
  ros::init(argc,argv,"image_smusher");
  ImageSmush is;
  ros::spin();
  return 0;
}
