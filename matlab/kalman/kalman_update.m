% inputs:
%   u       : motion command (relative displacement)
%   x, P    : position estimate
%   z_sim   : measurements
%   b       : beacon locations
%   A, B, Q : linearized motion model
%   J, R    : linearized measurement model
% outputs:
%   x, P    : new position estimate
%   q       : measurement covariance
%   zdiff   : difference between predicted and actual sensor values

function [x, P, q, zdiff] = kalman_update(u, x, P, z_sim, b, A, B, Q, R, jdir)
    
    if nargin < 10
        jdir = 'forwards';
    end

    % motion update
    x = A*x + B*u;
    P = A*P*transpose(A) + Q;

    % sensor update
    z = measurement(x, b); % expected measurements

    % calculate Jacobian
    if strcmp(jdir, 'forwards')
        J = meas_jacob(x, b);
    else
        J = -meas_jacob(x, b);
    end

    % update beliefs
    q = J*P*transpose(J) + R;
    K = P*transpose(J)/q;
    
    zdiff = zeros(size(z));
    for i=1:length(z)
        zdiff(i) = z_sim(i) - z(i);
        if mod(i, 2) == 0
            if zdiff(i) > pi
                zdiff(i) = zdiff(i) - 2*pi;
            elseif zdiff(i) < -pi
                zdiff(i) = zdiff(i) + 2*pi;
            end
        end
    end
    x = x + K*zdiff;
    P = P - K*J*P;
    P(2,1) = P(1,2); % force symmetry
