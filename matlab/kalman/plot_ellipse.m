function H = plot_ellipse(mu, E, color)

[V, D] = eig(E);
N = 65;
theta = linspace(0,2*pi,N);

pts = V * [ D(1,1)*cos(theta) ; D(2,2)*sin(theta) ] + repmat(mu, 1, N);

H = plot(pts(1,:), pts(2,:), strcat(color, '-'));
