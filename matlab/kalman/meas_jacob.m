% inputs:
%   x   : position
%   b   : beacon locations
% outputs:
%   J   : measurement model jacobian evaluated at x

% if length(x) == 3, then x(3) is theta, and the jacobian will be 2x3

function J = meas_jacob(x, b, d)

if nargin == 2
    d = 1;
elseif nargin == 3 && d == 'backwards'
    d = -1;
end

J = zeros(size(b,2)*2, 2);

for i=1:2:(size(b,2)*2)
    j = (i+1)/2;
    xr = x(1); yr = x(2); xb = b(1,j); yb = b(2,j);

    J(i,1) = d*(xr - xb)/sqrt((xb - xr)^2 + (yb - yr)^2);
    J(i,2) = d*(yr - yb)/sqrt((xb - xr)^2 + (yb - yr)^2);

    %J(i+1,1) = (yb - yr)/((xb - xr)^2*((yb - yr)^2/(xb - xr)^2 + 1));
    %J(i+1,2) = -1/((xb - xr)*((yb - yr)^2/(xb - xr)^2 + 1));
    u = xb - xr;
    v = yb - yr;
    J(i+1,1) = d*v/(u^2 + v^2);
    J(i+1,2) = d*-u/(u^2 + v^2);
end
    
