% inputs:
%   x, P  : position estimate
%   x_sim : actual position
%   b     : beacon locations

function kalman_plot(x, P, x_sim, b)

plot_ellipse(x, P, 'b'); % plot 60% confidence interval
hold on
for bi=1:size(b,2)
    plot(b(1,bi), b(2,bi), 'go');
end
plot(x_sim(1), x_sim(2), 'ro');
hold off
axis([-20 120 -20 120]);
pause(0.1);
    