places = [  8 12 80 80 ;
           80 80 10 80 ];

for t=1:size(places,2)

    %b = [ 10 90 20 70 20 10 ; 
    %      10 90 40 30  5 80 ];
    b = [ 10 10 ;
          10 80 ];
    x = places(:,t);
    h = 0.00000001;

    J = meas_jacob(x, b);

    J2 = zeros(size(J));
    for i=1:2
        xx = x;
        xx(i) = xx(i) - h;
        z0 = measurement(xx, b);
        xx(i) = xx(i) + 2*h;
        z1 = measurement(xx, b);
        for j=1:(size(b,2)*2)
            J2(j,i) = (z1(j) - z0(j))/(2*h);
        end
    end

    x
    [J J2 abs(J - J2)]
end
