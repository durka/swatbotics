% inputs:
%   x   : position
%   b   : actual beacon locations
% outputs:
%   z   : measurements

function z = measurement(x, b)

z = zeros(size(b,2)*2,1);
for i=1:2:(size(b,2)*2)
    j = (i+1)/2;
    z(i) = norm(x(1:2) - b(:,j));
    z(i+1) = atan2(b(2,j) - x(2), b(1,j) - x(1));
    if length(x) == 3 % we have bearing, so do relative angle
        z(i+1) = rem(z(i+1) - x(3), 2*pi);
        if z(i+1) > pi
            z(i+1) = z(i+1) - 2*pi;
        end
    end
end
