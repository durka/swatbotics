

%% initial conditions
% beacon positions in x (top row) and y (bottom)
b = [ 10 90 20 70 20 10 ; 
      10 90 40 30  5 80 ];
% position of simulated robot 
x_sim = [ 1 ; 10  ];
x = x_sim;
P = 2*Q;

%% model parameters
A = eye(2);
B = eye(2);
Q = 4*eye(2);
R = zeros(size(b,2)*2); % 2x size of b
for i=1:size(R,1)
    if mod(i,2) == 0
        R(i,i) = (15*pi/180)^2;
    else
        R(i,i) = 5;
    end
end

for t=1:100000000
    u = [0; 0];
    
    %% simulation
    if mod(t,50) == 0
        disp 'kidnap!'
        x_sim = rand(2,1)*100;
    else
        x_sim = A*x_sim + B*u + transpose(mvnrnd(zeros(2,1), Q));
    end
    z_sim = measurement(x_sim, b);
    z_sim = z_sim + transpose(mvnrnd(zeros(size(z_sim)), R));
    
    % out of bounds
    for i=1:2
        x_sim(i) = max(x_sim(i), 0);
        x_sim(i) = min(x_sim(i), 100);
    end
    
    [x, P] = kalman_update(u, x, P, z_sim, b, A, B, Q, R);
    
    kalman_plot(x, P, x_sim, b);
end
