%% tracing

fparticles = fopen('particles.trace', 'w');
fsensors = fopen('sensors.trace', 'w');
fsamples = fopen('samples.trace', 'w');
fcommands = fopen('commands.trace', 'w');

%% parameters
N = 100;
xh = 100; yh = 100; % world boundaries

% unknown feature weight
po = 0.001;

% sensor hardware parameters
sensor_range = 60; % meters
sensor_fov = 180*pi/180; % cone radius

% beacon positions in x (top row) and y (bottom)
b = [ 10 90 20 70 20 10 ; 
      10 90 40 30  5 80 ];
Nb = size(b,2);

R = diag([2^2, (3*pi/180)^2]); % sensor model covariance
S = diag([1, 1, (2*pi/180)^2]); % motion model covariance

x_sim = [ 5; 10; 0 ];

%% initialize particles
particles = struct('x',   num2cell([linspace(5, (xh-5), N); ...
                                    linspace(5, (yh-5), N); ...
                                    linspace(5, 355, N)*pi/180], 1), ...
                   'map', []);
for i=1:N
    particles(i).x(1:2) = rand(2,1)*100;
    particles(i).x(3) = rand*(2*pi);
    %for j=1:length(particles(i).map)
    %    particles(i).map(j).mu = transpose(mvnrnd(particles(i).map(j).mu, 20*particles(i).map(j).sigma));
    %end
end
weights = zeros(N,1);
weights(1) = 1;


%% particle filter
u = [2; 0];
t = 0;
while true
    tic
    t = t + 1;
    
    for i=1:N
        %                     t  i  w  x  y th Nb map...
        %fprintf(fparticles, '%d %d %f %f %f %f %d', t-1, i-1, weights(i), particles(i).x(1), particles(i).x(2), particles(i).x(3), length(particles(i).map));
        for j=1:length(particles(i).map)
            %                     mx my c00 c01 c10 c11
            %fprintf(fparticles, ' %f %f %f %f %f %f', particles(i).map(j).mu(1), particles(i).map(j).mu(2),...
                                                       %particles(i).map(j).sigma(1,1), particles(i).map(j).sigma(1,2), particles(i).map(j).sigma(2,1), particles(i).map(j).sigma(2,2));
        end
        %fprintf(fparticles, '\n');
    end
    
    % go in a diamond-ish shape
    u = [2; 0];
    if x_sim(1) < 5
        if x_sim(3) > 0 && x_sim(3) < pi
            u(2) = -pi/2;
        else
            u(2) = pi/2;
        end
    elseif x_sim(1) > 95
        if x_sim(3) > 0 && x_sim(3) < pi
            u(2) = pi/2;
        else
            u(2) = -pi/2;
        end
    end
    if x_sim(2) < 5
        if x_sim(3) < pi/2 && x_sim(3) > 3*pi/2
            u(2) = -pi/2;
        else
            u(2) = pi/2;
        end
    elseif x_sim(2) > 95
        if x_sim(3) < pi/2 && x_sim(3) > 3*pi/2
            u(2) = pi/2;
        else
            u(2) = -pi/2;
        end
    end
    %                    t ux uy
    %fprintf(fcommands, '%d %g %g\n', t, u(1), u(2));
    
    %% simulation
    if false && mod(t,20) == 0
        disp 'kidnap!'
        x_sim(1:2) = rand(2,1)*100;
        x_sim(3) = rand*(2*pi);
    else
        x_sim(3) = rem(x_sim(3) + u(2), 2*pi);
        x_sim(1) = x_sim(1) + u(1)*cos(x_sim(3));
        x_sim(2) = x_sim(2) + u(1)*sin(x_sim(3));
        x_sim = transpose(mvnrnd(x_sim, S));
    end
    
    % choose beacons to measure in this timestep (within sensor range/fov)
    z = measurement(x_sim, b);
    z_sim = [];
    j_sim = [];
    for j=1:Nb
        if z(j*2-1) <= sensor_range && abs(z(j*2)) <= sensor_fov
            z_sim = [z_sim; transpose(mvnrnd(z((j*2-1):(j*2)), R))];
            j_sim = [j_sim j];
        end
    end
    
    %                   t Nb
    %fprintf(fsensors, '%d %d', t-1, length(j_sim));
    for j=1:length(j_sim)
        %                    r th
        %fprintf(fsensors, ' %g %g', z_sim(j*2-1), z_sim(j*2));
    end
    %fprintf(fsensors, '\n');
    
    u = transpose(mvnrnd(u, diag([S(1,1) S(3,3)])));
    
    %% motion/map update
    weights = ones(N,1);
    for i=1:N
        particles(i).x(3) = rem(particles(i).x(3) + u(2), 2*pi);
        particles(i).x(1) = particles(i).x(1) + u(1)*cos(particles(i).x(3));
        particles(i).x(2) = particles(i).x(2) + u(1)*sin(particles(i).x(3));
        particles(i).x = transpose(mvnrnd(particles(i).x, S));
        
        % incorporate everything that was measured in this timestep
        j_obs = [];
        for s=1:length(j_sim)
            z = z_sim((s*2-1):(s*2));
            likelihood = zeros(length(particles(i).map),1);
            for j=1:length(particles(i).map)
                zhat = measurement(particles(i).x, particles(i).map(j).mu);
                zdiff = z - zhat;
                for k=1:length(zhat)
                    if mod(k, 2) == 0
                        if zdiff(k) > pi
                            zdiff(k) = zdiff(k) - 2*pi;
                        elseif zdiff(k) < -pi
                            zdiff(k) = zdiff(k) + 2*pi;
                        end
                    end
                end
                H = meas_jacob(particles(i).x, particles(i).map(j).mu, 'backwards');
                likelihood(j) = mvnpdf(zdiff, [0; 0], H*particles(i).map(j).sigma*H' + R);
            end
            [m, j] = max(likelihood);
            j_obs = [j_obs j];

            if isempty(j) || m < po % new feature!
                j = length(particles(i).map)+1;

                % invert measurement function
                d = z(1);
                th = rem(z(2) + particles(i).x(3), 2*pi);
                x = particles(i).x(1);
                y = particles(i).x(2);
                particles(i).map(j).mu = [ x + d*cos(th);
                                           y + d*sin(th) ];
                H = meas_jacob(particles(i).x, particles(i).map(j).mu, 'backwards');
                %particles(i).map(j).sigma = transpose(inv(H))*(R/H);
                particles(i).map(j).sigma = (H\R)*transpose(inv(H));
                particles(i).map(j).odds = 1;
            else
                zhat = measurement(particles(i).x, particles(i).map(j).mu);
                H = meas_jacob(particles(i).x, particles(i).map(j).mu, 'backwards'); 

                meas_covar = H*particles(i).map(j).sigma*H' + R;
                K = particles(i).map(j).sigma*H'/meas_covar;
                zdiff = z - zhat;
                for k=1:length(zhat)
                    if mod(k, 2) == 0
                        if zdiff(k) > pi
                            zdiff(k) = zdiff(k) - 2*pi;
                        elseif zdiff(k) < -pi
                            zdiff(k) = zdiff(k) + 2*pi;
                        end
                    end
                end

                particles(i).map(j).mu = particles(i).map(j).mu + K*zdiff;
                particles(i).map(j).sigma = (eye(2)-K*H)*particles(i).map(j).sigma + S(1:2,1:2);
                particles(i).map(j).odds = particles(i).map(j).odds + 1;
            end
        end
        
        % check for bad features
        map = particles(i).map(1:0); % hack to create a zero-size struct array
        for k=1:length(particles(i).map)
            
            if isempty(find(j_obs == k, 1))
                % expand covariance of unobserved features
                particles(i).map(k).sigma = particles(i).map(k).sigma + 0.25*S(1:2,1:2);
                
                % decrease confidence on existence of features that should have
                % been observed but weren't
                z = measurement(particles(i).x, particles(i).map(k).mu);
                if z(1) <= sensor_range && abs(z(2)) <= sensor_fov
                    particles(i).map(k).odds = particles(i).map(k).odds - 1;
                end
            end
            
            % only keep features that exist
            if particles(i).map(k).odds >= 0
                map(end+1) = particles(i).map(k);
            end
        end
        particles(i).map = map;
        
        if isempty(m)
            weights(i) = weights(i) * po;
        else
            weights(i) = weights(i) * m;
        end
    end
    
    weights = weights/sum(weights);
    
    %% dump
    
    disp(sprintf('x (%g, %g, %g)', x_sim(1), x_sim(2), x_sim(3)*180/pi));
    for i=1:N
        s = sprintf('%d(%g)\t(%g, %g, %g)\t{', i, weights(i), particles(i).x(1), particles(i).x(2), particles(i).x(3)*180/pi);
        for j=1:length(particles(i).map)
            s = strcat(s, sprintf(' (%g, %g)', particles(i).map(j).mu(1), particles(i).map(j).mu(2)));
        end
        s = strcat(s, ' }');
        disp(s)
    end
    
    %% resample
    if length(j_sim) > 0
        new_particles = particles;
        new_weights = weights;
        %                   t
        %fprintf(fsamples, '%d', t-1);
        for i=1:N
            if i <= N%-20
                k = sample_weighted(weights);
                new_particles(i) = particles(k);
                new_weights(i) = weights(k);
                
                %                    i  k
                %fprintf(fsamples, ' %d %d', i-1, k-1);
            else
                new_particles(i).x(1:2) = rand(2,1)*100;
                new_particles(i).x = rand*(2*pi);
                new_particles(i).map = [];
                new_weights(i) = 0;
            end
        end
        %fprintf(fsamples, '\n');
        particles = new_particles;
        weights = new_weights;
    end
    
    %% correct origin
    uncheated_particles = particles;
    T = @(x) [ cos(x(3)) -sin(x(3)) x(1) ; sin(x(3)) cos(x(3)) x(2) ; 0 0 1 ];
    for k=1:N
        %delta = particles(k).x - x_sim;
        delta = T(x_sim) * inv(T(particles(k).x));
        rot = delta(1:2,1:2);
        trans = delta(1:2,3);
        for j=1:length(particles(k).map)
            particles(k).map(j).mu = rot*particles(k).map(j).mu + trans;
            particles(k).map(j).sigma = rot' * particles(k).map(j).sigma * rot;
            %d = norm(particles(k).map(j).mu - particles(k).x(1:2));
            %t = atan2(particles(k).map(j).mu(2), particles(k).map(j).mu(1)) - particles(k).x(3);
            %particles(k).map(j).mu = x_sim(1:2) + d*[cos(t); sin(t)];
        end
        particles(k).x = x_sim;
    end
    
    %% plot
    [w,i] = max(weights);

    disp(particles(i).x)
    for k=1:length(particles(i).map)
        disp(particles(i).map(k).mu - particles(i).x(1:2))
    end

    clf
    hold on
    plot(x_sim(1), x_sim(2), 'ro');
    plot([x_sim(1) x_sim(1)+3*cos(x_sim(3))], [x_sim(2) x_sim(2)+3*sin(x_sim(3))], 'r');
    plot(particles(i).x(1), particles(i).x(2), 'bx');
    plot([particles(i).x(1) particles(i).x(1)+3*cos(particles(i).x(3))], [particles(i).x(2) particles(i).x(2)+3*sin(particles(i).x(3))], 'b');
    for k=1:length(particles(i).map)
        plot(particles(i).map(k).mu(1), particles(i).map(k).mu(2), 'go');
        plot_ellipse(particles(i).map(k).mu, particles(i).map(k).sigma, 'g');
    end
    for k=1:Nb
        if ~isempty(find(j_sim == k, 1))
            plot(b(1,k), b(2,k), 'kx', 'MarkerSize', 10);
        else
            plot(b(1,k), b(2,k), 'rx');
        end
    end
    title(sprintf('t = %d', t));
    hold off
    %axis equal
    axis([-20 120 -20 120]);

    particles = uncheated_particles;
    
    %pause
    pause(0.05 - toc);
end
