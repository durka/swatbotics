% x is a 2x1 position vector
% b is a 2xN vector of beacon positions
% sigma is uncertainty in range measurements
% returns an of N normally distributed range measurements
function z = sample_measurement(x, grid, sigma)

%Get the true level at x

if mod(x(1), 0.5)==0 || mod(x(2), 0.5)==0
    v = interp2(grid, x(1), x(2));
else  
    i=round(x(1));
    j=round(x(2));
    if i==0
        i=1;
    end
    if j==0
        j=1;
    end
    v = grid(i, j, :);
end

z = normrnd(v, sigma);