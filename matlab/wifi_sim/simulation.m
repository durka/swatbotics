%Wireless signal strength particle filter simulation

%robot size: 1 meter (pixel)
s=5; %grid cell size = 5 meters (pixels)
N=3; %Number of networks we are collecting data from

img = imread('map.jpg');

grid = zeros(size(img,1)*s, size(img,2)*s, N);

%transfer image data to grid

for j=1:size(img,2)
    for i=1:size(img,1)
        temp = img(i,j,:);
        for k=s*(j-1)+1:j*s
            for l=s*(i-1)+1:i*s
                grid(k, l, :) = temp;
            end
        end
    end
end

k = 500; %number particles

sigma_x = 2;
sigma_z = 5;
u_cur = [0;0];
x_sim = [50;50]; %arbitrary initial position

pointsize = 1;

numrnd = 40;

particles = zeros(2,k);

for i=1:k
  particles(:,i) = sample_motion(x_sim, [0;0], 100*sigma_x, grid);

  %fprintf('particles[%d]=[%f, %f]\n', i, particles(1,i), particles(2,i))
  %particles(:,i) = 100*rand(2,1);
end

% initialize weights (really, unused until inside the loop)
weights = ones(k,1)/k;


plot(particles(1,:), particles(2,:), 'k.', 'MarkerSize', pointsize);
axis equal off

t=0;

while 1
  %Update particles according to motion model
  for i=1:k
      particles(:,i) = sample_motion(particles(:,i), u_cur, sigma_x, grid);
  end
  
  %Update true position according to motion model
  x_sim = sample_motion(x_sim, u_cur, sigma_x, grid);
  %Get measurement
  z_sim = sample_measurement(x_sim, grid, sigma_z);
  
  %Weight each particle according to measurement model
  weights = zeros(k,1);
  for i=1:k
    weights(i) = measurement(z_sim, particles(:,i), grid, sigma_z);
  end
  weights = weights / sum(weights);
  
  new_particles = zeros(size(particles));
  
  for i=1:numrnd
    new_particles(1,i) = rand()*size(grid,1);
    new_particles(2,i) = rand()*size(grid,2);
  end
  
  for i=numrnd+1:k
    j = sample_weighted(weights);
    new_particles(:,i) = particles(:,j);
  end
  
  particles = new_particles;
  
  
%   xij = [0;0];
%   
%   for i=1:size(x1,1)
%     for j=1:size(x1,2)
%       xij(1) = x1(i,j);
%       xij(2) = x2(i,j);
%       vis_prob(i,j) = measurement(z_sim, xij, grid, sigma_z);
%     end
%   end
%   
%   imagesc(vis_range, vis_range, vis_prob);
  
  hold on
  plot(particles(1,:), particles(2,:), 'k.', 'MarkerSize', pointsize);
  plot(x_sim(1), x_sim(2), 'mo')
  hold off
  axis([0 size(grid, 1) 0 size(grid, 2)])
  axis equal off
  title(sprintf('t = %d', t));
  pause(0.1)
  t = t + 1;
  cla
  
end