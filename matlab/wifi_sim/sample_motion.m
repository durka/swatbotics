% sample motion according to motion model
% x_prev is a 2x1 vector
% u_cur is a 2x1 desired motion
% sigma is the uncertainty
function x_cur = sample_motion(x_prev, u_cur, sigma, grid)

x_cur = normrnd(x_prev + u_cur, sigma);

s = size(grid);

x_cur = abs(x_cur);

if x_cur(1)>s(1)
    x_cur(1)=mod(x_cur(1), s(1));
end
if x_cur(2)>s(2)
    x_cur(2)=mod(x_cur(2), s(2));
end

