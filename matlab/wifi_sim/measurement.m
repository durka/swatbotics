%measurement model for wifi signals
% z is an Nx1 vector of measurements
% x is a 2d vector
% grid is the 3-channel matrix of signal strengths
% p_z is a scalar which is the product of individual measurement probabilities

function p_z = measurement(z, x, grid, sigma)

p_z = 1;

if mod(x(1), 0.5)==0 || mod(x(2), 0.5)==0
    v = interp2(grid, x(1), x(2));
else  
    i=round(x(1));
    j=round(x(2));
    if i==0
        i=1;
    end
    if j==0
        j=1;
    end
    v = grid(i, j, :);
end

for i=1:length(z)
  p_zi = normpdf(z(i), double(v(i)), sigma);
  p_z = p_z * p_zi;
end
