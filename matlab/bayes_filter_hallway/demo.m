% demonstrate our bayes filter

% 32 possible states
N = 32;

% sensor parameters - door locations, sigma, scale factor
door_locations = [ 5, 18, 25 ];
sigma = 1;
scl = 0.9;

% initialize state belief to be in one place

x_sim = 1;

bel_x = zeros(N, 1);
bel_x(x_sim) = 1;

u_cur = 1;

do_measurement = 1;

t = 0;

while 1
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % bayes filter step 1: generate our posterior belief after motion model
  
  bel_x_bar = zeros(N, 1);
  
  % for each previous state
  for x_prev=1:N
    % get the probability for each cur state
    p_x_cur = motion(u_cur, x_prev, N);
    for x_cur=1:N
      % and integrate it in
      bel_x_bar(x_cur) = bel_x_bar(x_cur) + bel_x(x_prev) * p_x_cur(x_cur);
    end
  end
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % sample from motion model (for sim)
  
  p_x_sim_next = motion(u_cur, x_sim, N);
  x_sim = sample_motion(p_x_sim_next);
  
  x_sim
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  % handle measurement
    
  if (~do_measurement)
    
    bel_x = bel_x_bar;
    
  else
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % sample from our measurement model

    p_z_sim = measurement(x_sim, door_locations, sigma, scl);
    z_sim = sample_measurement(p_z_sim);
    
    if (z_sim)
      disp('beeep')
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % bayes filter step 2: combine measurement model
    % to refine belief
    
    for x_cur = 1:N
      p_z = measurement(x_cur, door_locations, sigma, scl);
      if (~z_sim)
        p_z = 1-p_z;
      end
      bel_x(x_cur) = p_z * bel_x_bar(x_cur);
    end
    
  end
  
  % normalize belief (necessary after step 2)
  bel_x = bel_x / sum(bel_x);

  % advance time
  t = t + 1;

  % plot some stuff
  bar(bel_x);
  axis([1, N, 0, 1])
  hold on
  plot([x_sim], [0], 'mo')
  hold off
  title(sprintf('state at t=%d', t))
  pause(0.1)

end

