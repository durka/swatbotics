% freq is a histogram of states
freq = zeros(7, 1);

% p holds probabilities of being in a state
p = zeros(7, 1);
p(1) = 0.5;
p(7) = 0.5;

% try sampling from p 100 times and make sure that the
% first and last bin are sampled roughly equally
for i=1:100
  x = sample_motion(p);
  freq(x) = freq(x)+1;
end

freq
bar(freq);

