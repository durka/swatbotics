% draw a sample from the motion model.
% this is simple "roulette wheel" sampling.
%
% p_x is a vector of probabilities that sums to one
% the x returned is sampled from the distribution p_x.
function x = sample_motion(p_x)

N = length(p_x);
r = rand();

cumsum = 0;

for i=1:N
  cumsum = cumsum + p_x(i);
  if (cumsum > r)
    x = i;
    return
  end
end

x = N;

