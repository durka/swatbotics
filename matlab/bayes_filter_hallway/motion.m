% motion model for the robot. 
%
% the robot is in a "circular" hallway with N different locations
% traveling to the right of the Nth location brings it back to
% location 1.
%
%
% u_cur is an integer: -1, 0, 1
% x_prev is an integer from 1 to N inclusive
% N is the total number of states
function p_x_cur = motion(u_cur, x_prev, N)

p_x_cur = zeros(N, 1);

i = x_prev + u_cur;

wrap = @(i,N) mod(i-1+N,N)+1;

% 50% chance of getting to destination, 25% on either side
p_x_cur(wrap(i,N)) = 0.5;
p_x_cur(wrap(i+1,N)) = 0.25;
p_x_cur(wrap(i-1,N)) = 0.25;

p_x_cur = p_x_cur / sum(p_x_cur);


