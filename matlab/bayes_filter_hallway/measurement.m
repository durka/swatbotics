% measurement model for the robot
%
% the robot has a "door detector" which beeps with a certain
% probability when the robot is close to doors
%
% the "map" of door locations is known. the probability of
% beeping near a door is roughly gaussian.

function p_z = measurement(x_cur, door_locations, sigma, scl)

p_z = 0;
cnt = length(door_locations);

for i=1:cnt
  door_i = door_locations(i);
  p_z = p_z + exp(-(x_cur - door_i)^2 / (2*sigma^2)) * scl;
end
