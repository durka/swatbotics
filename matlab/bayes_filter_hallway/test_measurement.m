% test our measurement model by plotting probability of doorway
% detector going off at each location

% 32 possible states
N = 32;

% place some doors at state 18 and 25
door_locations = [ 18, 25 ];
sigma = 1;
scl = 0.9;

% get the probability of door detection at each state
p_all_z = zeros(N, 1);
for i=1:N
  p_all_z(i) = measurement(i, door_locations, sigma, scl);
end
bar(p_all_z);

