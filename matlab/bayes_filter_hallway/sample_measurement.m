% sample a binary door detector measurement given 
% a probability of detection
%
% p_z is percent chance of siren going off
% and z = 1 indicates siren went off
function z = sample_measurement(p_z)

r = rand();
z = (r <= p_z);

