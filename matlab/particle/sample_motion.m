% sample motion according to motion model
% x_prev is a 2x1 vector
% u_cur is a 2x1 desired motion
% sigma is the uncertainty
function x_cur = sample_motion(x_prev, u_cur, sigma)

x_cur = normrnd(x_prev + u_cur, sigma);

