% z is an Nx1 vector of measurements
% x is a 2d vector
% b is a 2xN matrix of beacon positions
% p_z is a scalar which is the product of individual measurement probabilities

function p_z = measurement(z, x, b, sigma)

p_z = 1;

for i=1:length(z)
  true_range = norm(x - b(:,i));
  p_zi = normpdf(z(i), true_range, sigma);
  p_z = p_z * p_zi;
end

