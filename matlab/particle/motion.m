% x_cur  is a 2x1 vector
% x_prev is a 2x1 vector
% u_cur  is a 2x1 vector
% sigma is a scalar (std. dev of gaussian)
% p_x_cur is a scalar

function p_x_cur = motion(x_cur, x_prev, u_cur, sigma)

mu = x_prev + u_cur;
p_x_cur = prod(normpdf(x_cur, mu, sigma));


