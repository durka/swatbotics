% x is a 2x1 position vector
% b is a 2xN vector of beacon positions
% sigma is uncertainty in range measurements
% returns a vector of N normally distributed range measurements
function z = sample_measurement(x, b, sigma)

N = size(b, 2);
mu = sqrt(sum((b - repmat(x, 1, N)).^2));

z = normrnd(mu, sigma);

