% particle filter demo
%
% a robot wanders randomly in a roughly 100-by-100 m space, among
% several beacons. the robot can sense (with substantial error) the
% range to each beacon.
%
% this code demonstrates using a particle filter to localize the
% robot.

% beacon positions in x (top row) and y (bottom)
b = [ 10 90 20 70 ; 
      10 90 40 30 ]

% uncertainty on beacon range measurement
sigma_z = 10;

% uncertainty on robot motion
sigma_x = 1;

% position of simulated robot 
x_sim = [ 40 ; 60  ];

% number of particles
k = 1000;

% number of particles sampled from uniform random distribution at
% each time step
numrnd = 40;

% point size for drawing
pointsize = 1;

% initialize k particles
particles = zeros(2,k);

for i=1:k
  %particles(:,i) = sample_motion(x_sim, [0;0], 100*sigma_x);
  particles(:,i) = 100*rand(2,1);
end

% initialize weights (really, unused until inside the loop)
weights = ones(k,1)/k;


plot(particles(1,:), particles(2,:), 'k.');
axis equal off

% t is timestep
t = 0;

% u is desired displacement, which is 0,0 but robot wanders due to
% sampling motion model
u_cur = [0; 0];

% should we do measurement?
do_measurement = 1;

% this is just used for plotting likelihood of measurements across
% entire space -- not critical to the algorithm
vis_range = 0:2:100;
[x1, x2] = meshgrid(vis_range);
vis_prob = zeros(size(x1));

% loop forever:  
while 1

  % particle filter step 1: update each particle according to the
  % motion model.
  for i=1:k
    particles(:,i) = sample_motion(particles(:,i), u_cur, sigma_x);
  end

  % every 20 steps, "kidnap" the robot by teleporting it
  if (mod(t, 20) == 0)
    x_sim = rand(2,1)*100;
  else
    x_sim = sample_motion(x_sim, u_cur, sigma_x);
  end
  
  if do_measurement

      % get some measurements from our range sensors
      z_sim = sample_measurement(x_sim, b, sigma_z);
      
      % particle filter step 2: weight each particle by the
      % measurement model.
      weights = zeros(k,1);
      for i=1:k
        weights(i) = measurement(z_sim, particles(:,i), b, sigma_z);
      end
      weights = weights / sum(weights);

      % particle filter step 3: resample the particles by choosing
      % proportional to the weights
      
      new_particles = zeros(size(particles));
      new_weights = zeros(size(weights));

      % first do the randomly sampled ones
      for i=1:numrnd
        new_particles(:,i) = rand(2,1)*100;
        new_weights(i) = 0;
      end
      
      % then do the sample_weighted
      for i=numrnd+1:k
        j = sample_weighted(weights);
        new_particles(:,i) = particles(:,j);
        new_weights(i) = weights(j);
      end
      
      particles = new_particles;
      weights = new_weights;

  end

  % the rest is just handling visualization
  
  xij = [0;0];
  
  for i=1:size(x1,1)
    for j=1:size(x1,2)
      xij(1) = x1(i,j);
      xij(2) = x2(i,j);
      vis_prob(i,j) = measurement(z_sim, xij, b, sigma_z);
    end
  end
  
  imagesc(vis_range, vis_range, vis_prob);
  hold on
  plot(particles(1,:), particles(2,:), 'k.', 'MarkerSize', pointsize);
  plot(x_sim(1), x_sim(2), 'mo')
  hold off
  axis([0 100 0 100])
  axis equal off
  title(sprintf('t = %d', t));
  pause(0.1)
  t = t + 1;
  
end
