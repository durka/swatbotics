#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float32.h>

ros::NodeHandle* nh;
ros::Publisher pub;

void CallBack(std_msgs::Float32 msg)
{
  geometry_msgs::Twist out;
  out.linear.x = 0.1;
  out.linear.y = 0.0;
  out.linear.z = 0.0;
  out.angular.x = 0.0;
  out.angular.y = 0.0;
  out.angular.z = -0.005*(msg.data-320.0);
  pub.publish(out);
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "drive_turtle");
  nh = new ros::NodeHandle();
  pub = nh->advertise<geometry_msgs::Twist>("cmd_vel", 1000);
  ros::Subscriber sub = nh->subscribe("LineFollow/xPos",1000, CallBack);
  ros::spin();
  return 0;
}
  
