#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/opencv.hpp>
#include <opencv/highgui.h>
#include "ColorLUT.cpp"
#include <string>

namespace enc = sensor_msgs::image_encodings;

class ImageConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;
  cv::Mat element;
  ColorLUT lookup;
  
public:
  ImageConverter(string data)
    : it_(nh_)
  {

    image_pub_ = it_.advertise("LineDetect", 1);
    image_sub_ = it_.subscribe("/camera/rgb/image_color", 1, &ImageConverter::imageCb, this);
    element=cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3,3));
    lookup.load(data);
/*		if(nh_.getParam("bluetapefile", datafile)){
		} else { cout<<"Bad mojo, dude.\n"; }*/
  }

  ~ImageConverter(){
  
  }

  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {

    // Convert to a openCV image
    cv_bridge::CvImagePtr cv_ptr;
    cv_bridge::CvImage cv_new;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, enc::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
    /*Grab image, convert to yuv, mask 
	*/
    // Turn image to ycbcr, then pick only the cb channel
    const cv::Mat& src = cv_ptr->image;
    cv::Mat image; 


    cv::cvtColor(src, image, CV_RGB2YCrCb);
    cv::Mat result;
    lookup.getImageColor(image, lookup.lookupColor("blue_tape"), result );
    cv::imshow("Window", result);
    cv::waitKey(100);
    ROS_INFO("%d", (int) lookup.lookupColor("blue_tape"));
   // cv::morphologyEx(result, result, cv::MORPH_OPEN, element,
		 //    cv::Point(-1,-1), 1, cv::BORDER_REPLICATE);

    // Shove the result back into a image msg so it could be published
    cv_new.image = result;
    cv_new.encoding = sensor_msgs::image_encodings::MONO8;
    cv_new.header = cv_ptr->header;
    image_pub_.publish(cv_new.toImageMsg());
  }
};

int main(int argc, char** argv)
{
  cv::namedWindow("Window");
  string data;
  ros::init(argc, argv, "image_converter");
  if (argc>1){
    data = argv[1];
  }
  ImageConverter ic(data);
  ros::spin();
  return 0;
}
