#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/opencv.hpp>
#include <opencv/highgui.h>
#include "ColorLUT.cpp"
#include <string>

namespace enc = sensor_msgs::image_encodings;
ColorLUT lookup;
ros::Publisher* pub;

void callBack(sensor_msgs::Image msg){
  cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(msg, enc::BGR8);
  cv_bridge::CvImage cv_img;
  cv::Mat img = cv_ptr->image;
  cv::Mat mask(img.rows, img.cols, CV_8U);
  cv::Mat element=cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3,3));
  
  lookup.getImageColor(img, lookup.lookupColor("blue_tape"), mask);
  cv::morphologyEx(mask, mask, cv::MORPH_OPEN, element,
	   cv::Point(-1,-1), 1, cv::BORDER_REPLICATE);
  cv_img.image = mask;
  cv_img.encoding = enc::MONO8;
  cv::imshow("Window", cv_img.image);
  cv::waitKey(10);
  pub->publish(cv_img.toImageMsg());
}

int main(int argc, char** argv)
{
  cv::namedWindow("Window");
  string data;
  ros::init(argc, argv, "image_converter");
  ros::NodeHandle nh;
  pub = new ros::Publisher();
  *pub =  nh.advertise<sensor_msgs::Image>("LineFollow/LineDetect", 1);
  if (argc>1){
    data = argv[1];
    lookup.load(data);
  }
  ros::Subscriber sub = nh.subscribe("camera/rgb/image_color", 1, callBack);
  ros::spin();
  return 0;
}
