#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/opencv.hpp>


ros::NodeHandle* nh;
image_transport::ImageTransport* it;
image_transport::Subscriber image_sub;
ros::Publisher pub;


void CallBack(sensor_msgs::ImageConstPtr msg)
{
  pub = nh->advertise<std_msgs::Float32>("LineFollow/xPos", 1000);

  cv_bridge::CvImagePtr cv_ptr;
  try
  {
    cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }

  // Get the bottom strip of the image as a subimage
  cv::Rect strip(0, 7*cv_ptr->image.rows/8, cv_ptr->image.cols, cv_ptr->image.rows/8);
  cv::Mat subImage(cv_ptr->image, strip); 
  
  // Flip the black and white spots on the image so that we can find 
  // the correct contours
  // cv::bitwise_not(subImage, subImage); 

  // Find countours
  vector<vector<cv::Point> > contours;
  cv::Mat work(subImage.rows, subImage.cols, CV_8UC1);
  subImage.copyTo(work);
  cv::findContours(work, contours, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
  drawContours(subImage, contours, -1, CV_RGB(255,0,0));

  // Find xmean
  float total_moment=0;
  float total_area=0;
  float xmean=1280.0;
  for (int i=0; i<(int)contours.size(); i++)
  {
    cv::Moments m = cv::moments(cv::Mat(contours[i]));
    total_moment += m.m10;
    total_area += m.m00;
  }
  if (total_area!=0.0)
  {
    xmean = total_moment/total_area;
  }
  // publish xmean
  std_msgs::Float32 Xmean_out;
  Xmean_out.data = xmean;
  pub.publish(Xmean_out);

}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "blob_finder");
  nh = new ros::NodeHandle();
  it = new image_transport::ImageTransport(*nh);
  image_sub = it->subscribe("LineFollow/LineDetect", 1, CallBack);
  ros::spin();
  return 0;
}

  
