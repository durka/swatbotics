; Auto-generated. Do not edit!


(cl:in-package turtlesim-srv)


;//! \htmlinclude Line-request.msg.html

(cl:defclass <Line-request> (roslisp-msg-protocol:ros-message)
  ((x1
    :reader x1
    :initarg :x1
    :type cl:float
    :initform 0.0)
   (y1
    :reader y1
    :initarg :y1
    :type cl:float
    :initform 0.0)
   (x2
    :reader x2
    :initarg :x2
    :type cl:float
    :initform 0.0)
   (y2
    :reader y2
    :initarg :y2
    :type cl:float
    :initform 0.0))
)

(cl:defclass Line-request (<Line-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Line-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Line-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name turtlesim-srv:<Line-request> is deprecated: use turtlesim-srv:Line-request instead.")))

(cl:ensure-generic-function 'x1-val :lambda-list '(m))
(cl:defmethod x1-val ((m <Line-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader turtlesim-srv:x1-val is deprecated.  Use turtlesim-srv:x1 instead.")
  (x1 m))

(cl:ensure-generic-function 'y1-val :lambda-list '(m))
(cl:defmethod y1-val ((m <Line-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader turtlesim-srv:y1-val is deprecated.  Use turtlesim-srv:y1 instead.")
  (y1 m))

(cl:ensure-generic-function 'x2-val :lambda-list '(m))
(cl:defmethod x2-val ((m <Line-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader turtlesim-srv:x2-val is deprecated.  Use turtlesim-srv:x2 instead.")
  (x2 m))

(cl:ensure-generic-function 'y2-val :lambda-list '(m))
(cl:defmethod y2-val ((m <Line-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader turtlesim-srv:y2-val is deprecated.  Use turtlesim-srv:y2 instead.")
  (y2 m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Line-request>) ostream)
  "Serializes a message object of type '<Line-request>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'x1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'y1))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'x2))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'y2))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Line-request>) istream)
  "Deserializes a message object of type '<Line-request>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'x1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'y1) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'x2) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'y2) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Line-request>)))
  "Returns string type for a service object of type '<Line-request>"
  "turtlesim/LineRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Line-request)))
  "Returns string type for a service object of type 'Line-request"
  "turtlesim/LineRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Line-request>)))
  "Returns md5sum for a message object of type '<Line-request>"
  "1d74979d8119401281d48677f845994f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Line-request)))
  "Returns md5sum for a message object of type 'Line-request"
  "1d74979d8119401281d48677f845994f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Line-request>)))
  "Returns full string definition for message of type '<Line-request>"
  (cl:format cl:nil "float32 x1~%float32 y1~%float32 x2~%float32 y2~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Line-request)))
  "Returns full string definition for message of type 'Line-request"
  (cl:format cl:nil "float32 x1~%float32 y1~%float32 x2~%float32 y2~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Line-request>))
  (cl:+ 0
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Line-request>))
  "Converts a ROS message object to a list"
  (cl:list 'Line-request
    (cl:cons ':x1 (x1 msg))
    (cl:cons ':y1 (y1 msg))
    (cl:cons ':x2 (x2 msg))
    (cl:cons ':y2 (y2 msg))
))
;//! \htmlinclude Line-response.msg.html

(cl:defclass <Line-response> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass Line-response (<Line-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Line-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Line-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name turtlesim-srv:<Line-response> is deprecated: use turtlesim-srv:Line-response instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Line-response>) ostream)
  "Serializes a message object of type '<Line-response>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Line-response>) istream)
  "Deserializes a message object of type '<Line-response>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Line-response>)))
  "Returns string type for a service object of type '<Line-response>"
  "turtlesim/LineResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Line-response)))
  "Returns string type for a service object of type 'Line-response"
  "turtlesim/LineResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Line-response>)))
  "Returns md5sum for a message object of type '<Line-response>"
  "1d74979d8119401281d48677f845994f")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Line-response)))
  "Returns md5sum for a message object of type 'Line-response"
  "1d74979d8119401281d48677f845994f")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Line-response>)))
  "Returns full string definition for message of type '<Line-response>"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Line-response)))
  "Returns full string definition for message of type 'Line-response"
  (cl:format cl:nil "~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Line-response>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Line-response>))
  "Converts a ROS message object to a list"
  (cl:list 'Line-response
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'Line)))
  'Line-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'Line)))
  'Line-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Line)))
  "Returns string type for a service object of type '<Line>"
  "turtlesim/Line")
