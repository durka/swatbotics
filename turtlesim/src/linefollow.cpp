#include "ros/ros.h"
#include "turtlesim/Velocity.h"
#include "turtlesim/Pose.h"
#include "turtlesim/Line.h"
#include "turtlesim/Pt.h"
#include "turtlesim/path.h"
#include "std_srvs/Empty.h"
#include <wx/wx.h>
#include <string>
#include <vector>

using namespace std;
using namespace ros;
using turtlesim::Velocity;
using turtlesim::Pose;
using turtlesim::Line;
using turtlesim::Pt;
using std_srvs::Empty;

NodeHandle *g_nh;
TurtlePath g_path;
Publisher g_vel_pub;
Publisher g_point_pub;
Publisher g_point2_pub;
Subscriber g_pose_sub;
Subscriber g_path_sub;
ServiceClient g_line_srv;
bool g_following = false;
string g_turtle;

// distance between two points
float dist(Point p1, Point p2, bool squared=false)
{
	float dsqr = pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2);

	return squared ? dsqr : sqrt(dsqr);
}

// distance from point to a line
float dist(Point q, Point p1, Point p2, Point *out, bool squared=false)
{
	Point a ( p2.x - p1.x, p2.y - p1.y );
	Point b ( q.x - p1.x, q.y - p1.y );

	float u = (a.x*b.x + a.y*b.y) / (a.x*a.x + a.y*a.y);

	out->x = p1.x + u*a.x;
	out->y = p1.y + u*a.y;

	if (u > 0 && u < 1)
	{
		return dist(q, *out, squared);
	}
	else
	{
		return INFINITY;
	}
}

Point closest_on_path(TurtlePath path, Point q, int *before=NULL)
{
	float mindist = INFINITY;
	Point minpt;
	int mini = 0;

	float d;
	Point temppt;
	unsigned int i;
	for (i = 0; i < path.size(); ++i)
	{
		// check p_i
		d = dist(q, path[i]);
		if (d < mindist)
		{
			mindist = d;
			minpt = path[i];
			mini = i;
		}

		// check segment p_i-p_i+1
		if (i+1 < path.size())
		{
			d = dist(q, path[i], path[i+1], &temppt);
			if (d != INFINITY && d < mindist)
			{
				mindist = d;
				minpt = temppt;
				mini = i;
			}
		}
	}

	if (before) *before = mini;
	return minpt;
}

Point add_on_path(TurtlePath path, Point q, unsigned int before, float d)
{
	// wrap around at end of path?
	if (before+1 == path.size())
	{
		return add_on_path(path, path[0], 0, d - dist(q, path[before]));
	}

	// add along segment
	Point
		pi = path[before],
		pj = path[before+1],
		a = norm(pj - pi),
		end = q + a*d;
	
	// fell off end of segment?
	if (dist(pi, end) > dist(pi, pj))
	{
		return add_on_path(path, pj, before+1, d - dist(q, pj));
	}

	return end;
}

Point rotate(float theta, Point p)
{
	float c = cos(theta), s = sin(theta);

	return Point(c*p.x - s*p.y, s*p.x + c*p.y);
}

Point to_robot_frame(Point p, Pose pose)
{
	return rotate(-pose.theta, p - Point(pose.x, pose.y));
}

void pose_callback(const Pose::ConstPtr &msg)
{
	const float alpha = 1.25;
	const float cx = .5, cth = 1.5;

	//ROS_INFO("Turtle at (%g, %g, %g)", msg->x, msg->y, msg->theta);

	// calculate goal
	int i;
	Point q(msg->x, msg->y);
	Point p = closest_on_path(g_path, q, &i);
	Point g = add_on_path(g_path, p, i, alpha);

	/*// cross off passed point(s)
	if (i > 2)
	{
		g_path = TurtlePath(g_path.begin() + i-2, g_path.end());
		ROS_INFO("Crossed off points 0-%d", i);
	}*/

	if (dist(p, g_path[g_path.size()-1]) < 0.1)
	{
		ROS_INFO("Turtle reached goal!");
		g_path = TurtlePath(g_path.end() - 1, g_path.end());
		g_pose_sub.shutdown();
		g_following = false;
		return;
	}

	//ROS_INFO("Turtle is closest to (%g, %g) on the path", p.x, p.y);
	//ROS_INFO("Turtle heading for (%g, %g)", g.x, g.y);
	Pt pt;
	pt.x = p.x;
	pt.y = p.y;
	g_point_pub.publish(pt);
	pt.x = g.x;
	pt.y = g.y;
	g_point2_pub.publish(pt);

	// give commands
	Velocity vel;
	vel.linear = cx;
	vel.angular = to_robot_frame(g, *msg).y * cth;
	//ROS_INFO("Command = (%g, %g)", vel.linear, vel.angular);
	g_vel_pub.publish(vel);
}

void path_callback(const Pt::ConstPtr &msg)
{
	if (g_path.size())
	{
		g_path.push_back(Point(msg->x, msg->y));
	
		Line srv;
		srv.request.x1 = g_path[g_path.size()-2].x;
		srv.request.y1 = g_path[g_path.size()-2].y;
		srv.request.x2 = g_path[g_path.size()-1].x;
		srv.request.y2 = g_path[g_path.size()-1].y;

		if (g_line_srv.call(srv))
		{
			ROS_INFO("Added (%g, %g) to path", msg->x, msg->y);
		}
		else
		{
			ROS_ERROR("Failed to add (%g, %g) to path", msg->x, msg->y);
		}

		if (!g_following)
		{
			g_pose_sub = g_nh->subscribe(g_turtle + "/pose", 1000, pose_callback);
			g_following = true;
			ROS_INFO("Restarted line follower");
		}
	}
	else
	{
		ROS_INFO("Initializing path with (%g, %g)", msg->x, msg->y);
		g_path.resize(1);
		g_path[0] = Point(msg->x, msg->y);
	}
}

int main(int argc, char *argv[])
{
	init(argc, argv, "linefollow");

	ROS_INFO("Line follower started");

	// publish velocity commands
	g_turtle = string(argv[1]);
	g_nh = new NodeHandle();
	g_vel_pub = g_nh->advertise<Velocity>(g_turtle + "/command_velocity", 1000);
	g_point_pub = g_nh->advertise<Pt>("/point", 1000);
	g_point2_pub = g_nh->advertise<Pt>("/point2", 1000);
	g_path_sub = g_nh->subscribe("/path", 1000, path_callback);
	g_line_srv = g_nh->serviceClient<Line>("/line");

	ServiceClient client = g_nh->serviceClient<Empty>("/clear");
	Empty e;
	client.call(e);
	ServiceClient client2 = g_nh->serviceClient<Empty>("/reset");
	client2.call(e);

	spin();

	delete g_nh;
	return 0;
}

