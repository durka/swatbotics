#include "ros/ros.h"
#include "turtlesim/Pt.h"
#include "turtlesim/path.h"
#include <vector>
#include <iostream>
#include <string>
#include <sstream>

using namespace std;
using namespace ros;
using turtlesim::Pt;

void show_path(TurtlePath path)
{
	for (unsigned i = 0; i < path.size(); ++i)
	{
		cout << setw(3) << i << ": " << path[i] << endl;
	}
}

void send(Publisher path_pub, Point pt)
{
	Pt msg;
	msg.x = pt.x;
	msg.y = pt.y;

	path_pub.publish(msg);
	cout << "Sending " << pt << endl;
}

int main(int argc, char *argv[])
{
	TurtlePath path;

	if (argc > 1)
	{
		if (!(argc % 2))
		{
			cerr << "Arguments must be some coordinate pairs." << endl;
			return 1;
		}

		for (int i = 1; i < argc; i += 2)
		{
			path.push_back(Point(atoi(argv[i]), atoi(argv[i+1])));
		}
	}

	init(argc, argv, "sendpath");
	NodeHandle nh;
	Publisher path_pub = nh.advertise<Pt>("/path", 1000);

	string input;
	float x, y;
	while (true)
	{
		cout << "> ";
		getline(cin, input);
		
		if (input == "quit")
		{
			break;
		}

		if (input == "p")
		{
			for (TurtlePath::iterator i = path.begin(); i != path.end(); ++i)
			{
				send(path_pub, *i);
			}
			continue;
		}

		if (input == "u")
		{
			path.pop_back();
			continue;
		}

		if (input == "s")
		{
			show_path(path);
			continue;
		}
		
		stringstream parser(input);
		parser >> x >> y;
		path.push_back(Point(x, y));
		send(path_pub, Point(x, y));
	}

	return 0;
}

