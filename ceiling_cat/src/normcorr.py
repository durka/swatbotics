#!/usr/bin/env python

import sys
import cv

if __name__ == '__main__':
    temp = cv.LoadImage(sys.argv[1])
    tempbw = cv.CreateMat(temp.height, temp.width, cv.CV_8UC1)
    big = cv.CreateMat(temp.height, temp.width, cv.CV_32FC1)
    res = cv.CreateMat(big.height-480+1, big.width-640+1, cv.CV_32FC1)

    cv.CvtColor(temp, tempbw, cv.CV_BGR2GRAY)
    cv.Laplace(tempbw, big, 11)
    
    for i in range(2, len(sys.argv)):
        temp = cv.LoadImage(sys.argv[i])
        tempbw = cv.CreateMat(480, 640, cv.CV_8UC1)
        templp = cv.CreateMat(480, 640, cv.CV_32FC1)

        cv.CvtColor(temp, tempbw, cv.CV_BGR2GRAY)
        cv.Laplace(tempbw, templp, 11)

        cv.MatchTemplate(big, templp, res, cv.CV_TM_CCORR_NORMED)
        print sys.argv[i], cv.MinMaxLoc(res)[3]

