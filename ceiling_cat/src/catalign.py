#!/usr/bin/env python

from __future__ import division
import optparse
import re
from pprint import pprint
from math import cos, sin, pi
import cv

def vararg_callback(option, opt_str, value, parser):
    assert value is None
    value = []

    def floatable(str):
        try:
            float(str)
            return True
        except ValueError:
            return False

    for arg in parser.rargs:
        # stop on --foo like options
        if arg[:2] == "--" and len(arg) > 2:
            break
        # stop on -a, but not on -3 or -3.0
        if arg[:1] == "-" and len(arg) > 1 and not floatable(arg):
            break
        value.append(arg)

    del parser.rargs[:len(value)]
    setattr(parser.values, option.dest, (getattr(parser.values, option.dest) or []) + [value])

# stuff a list of tuples (points) into a CvMat
def points2mat(points):
    mat = cv.CreateMat(len(points), len(points[0]), cv.CV_32FC1)
    for i in range(len(points)):
        for j in range(len(points[i])):
            mat[i, j] = points[i][j]
    return mat

def mat2points(mat):
    points = []
    for r in range(mat.rows):
        points.append([])
        for c in range(mat.cols):
            points[r].append(mat[r, c])
        points[r] = tuple(points[r])
    return points

def to_matlab(mat):
    s = '['
    for r in range(mat.rows):
        for c in range(mat.cols):
            s += str(mat[r, c]) + ', '
        s = s[:-2] + '; '
    s = s[:-2] + ' ]'
    return s

def from_matlab(s):
    points = []
    s = s[1:-1]
    for row in s.split(';'):
        r = []
        for val in row.split(','):
            r.append(float(val))
        points.append(r)
    return points2mat(points)

def calculate_dimensions(stuff):
    allcorners = []
    for stufflet in stuff:
        J = stufflet['H']
        for image in stufflet['images']:
            K = image['H']
            H = cv.CreateMat(3, 3, cv.CV_32FC1)
            cv.GEMM(J, K, 1, None, 0, H)

            size = cv.GetSize(image['image'][0])
            corners = [(0, 0), (size[0], 0), (0, size[1]), size]
            corners = points2mat(corners) # to a 4x2 1-channel cvMat
            corners = cv.Reshape(corners, 2) # to a 4x1 2-channel cvMat
            cv.PerspectiveTransform(corners, corners, H)
            corners = cv.Reshape(corners, 1) # to a 4x2 1-channel cvMat
            corners = mat2points(corners) # grr
            allcorners.extend(corners)

    box = cv.BoundingRect(allcorners)
    return box

def glom_images(stuff, box):
    width = box[2] - box[0]
    height = box[3] - box[1]
    A = cv.CreateMat(3, 3, cv.CV_32FC1)
    cv.SetIdentity(A)
    cv.Set2D(A, 0, 2, -box[0])
    cv.Set2D(A, 1, 2, -box[1])
    
    output = cv.CreateMat(height, width, cv.CV_8UC3)
    temp = cv.CreateMat(height, width, cv.CV_8UC3)
    cv.SetZero(output)

    for stufflet in stuff:
        J = stufflet['H']
        for image in stufflet['images']:
            K = image['H']
            H = cv.CreateMat(3, 3, cv.CV_32FC1)
            cv.GEMM(A, J, 1, None, 0, H)
            cv.GEMM(H, K, 1, None, 0, H)

            cv.SetZero(temp)
            cv.WarpPerspective(image['image'][0], temp, H)
            cv.AddWeighted(output, 0.8, temp, 0.2, 0, output)

    return output

def make_stuff(args):
    N = len(args.images)
    stuff = [0]*N
    for i in range(N):
        stuff[i] = {'H': cv.CreateMat(3, 3, cv.CV_32FC1)}
        tx = args.positions[i][0] * args.scale[0]
        ty = args.positions[i][1] * args.scale[1]
        theta = args.positions[i][2] * pi/180 * args.scale[2]
        cv.Set2D(stuff[i]['H'], 0, 0, cos(theta))
        cv.Set2D(stuff[i]['H'], 0, 1, -sin(theta))
        cv.Set2D(stuff[i]['H'], 0, 2, tx)
        cv.Set2D(stuff[i]['H'], 1, 0, sin(theta))
        cv.Set2D(stuff[i]['H'], 1, 1, cos(theta))
        cv.Set2D(stuff[i]['H'], 1, 2, ty)
        cv.Set2D(stuff[i]['H'], 2, 0, 0)
        cv.Set2D(stuff[i]['H'], 2, 1, 0)
        cv.Set2D(stuff[i]['H'], 2, 2, 1)

        n = len(args.images[i])
        stuff[i]['images'] = [0]*n
        for j in range(n):
            stuff[i]['images'][j] = {'H': cv.CreateMat(3, 3, cv.CV_32FC1)}

            filename = args.images[i][j]

            m = re.match(r'.*x([\d\.-]+)y([\d\.-]+)a([\d\.-]+)\..*', filename)
            if not m:
                raise Exception('%s has no position information' % filename)
            try:
                tx = float(m.groups()[0]) * args.scale[0]
                ty = float(m.groups()[1]) * args.scale[1]
                theta = float(m.groups()[2]) * pi/180 * args.scale[2]
            except ValueError:
                raise Exception('%s has invalid position information' % filename)
            cv.Set2D(stuff[i]['images'][j]['H'], 0, 0, cos(theta))
            cv.Set2D(stuff[i]['images'][j]['H'], 0, 1, -sin(theta))
            cv.Set2D(stuff[i]['images'][j]['H'], 0, 2, tx)
            cv.Set2D(stuff[i]['images'][j]['H'], 1, 0, sin(theta))
            cv.Set2D(stuff[i]['images'][j]['H'], 1, 1, cos(theta))
            cv.Set2D(stuff[i]['images'][j]['H'], 1, 2, ty)
            cv.Set2D(stuff[i]['images'][j]['H'], 2, 0, 0)
            cv.Set2D(stuff[i]['images'][j]['H'], 2, 1, 0)
            cv.Set2D(stuff[i]['images'][j]['H'], 2, 2, 1)

            stuff[i]['images'][j]['image'] = (cv.LoadImage(filename), filename)
    
    return stuff

def trackbar(bar, val):
    global stuff, args, output
    if bar[:5] == 'scale':
        c = int(bar[5])

        print val

        a = args.scale
        if c == 0:
            a = (val, a[1], a[2])
        elif c == 1:
            a = (a[0], val, a[2])
        else:
            a = (a[0], a[1], val)
        args.scale = a

    elif bar[:3] == 'pos':
        c = int(bar[3])
        i = int(bar[4:])

        a = args.positions[i]
        if c == 0:
            v = (val-200)/100
            print v
            a = (v, a[1], a[2])
        elif c == 1:
            v = (val-200)/100
            print v
            a = (a[0], v, a[2])
        else:
            v = (val-400)/100
            print v
            a = (a[0], a[1], v)
        args.positions[i] = a

    stuff = make_stuff(args)
    box = calculate_dimensions(stuff)
    print box
    output = glom_images(stuff, box)


if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option('--images', '-i', dest='images', action='callback', callback=vararg_callback)
    parser.add_option('--positions', '-p', dest='positions', action='append', nargs=3, type=float)
    parser.add_option('--scale', '-s', dest='scale', nargs=3, type=int)

    args = parser.parse_args()[0]
    N = len(args.positions)

    stuff = make_stuff(args)
    box = calculate_dimensions(stuff)
    output = glom_images(stuff, box)

    dummy = cv.CreateMat(2, 2, cv.CV_8UC1)

    cv.NamedWindow('Output')
    cv.NamedWindow('Settings')
    cv.CreateTrackbar('X Scale', 'Settings', args.scale[0], 500, lambda x: trackbar('scale0', x))
    cv.CreateTrackbar('Y Scale', 'Settings', args.scale[1], 500, lambda x: trackbar('scale1', x))
    cv.CreateTrackbar('A Scale', 'Settings', args.scale[2], 500, lambda x: trackbar('scale2', x))
    for i in range(N):
        cv.CreateTrackbar('X %d' % i, 'Settings', int(args.positions[i][0]*100)+200, 400, lambda x: trackbar('pos0%d' % i, x))
        cv.CreateTrackbar('Y %d' % i, 'Settings', int(args.positions[i][1]*100)+200, 400, lambda x: trackbar('pos1%d' % i, x))
        cv.CreateTrackbar('A %d' % i, 'Settings', int(args.positions[i][2]*100)+400, 800, lambda x: trackbar('pos2%d' % i, x))
    cv.ShowImage('Settings', dummy)
    cv.ResizeWindow('Settings', 1000, 110 * (N+1))

    while True:
        cv.ShowImage('Output', output)
        if cv.WaitKey(10) == 13:
            for imageset in stuff:
                print '\tH: %s' % to_matlab(imageset['H'])
                for image in imageset['images']:
                    print '\t\tfilename: %s' % image['image'][1]
                    print '\t\tH: %s' % to_matlab(image['H'])

