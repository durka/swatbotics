#include <opencv2/opencv.hpp>
using namespace cv;
using namespace std;

#include "warp.h"

Mat param_mat(float tx, float ty, float theta)
{
    Mat_<float> p(3, 1);

    p(0, 0) = tx;
    p(1, 0) = ty;
    p(2, 0) = theta;

    return p;
}

// we expect p to be a 3-element column vector with tx,ty,theta
Mat warp(const Mat& p, const Size& s) {

  Mat_<float> H(3,3);

  float tx = p.at<float>(0,0);
  float ty = p.at<float>(1,0);
  float theta = p.at<float>(2,0);

  float cx = s.width*0.5;
  float cy = s.height*0.5;

  float a = cos(theta);
  float b = sin(theta);
  
  H(0,0) = a;   H(0,1) = -b;  H(0,2) = cx + tx - cx*a + cy*b;
  H(1,0) = b;   H(1,1) = a;   H(1,2) = cy + ty - cy*a - cx*b;
  H(2,0) = 0;   H(2,1) = 0;   H(2,2) = 1;

/*[ cos(theta1), -sin(theta1), cx + tx1 - cx*cos(theta1) + cy*sin(theta1)]
[ sin(theta1),  cos(theta1), cy + ty1 - cy*cos(theta1) - cx*sin(theta1)]
[           0,            0,                                          1]*/
  
  return H;
}

Transform2D warp(Transform2D p, const Size& s)
{
    float cx = s.width*0.5;
    float cy = s.height*0.5;

    return Transform2D(1, cx, cy, 0) * p * Transform2D(1, -cx, -cy, 0);
}

void jacobian(const Size& s, Mat* jx, Mat* jy) {
  
  // dx/dtx and dy/dty
  jx[0] = jy[1] = Mat::ones(s, CV_32F);

  // dx/dty and dy/dtx
  jx[1] = jy[0] = Mat::zeros(s, CV_32F);

  jx[2] = Mat(s, CV_32F);
  jy[2] = Mat(s, CV_32F);

  float cx = s.width*0.5;
  float cy = s.height*0.5;

  for (int y=0; y<s.height; ++y) {
    for (int x=0; x<s.width; ++x) {
      jx[2].at<float>(y,x) = (cy-y);
      jy[2].at<float>(y,x) = (x-cx);
    }
  }

}

Mat invcomp(const Mat& p1, const Mat& p2) {

  float tx1    = p1.at<float>(0,0);
  float ty1    = p1.at<float>(1,0);
  float theta1 = p1.at<float>(2,0);

  float tx2    = p2.at<float>(0,0);
  float ty2    = p2.at<float>(1,0);
  float theta2 = p2.at<float>(2,0);

  Mat_<float> rval(3, 1);

  rval(0, 0) = tx1 - tx2*cos(theta1 - theta2) + ty2*sin(theta1 - theta2);
  rval(1, 0) = ty1 - ty2*cos(theta1 - theta2) - tx2*sin(theta1 - theta2);
  rval(2, 0) = theta1 - theta2;

  return rval;

}

Mat comp(const Mat& p1, const Mat& p2, const Size& s)
{
  float tx1    = p1.at<float>(0,0);
  float ty1    = p1.at<float>(1,0);
  float theta1 = p1.at<float>(2,0);

  float tx2    = p2.at<float>(0,0);
  float ty2    = p2.at<float>(1,0);
  float theta2 = p2.at<float>(2,0);

  float cx = s.width*0.5;
  float cy = s.height*0.5;

  Mat_<float> rval(3, 1);

  rval(0, 0) = cx + tx1 - cx*cos(theta1 + theta2) + cy*sin(theta1 + theta2) + tx2*cos(theta1) - ty2*sin(theta1);
  rval(1, 0) = cy + ty1 - cy*cos(theta1 + theta2) - cx*sin(theta1 + theta2) + ty2*cos(theta1) + tx2*sin(theta1);
  rval(2, 0) = theta1 + theta2;

  return rval;
}

