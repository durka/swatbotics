#include <ros/ros.h>
#include <opencv2/opencv.hpp>
#include <vector>
#include "lk.h"
#include "parse.h"
#include "transform2d.h"
using namespace std;
using namespace cv;
using namespace ros;

#define TIME(x) clock_t start__ = clock(); x; printf("%g s\n", (float)(clock() - start__)/CLOCKS_PER_SEC)
#define PI 3.14159265358979

// ./catographer w h scale inputfile outputfile
// ex: ./catographer 1101 735 ceiling.byhand.map ceiling.byhand.png
// w=1101 h=735 xs=288 ys=62
int main(int argc, char *argv[])
{
    init(argc, argv, "catographer");

    int w = atoi(argv[1]), h = atoi(argv[2]);

    
    float xcam, ycam, scale;
    if (param::has("/turtlebot_node/scale"))
    {
        double s, ox, oy;
        param::get("/turtlebot_node/scale", s);
        param::get("/turtlebot_node/cam_off_x", ox);
        param::get("/turtlebot_node/cam_off_y", oy);
        scale = s;
        xcam = ox;
        ycam = oy;
        cout << scale << " " << xcam << " " << ycam << endl;
    }
    
    vector<string> filenames;
    vector<Mat_<float> > params;
    int N = parse_map(argv[3], scale, xcam, ycam, Size(w, h), filenames, params);
    cout << scale << " " << xcam << " " << ycam << endl;

    Mat_<float> H(3, 3);
    H(0,0) = 1.083892276196663;     H(0,1) = -0.008710256297209434;     H(0,2) = -12.44756190521801;
    H(1,0) = 0.02922322008634242;   H(1,1) = 1.02708737551197;          H(1,2) = -10.39614197788496;
    H(2,0) = 0.0001181193622689317; H(2,1) = -3.994049577493519e-05;    H(2,2) = 1;
    //H = H.inv();

    //H = Mat_<float>::eye(3,3);

    namedWindow("derp");
    namedWindow("herp");

    Mat big = Mat::zeros(h, w, CV_32F);
    Mat mask = Mat::zeros(big.size(), CV_8U);

    for (int i = 0; i < N; ++i)
    {
        Mat templ2 = laplace_read(("data/" + filenames[i]).c_str()), ones2 = Mat::ones(templ2.size(), CV_8U);

        printf("processing %s... ", filenames[i].c_str());

        // run registration algorithm
        Transform2D p(1, params[i](0), params[i](1), params[i](2));
        cout << params[i](0)/scale << " " << params[i](1)/scale << " " << params[i](2)*180/M_PI << endl;

        if (i) {
          TIME(pyramidLK(big, templ2, mask, H, p, 4));
        }

        feather_place(big, mask, templ2, warp(p, templ2.size())*H, 20);
        
        imshow("derp", big); 
        if (i) {
          waitKey(1);
        } else {
          waitKey();
        }

    }

    waitKey();

    Mat derp(big.size(), CV_8UC1);
    big.convertTo(derp, CV_8UC1, 255.0, 0);
    imwrite(argv[4], derp);

}

