#include <opencv2/opencv.hpp>
#include <cstdio>
#include <ctime>
#include <cstdlib>
#include <iostream>

#include "lk.h"

using namespace cv;
using namespace std;

#define TIME(x) clock_t start__ = clock(); x; printf("%g s\n", (float)(clock() - start__)/CLOCKS_PER_SEC)
#define PI 3.14159265358979


//////////////////////////////////////////////////////////////////////

double frand(double top)
{
    return top * (rand() % 1001) / 1000.0;
}
double symrand(double top)
{
    return frand(top*2) - top;
}


// ./invcomp big.png N
int main(int argc, char *argv[])
{
    namedWindow("derp");
    namedWindow("herp");

    const int offset = 100;
    const float dxy = 10, dtheta = 5*PI/180;

    Mat derp = imread(argv[1], 0);
    Mat big(derp.size(), CV_32FC1);
    derp.convertTo(big, CV_32FC1, 1.0/255.0, 0);
    //Laplacian(derp, big, CV_32FC1);
    //GaussianBlur(big, big, Size(17, 17), 0);
    imshow("derp", big);
    //system("say press space to start");
    waitKey();

    float sse = 0;

    int n = atoi(argv[2]);
    srand(time(NULL));
    /*float tx = big.cols/2 - 320;
    float ty = big.rows/2 - 240;
    float theta = PI/3;*/
    float tx = frand(big.cols - 640);
    float ty = frand(big.rows - 480);
    float theta = symrand(PI/4);
    for (int i = 0; i < n; ++i)
    {
        tx += symrand(dxy);
        ty += symrand(dxy);
        theta += symrand(dtheta);
        printf("%g %g %g\n", tx, ty, theta*180/PI);
        Mat pbefore = param_mat(tx, ty, theta);

        Mat herp(480, 640, CV_32FC1);
        warpPerspective(big, herp, warp(param_mat(tx, ty, theta), herp.size()), herp.size(), CV_WARP_INVERSE_MAP);
        Mat templ = herp(Range(offset, herp.size().height - offset), Range(offset, herp.size().width - offset));

        Mat_<float> params(3, 1);
        Mat mask = Mat::ones(big.size(), CV_8UC1);
        params(0, 0) = tx+offset; params(1, 0) = ty+offset; params(2, 0) = theta;
        TIME(pyramidLK(big, templ, mask, Mat::eye(3,3,CV_32FC1), params, 3));
        tx = params(0, 0)-offset; ty = params(1, 0)-offset; theta = params(2, 0);
        
        printf("%g %g %g\n", tx, ty, theta*180/PI);
        Mat pafter = param_mat(tx, ty, theta);
        sse += (pbefore - pafter).dot(pbefore - pafter);

        Mat output(big.size().height, big.size().width, CV_32FC1);
        warpPerspective(templ, output, warp(params, templ.size()), output.size());
        addWeighted(big, 0.5, output, 0.5, 0, output);

        imshow("derp", output);
        waitKey(50);
    }

    printf("\n%g\n", sse);

    return 0;
}


