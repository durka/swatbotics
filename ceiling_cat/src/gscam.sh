pushd .
roscd gscam
cd bin
uvcdynctrl -d /dev/video0 -s "White Balance Temperature, Auto" 0 # no auto white balance
uvcdynctrl -d /dev/video0 -s "Exposure, Auto Priority" 0 # no auto exposure
uvcdynctrl -d /dev/video0 -s "Exposure, Auto" 1 # really, manual mode for exposure
uvcdynctrl -d /dev/video0 -s "Exposure (Absolute)" 200
uvcdynctrl -d /dev/video0 -s "White Balance Temperature" 6000
GSCAM_CONFIG="v4l2src device=/dev/video0 ! video/x-raw-rgb ! ffmpegcolorspace" rosrun gscam gscam
popd

