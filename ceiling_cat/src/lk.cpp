#include <opencv2/opencv.hpp>
#include <cstdarg>
#include <cassert>
#include <iomanip>
using namespace cv;
using namespace std;
#include "lk.h"

#include "profile.h"

void displayScaledHappy(const char* name, const Mat& image) {

  Mat tmp(image.size(), CV_8UC1);
  
  double minVal, maxVal;

  minMaxLoc(image, &minVal, &maxVal);

  maxVal = max(fabs(maxVal), fabs(minVal));

  image.convertTo(tmp, CV_8UC1, 127/maxVal, 127);

  imshow(name, tmp);

}


Mat laplace_read(const char *filename)
{
    Mat raw = imread(filename, 0), processed(raw.size(), CV_32FC1);

    Laplacian(raw, processed, CV_32FC1);
    GaussianBlur(processed, processed, Size(17, 17), 0);

    return processed;
}

void laplace(const Mat& src, Mat& dst, 
	     int blur_size,
	     const Size& dst_size)
{

  Mat tmp;
  Laplacian(src, tmp, CV_32FC1);
  
  int w = std::min(dst_size.width, src.cols);
  int h = std::min(dst_size.height, src.rows);
  int x0 = (src.cols - w) / 2;
  int y0 = (src.rows - h) / 2;

  Mat subrect(tmp, cv::Rect(x0, y0, w, h));

  GaussianBlur(subrect, dst, Size(blur_size, blur_size), 0);

}

void LucasKanade(const Mat& big, const Mat& templ, const Mat& mask, const Mat& camera, Transform2D& p, const double epsilon)
{
    int w = templ.size().width, h = templ.size().height;

    // precompute 3: gradient
    Mat gradT[2] = {Mat(h, w, CV_32FC1), Mat(h, w, CV_32FC1)};
    TIME("gradient",
        Sobel(templ, gradT[0], CV_32FC1, 1, 0);
        Sobel(templ, gradT[1], CV_32FC1, 0, 1));

    // precompute 4: jacobian
    Mat jx[3], jy[3];
    TIME("jacobian",
        jacobian(Size(w, h), jx, jy));

    // precompute 5: steepest descent images, and 6: hessian
    Mat stpdst[3] = {Mat::zeros(h, w, CV_32FC1), Mat::zeros(h, w, CV_32FC1), Mat::zeros(h, w, CV_32FC1)};
    Mat hessian = Mat::zeros(3, 3, CV_32FC1), hessinv(3, 3, CV_32FC1);

    /*
    if (i_ == 0) {
      displayScaledHappy("herp", gradT[0]);
      waitKey();
      
      displayScaledHappy("herp", gradT[1]);
      waitKey();
    }
    */

    TIME("steepest descent",
        for (int i = 0; i < 3; ++i)
        {
            stpdst[i] += jx[i].mul(gradT[0]) + jy[i].mul(gradT[1]);


            /*
            if (i_ == 0) {

              displayScaledHappy("herp", jx[i]);
              waitKey();
              
              displayScaledHappy("herp", jy[i]);
              waitKey();
              
              displayScaledHappy("herp", stpdst[i]);
              waitKey();

            }
            */

        });

    TIME("hessian",
        for (int i = 0; i < 3; ++i)
        {
            for (int j = 0; j <= i; ++j)
            {
                hessian.at<float>(i, j) = hessian.at<float>(j, i) = sum(stpdst[i].mul(stpdst[j]))[0];
            }
        }
        hessinv = hessian.inv());

    // redistributing parameters
    // allocations for loop
    Mat Iwarp(h, w, CV_32FC1), IwarpH(h, w, CV_32FC1), maskwarp(h, w, CV_8UC1);
    Mat error(h, w, CV_32FC1);
    Mat updmat(3, 1, CV_32FC1);
    Mat_<float> delP(3, 1);
    Mat derp, H, H2, M;
#   ifdef SANITY
        Mat_<float> params(3, 1); params(0) = p.tx; params(1) = p.ty; params(2) = p.theta;
#   endif
    int j = 0;
    TIMEl_init(7);
    //TIME("loop",
        while (true)
        {
            TIMEl_loop();

            // loop 1: warp I
            TIMEl(
                H = warp(p, Size(w, h)));
#               ifdef SANITY
                    H2 = warp(params, Size(w, h));
                    cout << "warps: " << endl << H << endl << H2 << endl << endl;
#               endif
                M = camera.inv()*H.inv();
            TIMEl(
                warpPerspective(big, Iwarp, M, Iwarp.size()));
            TIMEl(
                warpPerspective(mask, maskwarp, M, maskwarp.size()));

            // loop 2: error image
            TIMEl(
                error = 0.0;
                subtract(Iwarp, templ, error, maskwarp));
            
            /*// display stuff
            imshow("herp", Iwarp);
            waitKey();
            displayScaledHappy("herp", maskwarp);
            waitKey();
            imshow("herp", error);
            waitKey();*/

            // loop 7: steepest descent parameter updates
            TIMEl(
                for (int i = 0; i < 3; ++i)
                {
                    derp = stpdst[i].mul(error);
                    updmat.at<float>(i, 0) = sum(derp)[0];
                });

            // loop 8: delta P
            TIMEl(
                delP = hessinv*updmat);

            // loop 9: warp update
            TIMEl(
                p = p * Transform2D(1, delP(0), delP(1), delP(2)).inv();
#               ifdef SANITY
                    params = invcomp(params, delP);
                    cout << templ.size().width << ", " << templ.size().height << endl;
                    cout << setw(5) << "params: " << endl << p.tx << ", " << p.ty << ", " << p.theta << endl << params << endl << endl;
#               endif
                 )
            //printf("%g %g %g\t\t\t%g\n", p(0,0), p(1,0), p(2,0)*180/3.14, abs(delP.dot(delP)));
            
            if (j++ > 50 || abs(delP.dot(delP)) < epsilon)
            {
#               ifdef PROFILE
                    printf("%d iters\n", j);
#               endif
                break;
            }
        }//);
    TIMEl_end("warp", "warp I", "warp mask", "error image", "steepest descent parameter updates", "calculate delP", "compose warps");
}

void pyramidLK(const std::vector<Mat>& bigs, const Mat& templ, 
	       const std::vector<Mat>& masks, const Mat& camera, 
	       Transform2D& params, 
	       int levels, int minlevels, 
	       const double epsilon)
{
    assert(params.scale == 1);

    //vector<Mat> templates(levels), bigs(levels), masks(levels);
    vector<Mat> templates(levels);
    buildPyramid(templ, templates, levels);
    Mat_<float> scale = Mat::eye(3,3,CV_32FC1);
    
    scale(0,0) = scale(1,1) = pow(2.0f, (float)levels+1);
    params.tx /= pow(2.0f, (float)levels+1);
    params.ty /= pow(2.0f, (float)levels+1);

    for (; levels >= minlevels; --levels)
    {
        scale(0,0) /= 2.0;
        scale(1,1) /= 2.0;
        params.tx *= 2.0;
        params.ty *= 2.0;
        
        //printf("level %d, scaling factor %g\n", levels, scale(0,0));

        LucasKanade(bigs[levels], templates[levels], masks[levels], scale.inv() * camera * scale, params, epsilon);
    }
    params.tx *= pow(2.0f, (float)levels+1);
    params.ty *= pow(2.0f, (float)levels+1);
}

void feather_place(Mat& big, Mat& mask, const Mat& templ2, const Mat& WH, int fsize)
{

  // mask2 is a white rectangle surrounded by black
  Mat wr = 255*Mat::ones(templ2.size(), CV_8U);
  wr.rowRange(0, fsize) = 0.0;
  wr.rowRange(wr.rows-fsize, wr.rows) = 0.0;
  wr.colRange(0, fsize) = 0.0;
  wr.colRange(wr.cols-fsize, wr.cols) = 0.0;
  
  Mat temp_mask;
  warpPerspective(wr, temp_mask, WH, mask.size());
  mask = mask | temp_mask;

  int r = fsize;
  if (r % 2 == 0) { --r; }
  GaussianBlur(temp_mask, temp_mask, Size(r, r), 0, 0, BORDER_REPLICATE);

  Mat temp_big;
  warpPerspective(templ2, temp_big, WH, big.size());


  for (int y=0; y<big.rows; ++y) {
    for (int x=0; x<big.cols; ++x) {
      float a = temp_mask.at<unsigned char>(y,x) / 255.0;
      unsigned char src = temp_big.at<unsigned char>(y,x);
      unsigned char& dst = big.at<unsigned char>(y,x);
      dst = dst*(1-a) + src*a;
    }
  }
  

  /*
  Mat feather(templ2.size(), CV_32F);
  mask2.convertTo(feather, CV_32F);
  int r = fsize;
  if (r % 2 == 0) { --r; }
  GaussianBlur(feather, feather, Size(r, r), 0, 0, BORDER_REPLICATE);


    //displayScaledHappy("derp", feather);
    //waitKey();

    temp = Mat::zeros(big.size(), CV_32F);
    Mat warp2 = Mat::zeros(big.size(), CV_32F);
    warpPerspective(feather, temp, WH, temp.size());
    warpPerspective(templ2, warp2, WH, warp2.size());
    big = (1.0 - temp).mul(big) + temp.mul(warp2);
  */
}

