clear
reset(symengine)


R = @(theta) [ cos(theta) -sin(theta) 0 ; sin(theta) cos(theta) 0 ; 0 0 1 ]
T = @(tx,ty) [ 1 0 tx ; 0 1 ty ; 0 0 1 ]
W = @(tx,ty,theta,cx,cy) T(tx,ty)*T(cx,cy)*R(theta)*T(-cx,-cy)

syms tx1 ty1 theta1 tx2 ty2 theta2 cx cy real

W1 = W(tx1,ty1,theta1, cx, cy)
W2 = W(tx2, ty2, theta2, cx, cy)

W3 = W1*inv(W2)

W3_alt = W(0, 0, theta1-theta2, cx, cy)

diff = W3 - W3_alt

simplify(diff)


