#!/usr/bin/env python

from __future__ import division
import sys
import cv
from math import cos, sin
from time import time

def from_mat(mat):
    v = [0]*mat.height
    for y in range(mat.height):
        v[y] = [0]*mat.width
        for x in range(mat.width):
            v[y][x] = cv.Get2D(mat, y, x)[0]
    return v

def rescale(mat):
    r = cv.CreateMat(mat.height, mat.width, cv.CV_8UC1)
    cv.ConvertScale(mat, r, 127, 127)
    return r

if __name__ == '__main__':
    temp = cv.LoadImage(sys.argv[1], cv.CV_LOAD_IMAGE_GRAYSCALE)
    big = cv.CreateMat(temp.height, temp.width, cv.CV_32FC1)
    cv.ConvertScale(temp, big, 1/255, 0)
    
    gradT = [cv.CreateMat(480, 640, cv.CV_32FC1), cv.CreateMat(480, 640, cv.CV_32FC1)]

    for i in range(2, len(sys.argv)):
        start = time()
        print 'precomputing %s... ' % sys.argv[i],

        # load template image from file
        temp = cv.LoadImage(sys.argv[i], cv.CV_LOAD_IMAGE_GRAYSCALE)
        template = cv.CreateImage((640, 480), cv.IPL_DEPTH_32F, 1)
        cv.ConvertScale(temp, template, 1/255, 0)

        cv.Smooth(template, template, cv.CV_GAUSSIAN, 11)
        cv.Smooth(big, big, cv.CV_GAUSSIAN, 11)

        # precompute 3: gradient
        cv.Sobel(template, gradT[0], 1, 0)
        cv.Sobel(template, gradT[1], 0, 1)

        # precompute 4: jacobian
        # W(tx, ty, theta) = [cos(theta) -sin(theta) tx; sin(theta) cos(theta) ty]
        # dW/dp = [1 0 -y/k; 0 1 x/k]
        k = 1

        # precompute 5: steepest descent images, and 6: hessian
        stpdst = [0, 0, 0]
        hessian = cv.CreateMat(3, 3, cv.CV_32FC1)
        hinv = cv.CreateMat(3, 3, cv.CV_32FC1)
        for i in range(3):
            stpdst[i] = cv.CreateMat(480, 640, cv.CV_32FC1)
        cv.SetZero(hessian)
        cv.Copy(gradT[0], stpdst[0])
        cv.Copy(gradT[1], stpdst[1])
        for x in range(640):
            for y in range(480):
                cv.Set2D(stpdst[2], y, x, x/k*cv.Get2D(gradT[1], y, x)[0] - y/k*cv.Get2D(gradT[0], y, x)[0])

                Tx = cv.Get2D(gradT[0], y, x)[0]
                Ty = cv.Get2D(gradT[1], y, x)[0]
                q = x*Ty - y*Tx
                cv.Set2D(hessian, 0, 0, cv.Get2D(hessian, 0, 0)[0] + Tx**2)
                cv.Set2D(hessian, 0, 1, cv.Get2D(hessian, 0, 1)[0] + Tx*Ty)
                cv.Set2D(hessian, 0, 2, cv.Get2D(hessian, 0, 2)[0] + Tx*q)
                cv.Set2D(hessian, 1, 0, cv.Get2D(hessian, 1, 0)[0] + Tx*Ty)
                cv.Set2D(hessian, 1, 1, cv.Get2D(hessian, 1, 1)[0] + Ty**2)
                cv.Set2D(hessian, 1, 2, cv.Get2D(hessian, 1, 2)[0] + Ty*q)
                cv.Set2D(hessian, 2, 0, cv.Get2D(hessian, 2, 0)[0] + Tx*q)
                cv.Set2D(hessian, 2, 1, cv.Get2D(hessian, 2, 1)[0] + Ty*q)
                cv.Set2D(hessian, 2, 2, cv.Get2D(hessian, 2, 2)[0] + q**2)
        cv.Invert(hessian, hinv, cv.CV_SVD) # SVD method doesn't produce random numbers from singular matrices
        #print from_mat(hessian)
        #print from_mat(hinv)
        #print

        Iwarp = cv.CreateImage((640, 480), cv.IPL_DEPTH_32F, 1)
        error = cv.CreateMat(480, 640, cv.CV_32FC1)
        temp = cv.CreateMat(480, 640, cv.CV_32FC1)
        updmat = cv.CreateMat(3, 1, cv.CV_32FC1)
        delP = cv.CreateMat(3, 1, cv.CV_32FC1)
        H = cv.CreateMat(3, 3, cv.CV_32FC1)
        tx = 0
        ty = 0
        theta = 0

        print '%.3f s' % (time() - start)
        start = time()
        print 'matching %s...' % sys.argv[i],
        while True:
            # loop 1: warp I
            cv.Set2D(H, 0, 0, cos(theta))
            cv.Set2D(H, 0, 1, -sin(theta))
            cv.Set2D(H, 0, 2, tx)
            cv.Set2D(H, 1, 0, sin(theta))
            cv.Set2D(H, 1, 1, cos(theta))
            cv.Set2D(H, 1, 2, ty)
            cv.Set2D(H, 2, 0, 0)
            cv.Set2D(H, 2, 1, 0)
            cv.Set2D(H, 2, 2, 1)
            cv.WarpPerspective(big, Iwarp, H, cv.CV_WARP_INVERSE_MAP)
            
            # loop 2: error image
            cv.Sub(Iwarp, template, error)
            
            # loop 7: steepest descent parameter updates
            updates = [0, 0, 0]
            for i in range(3):
                cv.Mul(stpdst[i], error, temp)
                updates[i] = cv.Sum(temp)
            #print updates
            
            # loop 8: delta P
            for i in range(3):
                cv.Set2D(updmat, i, 0, updates[i])
            cv.GEMM(hinv, updmat, 1, None, 0, delP)
            #print from_mat(delP)
            
            # loop 9: warp update
            dx = cv.Get2D(delP, 0, 0)[0]
            dy = cv.Get2D(delP, 1, 0)[0]
            dtheta = cv.Get2D(delP, 2, 0)[0]
            tx += -dx*cos(dtheta) - dy*sin(dtheta)
            ty += dx*sin(dtheta) - dy*cos(dtheta)
            theta -= dtheta

            if abs(cv.DotProduct(delP, delP)) < 1e-6:
                break
            print tx, ty, theta
        print '%.3f s' % (time() - start)

        tx += 0.5
        ty += 0.5
        print tx, ty, theta*180/3.14
        cv.Set2D(H, 0, 0, cos(theta))
        cv.Set2D(H, 0, 1, -sin(theta))
        cv.Set2D(H, 0, 2, tx)
        cv.Set2D(H, 1, 0, sin(theta))
        cv.Set2D(H, 1, 1, cos(theta))
        cv.Set2D(H, 1, 2, ty)
        cv.Set2D(H, 2, 0, 0)
        cv.Set2D(H, 2, 1, 0)
        cv.Set2D(H, 2, 2, 1)
        output = cv.CreateMat(big.height, big.width, cv.CV_32FC1)
        cv.WarpPerspective(template, output, H)
        cv.AddWeighted(big, 0.5, output, 0.5, 0, output)
        cv.NamedWindow('t')
        cv.ShowImage('t', output)
        cv.WaitKey()
