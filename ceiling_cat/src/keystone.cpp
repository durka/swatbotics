#include <opencv2/opencv.hpp>
#include <vector>
#include "lk.h"
#include "warp.h"
using namespace std;
using namespace cv;

#define TIME(x) clock_t start__ = clock(); x; printf("%g s\n", (float)(clock() - start__)/CLOCKS_PER_SEC)
#define PI 3.14159265358979

void displayScaledHappy(const char* name, const Mat& image) {

  Mat tmp(image.size(), CV_8UC1);
  
  double minVal, maxVal;

  minMaxLoc(image, &minVal, &maxVal);

  maxVal = max(fabs(maxVal), fabs(minVal));

  image.convertTo(tmp, CV_8UC1, 127/maxVal, 127);

  imshow(name, tmp);

}

// ./catographer w h xs ys inputfile outputfile
// ex: ./catographer 1101 735 288 62 ceiling.byhand.map ceiling.byhand.png
// w=1101 h=735 xs=288 ys=62
int main(int argc, char *argv[])
{
    namedWindow("keystone");
    namedWindow("keystone2");

    Mat image = Mat::zeros(800, 800, CV_8UC1);
    Mat image_smaller = Mat::zeros(400, 400, CV_8UC1);
    Mat_<float> H(3, 3);
    H(0,0) = 1.083892276196663;     H(0,1) = -0.008710256297209434;     H(0,2) = -12.44756190521801;
    H(1,0) = 0.02922322008634242;   H(1,1) = 1.02708737551197;          H(1,2) = -10.39614197788496;
    H(2,0) = 0.001181193622689317; H(2,1) = -3.994049577493519e-05;    H(2,2) = 1;

    // make checkerboard
    bool white = true;
    for (int x = 0; x < 800; x += 80)
    {
        for (int y = 0; y < 800; y += 80)
        {
            rectangle(image, Point(x, y), Point(x+160, y+160), white ? CV_RGB(255, 255, 255) : CV_RGB(0, 0, 0), CV_FILLED);
            white = !white;
        }
        white = !white;
    }
    resize(image, image_smaller, image_smaller.size());

    Mat_<float> p(3,1);
    p(0,0) = 180; p(1,0) = 180; p(2,0) = 10*PI/180;

    Mat small(240, 320, CV_8UC1);
    Mat tiny(120, 160, CV_8UC1);
    Mat W = warp(p, small.size());

    Mat M;

    Mat_<float> S = Mat_<float>::eye(3,3);
    S(0,0) = S(1,1) = 2;

    int what = 0;

    while (1) {

        switch (what) {
            case 0:
                M = W.inv();
                break;
            case 1:
                M = H.inv()*W.inv();
                break;
            case 2:
                M = W.inv()*H.inv();
                break;
            case 3:
            default:
                M = H.inv();
        }



        warpPerspective(image, small, M, small.size());
        warpPerspective(image_smaller, tiny, S.inv()*M*S, tiny.size());

        imshow("keystone", small);
        imshow("keystone2", tiny);
        int k = waitKey() & 0xff;

        if (k == 27) {
            break;
        } else if (k == 'a') {
            what = (what + 1) % 4;
        } else if (k == 'z') {
            what = (what + 3) % 4;
        } else if (k >= '1' && k <= '4') {
            what = k - '1';
        }

        printf("%d\n", what);

    }
    return 0;
}

