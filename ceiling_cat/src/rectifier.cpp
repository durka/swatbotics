#include <opencv2/opencv.hpp>
#include <fstream>
#include <sstream>

#define RESIZE 1
const int shift = 4;
const int scl = (1 << shift);

void drawMarker(cv::Mat& mat, const cv::Point2f& pf, const cv::Scalar& color) {

  const int ax = scl*4;

  cv::Point p = scl*pf;
  cv::Point a1(ax,ax);
  cv::Point a2(ax,-ax);

  cv::line(mat, p+a1, p-a1, color, 1, CV_AA, shift);
  cv::line(mat, p+a2, p-a2, color, 1, CV_AA, shift);

}

void drawLine(cv::Mat& mat, 
              const cv::Point2f& pf1, 
              const cv::Point2f& pf2, 
              const cv::Scalar& color) {

  cv::Point2f diff = pf2-pf1;

  cv::line(mat, scl*pf1, scl*pf2, color, 1, CV_AA, shift);

}

void mapPoints(cv::Point2f points[4],
               const cv::Point2f& size,
               cv::Point2f npts[4]) {
  
  /*
  cv::Mat_<float> data(4, 2, &points[0].x);

  cv::PCA pca(data, cv::Mat(), CV_PCA_DATA_AS_ROW, 2);

  std::cout << "mean = " << pca.mean << "\n";
  std::cout << "axis 0 = " << pca.eigenvectors.row(0) << "\n";
  std::cout << "axis 1 = " << pca.eigenvectors.row(1) << "\n";
  */


  cv::Point2f mean(0,0);
  for (int i=0; i<4; ++i) {
    mean += points[i];
  }
  mean = mean * 0.25;

  cv::Point2f h0 = points[1] - points[0];
  cv::Point2f h1 = points[2] - points[3];

  cv::Point2f v0 = points[2] - points[1];
  cv::Point2f v1 = points[3] - points[0];


  cv::Point2f h = 0.5*(h0 + h1);

  cv::Point2f v = 0.5*(v0 + v1);

  float vn = sqrt(v.dot(v));
  float hn = sqrt(h.dot(h));

  float va = atan2(v.y, v.x);
  float ha = atan2(h.y, h.x);

  v = v * (1/vn);
  h = h * (1/hn);

  float sx = hn / size.x;
  float sy = vn / size.y;
  float scl = sqrt(sx*sy);

  while (va - ha > M_PI) { ha += 2*M_PI; }
  while (va - ha < -M_PI) { ha -= 2*M_PI; }

  float ma = 0.5*(va+ha);
  
  va = ma + M_PI/4;
  ha = ma - M_PI/4;

  cv::Point2f nh = scl*size.x*cv::Point2f(cos(ha), sin(ha));
  cv::Point2f nv = scl*size.y*cv::Point2f(-sin(ha), cos(ha));

  npts[0] = mean - 0.5*nh - 0.5*nv;
  npts[1] = mean + 0.5*nh - 0.5*nv;
  npts[2] = mean + 0.5*nh + 0.5*nv;
  npts[3] = mean - 0.5*nh + 0.5*nv;
  

}

cv::Mat findHomography(cv::Point2f src[4], cv::Point2f dst[4]) {
  cv::Mat smat(4,1,CV_32FC2,&src[0].x);
  cv::Mat dmat(4,1,CV_32FC2,&dst[0].x);
  return findHomography(smat, dmat);
}

void fatBits(const cv::Mat& src, cv::Mat& dst) {

  assert(src.type() == CV_8UC3);
  assert(dst.type() == CV_8UC3);

  std::vector<cv::Mat> tmps;
  cv::Mat tmp;

  cv::split(src, tmps);

  for (int i=0; i<3; ++i) {
    cv::equalizeHist(tmps[i], tmps[i]);
  }

  cv::merge(tmps, tmp);

  int scl = dst.cols / src.cols;
  assert(dst.cols == src.cols * scl);
  assert(dst.rows == src.rows * scl);
  
  for (int y=0; y<dst.cols; ++y) {
    for (int x=0; x<dst.cols; ++x) {
      int sy = y/scl;
      int sx = x/scl;
      dst.at<cv::Vec3b>(y,x) = tmp.at<cv::Vec3b>(sy,sx);
    }
  }
  
}

int main(int argc, char** argv) {

  cv::namedWindow("window");
  cvMoveWindow("window", 0, 0);

  bool warp = false;
  bool line = true;
  bool coarse = true;
  bool done = false;

  std::ifstream istr("data/rect.txt");
  std::string filename;

  istr >> filename;

  cv::Mat image = cv::imread(filename);

  cv::Point2f points[4];

  for (int i=0; i<4; ++i) {
    istr >> points[i].x >> points[i].y;
  }

  cv::Point2f size;
  istr >> size.x >> size.y;

  int selected = 0;
  while (!done) {


    //////////////////////////////////////////////////

    cv::Point2f npts[4];

    mapPoints(points, size, npts);

    cv::Mat_<float> h = findHomography(points, npts);
    cout << "keystone homography: " << fixed << endl
         << "[ [ " << h(0,0) << ", " << h(0,1) << ", " << h(0,2) << " ]," << endl
         << "  [ " << h(1,0) << ", " << h(1,1) << ", " << h(1,2) << " ]," << endl
         << "  [ " << h(2,0) << ", " << h(2,1) << ", " << h(2,2) << " ] ]" << endl;

    cout << "scale (px/m): " << sqrt((points[0]-points[1]).dot((points[0]-points[1])))/size.y/2 << endl;

    cv::Mat big;

    if (warp) {
      cv::Mat tmp;
      cv::warpPerspective(image, tmp, h, image.size());
      cv::resize(tmp, big, cv::Size(0,0), RESIZE, RESIZE, cv::INTER_NEAREST);
    } else {
      cv::resize(image, big, cv::Size(0,0), RESIZE, RESIZE, cv::INTER_NEAREST);
    }
    

    for (int i=0; i<4; ++i) {
      const cv::Point2f* pts = warp ? npts : points;
      int ii = (i + 1) % 4;
      cv::Point2f l0 = pts[i];
      cv::Point2f l1 = pts[ii];
      cv::Point2f delta = l1 - l0;
      l0 = l0 - 10*delta;
      l1 = l1 + 10*delta;
      if (line) {
	cv::line(big, 4*RESIZE*l0, 4*RESIZE*l1, CV_RGB(0,255,255), 1, CV_AA, 2);
      }
    }

    // draw inset
    int sz = 25;
    int scl = 6;
    cv::Point psel = points[selected];
    cv::Point p0 = psel - cv::Point(sz, sz);

    p0.x = std::max(0, std::min(p0.x, image.cols-2*sz-1));
    p0.y = std::max(0, std::min(p0.y, image.rows-2*sz-1));

    cv::Point2f p1 = points[selected]-cv::Point2f(p0.x, p0.y);
    cv::Point2f p2 = p1 + cv::Point2f(1,1);

    cv::Mat inset(image, cv::Rect(p0.x, p0.y, 2*sz+1, 2*sz+1));
    cv::Mat dst(big, cv::Rect(0, 0, scl*(2*sz+1), scl*(2*sz+1)));
    fatBits(inset, dst);
    
    std::cerr << "inset.size = "<< inset.rows << "\n";
    std::cerr << "dst.size = "<< dst.rows << "\n";
    
    cv::rectangle(dst, scl*p1, scl*p2, CV_RGB(0,0,255), 1, CV_AA, 0);

    cv::imshow("window", big);
    std::cout << "point " << selected << " selected" << endl;
    for (int i=0; i<4; ++i) {
      std::cout << "  points[" << i << "] = " << points[i] << endl;
    }

    int k = cv::waitKey();

    switch (k & 0xff) {
    case 'h':
      cout << "rectifier key commands\n"
              "\tspace : toggle warped/unwarped view\n"
              "\t0-3   : select points\n"
              "\twasd  : move selected point\n"
              "\tc     : toggle moving by 10 px or 1 px\n"
              "\tl     : toggle blue lines\n"
              "\tEsc   : exit and save\n"
              "\tC-c   : exit without saving"
              << endl;
    case 27: //Esc
      {
          ofstream out("data/rect.txt");
          out << filename << endl;
          for (int i = 0; i < 4; ++i)
          {
              out << points[i].x << " " << points[i].y << endl;
          }
          out << size.x << " " << size.y << endl;
          out.close();
      }
      done = true;
      break;
    case ' ':
      warp = !warp;
      break;
    case '0':
      selected = 0;
      break;
    case '1':
      selected = 1;
      break;
    case '2':
      selected = 2;
      break;
    case '3':
      selected = 3;
      break;
    case 'w':
      points[selected].y -= coarse ? 10 : 0.1;
      break;
    case 'a':
      points[selected].x -= coarse ? 10 : 0.1;
      break;
    case 's':
      points[selected].y += coarse ? 10 : 0.1;
      break;
    case 'd':
      points[selected].x += coarse ? 10 : 0.1;
      break;
    case 'l':
      line = !line;
      break;
    case 'c':
      coarse = !coarse;
      break;
    }

  }
    
  return 0;

}


    
