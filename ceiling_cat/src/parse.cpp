#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

using namespace std;
using namespace cv;

#include "parse.h"
#include "warp.h"
#include "transform2d.h"

#define PI 3.14159265358979

int parse_map(string filename, float scale, float xcam, float ycam, Size s, vector<string>& filenames, vector<Mat_<float> >& params)
{
    ifstream input(filename.c_str());
    Mat_<float> curwarp = param_mat(0, 0, 0);
    Transform2D S(scale, 0, 0, 0), offset(1, xcam, ycam, 0);
    Mat image;

    Transform2D qw(1,0,0,0), qw_prev(1,0,0,0), qm(1,0,0,0), T;

    while (input.good())
    {
        string line;
        getline(input, line);

        if (line == "") continue;

        Transform2D curxf(1, curwarp(0), curwarp(1), curwarp(2));


        if (line[0] == ' ' || line[0] == '\t')
        {
            istringstream unit(line);
            float tx, ty, theta;
            string fn;
            unit >> fn >> tx >> ty >> theta;
            cout << fn << " " << tx << " " << ty << " " << theta << endl;

            filenames.push_back(fn);
            image = imread("data/" + fn);
            //params.push_back(comp(curwarp, param_mat(tx*scale, ty*scale, theta*PI/180), image.size()));
            
            qw = Transform2D(1, tx, ty, theta*M_PI/180) * curxf;

            T = qm * S * offset * qw.inv();
            Transform2D delta = T * qw * qw_prev.inv() * T.inv();
            qm = delta * qm;

            qw_prev = qw;

            Transform2D& new_overall = qm;

            params.push_back(param_mat(new_overall.tx, new_overall.ty, new_overall.theta));            

        }
        else
        {
            istringstream unit(line);
            float tx, ty, theta;
            string setname;
            unit >> setname >> tx >> ty >> theta;

            curwarp = param_mat(tx, ty, theta*PI/180);
        }
    }

    return filenames.size();
}

/*
// ./parse scale scale 640 480 inputfile
int main(int argc, char *argv[])
{
    float scale = strtod(argv[1], 0), scale = strtod(argv[2], 0);
    int w = atoi(argv[3]), h = atoi(argv[4]);

    vector<string> filenames;
    vector<Mat_<float> > params;

    int N = parse(argv[5], scale, scale, Size(w, h), filenames, params);

    for (unsigned i = 0; i < N; ++i)
    {
        cout << filenames[i] << ": " << params[i](0,0) << ", " << params[i](1,0) << ", " << params[i](2,0) << endl;
    }

    return 0;
}
*/

