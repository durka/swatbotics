#!/usr/bin/env python

"""
For homework #2. Stitch together planar images using homographies.
"""

from cv import *
from cvk import *
from copy import deepcopy

def get_images(name, sources=None):
    camera = False
    if sources is None:
        # setup camera 640x480
        capt = CaptureFromCAM(0)
        SetCaptureProperty(capt, CV_CAP_PROP_FRAME_WIDTH, 640)
        SetCaptureProperty(capt, CV_CAP_PROP_FRAME_HEIGHT, 480)
        camera = True
    else:
        for i in range(len(sources)):
            sources[i] = LoadImage(sources[i])

    images = []
    points = []
    marked = []

    # gather as many images as the user wants
    # ENTER saves this image and moves on
    # ESC saves this image and stops gathering
    # SPACE freezes/unfreezes the camera feed
    i = 0
    while True:
        # new MPW for this set of points
        win = '%s%d' % (name, i)
        NamedWindow(win)
        mpw = MultiPointWidget('points')

        # start with the points from the previous image
        if len(points) > 0:
            # without deepcopy we end up modifying points[-1]
            mpw.points = deepcopy(points[-1])

        # show which image this is
        mpw.statusText = 'Image #%s' % (len(images)+1)

        # go!
        if camera:
            result = mpw.start(win, capt)
        else:
            mpw.start(win, sources[i])
            result = not (i+1 == len(sources))

        # save the source image (clean),
        #   the image buffer (has points on it),
        #   and the points
        images.append(CreateImage(GetSize(mpw.srcImage), \
                mpw.srcImage.depth, mpw.srcImage.channels))
        Copy(mpw.srcImage, images[i])
        points.append(mpw.points)
        marked.append(CreateImage(GetSize(mpw.imageBuffer), \
                mpw.imageBuffer.depth, mpw.imageBuffer.channels))
        Copy(mpw.imageBuffer, marked[i])

        i += 1
        if not result:
            break

    return images, points, marked

# stuff a list of tuples (points) into a CvMat
def points2mat(points):
    mat = CreateMat(len(points), len(points[0]), CV_32FC1)
    for i in range(len(points)):
        for j in range(len(points[i])):
            mat[i, j] = points[i][j]
    return mat

def mat2points(mat):
    points = []
    for r in range(mat.rows):
        points.append([])
        for c in range(mat.cols):
            points[r].append(mat[r, c])
        points[r] = tuple(points[r])
    return points

if __name__ == '__main__':
    from sys import argv
    
    # create GUI and gather images
    NamedWindow('Planar Image Stitching')

    if len(argv) == 2: # no images
        images, points, marked = get_images('Planar Image Stitching')
    else:
        images, points, marked = get_images('Planar Image Stitching', argv[2:])

    mats = [None]*len(points)
    homographies = []
    mats[0] = points2mat(points[0])

    # figure out how big to make the target image
    allcorners = []
    for i in range(len(images)):
        # find the homography from image #i to image #0
        homography = CreateMat(3, 3, CV_32FC1)
        mats[i] = points2mat(points[i])
        FindHomography(mats[i], mats[0], homography, CV_LMEDS)
        homographies.append(homography)

        # homogrify the corners
        size = GetSize(images[i])
        corners = [(0, 0), (size[0], 0), (0, size[1]), size]
        corners = points2mat(corners) # to a 4x2 1-channel cvMat
        corners = Reshape(corners, 2) # to a 4x1 2-channel cvMat
        PerspectiveTransform(corners, corners, homography)
        corners = Reshape(corners, 1) # to a 4x2 1-channel cvMat
        corners = mat2points(corners) # grr
        allcorners.extend(corners)
        
    # add translation and create large image to stitch into
    box = BoundingRect(allcorners)
    size = (box[2], box[3])
    H1 = points2mat([(1, 0, -box[0]), (0, 1, -box[1]), (0, 0, 1)])
    final = CreateImage(size, images[0].depth, images[0].channels)
    temp = CreateImage(size, images[0].depth, images[0].channels)

    for i in range(len(images)):
        # homogrify the image into the buffer
        GEMM(H1, homographies[i], 1, None, 0, homographies[i], 0) # premultiply
        WarpPerspective(images[i], temp, homographies[i], flags=CV_INTER_LINEAR)
        AddWeighted(temp, 1./len(images), final, 1, 0, final) # overlay
        
        # write image w/ points marked out to file
        SaveImage('%s_img%d.png' % (argv[1], i), marked[i])

    ShowImage('Planar Image Stitching', final)
    waitKey()

    # write stitched image out to file
    SaveImage('%s_final.png' % argv[1], final)

