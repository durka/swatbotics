#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <XmlRpcException.h>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <kdl/frames.hpp>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <fstream>
#include "lk.h"
#include "transform2d.h"

#define PROFILE

using namespace ros;
using namespace rosbag;
using namespace cv;
namespace enc = sensor_msgs::image_encodings;
using sensor_msgs::Image;
using nav_msgs::Odometry;
using geometry_msgs::PoseWithCovarianceStamped;
using KDL::Rotation;

enum DisplayType {
  DISPLAY_GRAY=0,
  DISPLAY_LAPLACE=1,
  DISPLAY_MASK=2,
};

DisplayType g_display = DISPLAY_GRAY;
bool g_overlay = true;

#ifdef PROFILE
#   define TIME(x) clock_t start__ = clock(); x; printf("%g s\n", (float)(clock() - start__)/CLOCKS_PER_SEC)
#else
#   define TIME(x) x
#endif
#define PI 3.14159265358979
enum ParamType {TX = 0, TY = 1, THETA = 2};

struct trackbar_info
{
  const Mat *big_grayscale, *big_laplace, *small_grayscale, *small_laplace, *mask;
  Transform2D *params;
  Mat H;
  ParamType param;
};

bool g_verbose = true, g_window = true;
float g_map_zoom = 0.5;

/*
 * callback for updating trackbars
 * updates zero or one of the image coordinates and redraws the stuff
 */
void track_callback(int val, void *data)
{
  trackbar_info *track = (trackbar_info*)data;

  const cv::Mat& small_grayscale = *(track->small_grayscale);
  int extra = sqrt(small_grayscale.rows*small_grayscale.rows + small_grayscale.cols*small_grayscale.cols);


  switch (track->param) {
  case TX:
    track->params->tx = val - extra;
    break;
  case TY:
    track->params->ty = val - extra;
    break;
  case THETA:
    track->params->theta = val*PI/180;
    break;
  }

  if (g_window) {
    
    Mat output, resized;
    
    const Mat& big = (g_display == DISPLAY_GRAY) ? *track->big_grayscale :
      (g_display == DISPLAY_MASK) ? *track->mask : *track->big_laplace;
    
    cv::Point ixform[4];

    if (g_overlay) {
	
      const Mat& small = (g_display == DISPLAY_LAPLACE) ? *track->small_laplace : *track->small_grayscale;

      cv::Mat_<float> M = cv::Mat_<float>::eye(3,3);
      float dx = (track->small_grayscale->cols - small.cols)*0.5;
      float dy = (track->small_grayscale->rows - small.rows)*0.5;
      M(0, 2) = dx;
      M(1, 2) = dy;
	
      //cv::Mat X = 
      cv::Mat_<float> X = M * warp(*track->params, small.size()) * M.inv() * track->H * M;

      warpPerspective(small, output, X, big.size());
      addWeighted(output, 0.5, big, 0.5, 0, output);

      cv::Point2f porig[4], pxform[4];
      cv::Mat morig(4, 1, CV_32FC2, porig);
      cv::Mat mxform(4, 1, CV_32FC2, pxform);
	
      porig[0] = cv::Point2f(0, 0);
      porig[1] = cv::Point2f(small.cols, 0);
      porig[2] = cv::Point2f(small.cols, small.rows);
      porig[3] = cv::Point2f(0, small.rows);

      perspectiveTransform(morig, mxform, X);
	
      for (int i=0; i<4; ++i) { ixform[i] = pxform[i]; }

      const Point* p = ixform;
      int npts = 4;

      cv::polylines(output, &p, &npts, 1, true, CV_RGB(255,255,255),
		    2/g_map_zoom, CV_AA);

	
    } else {
	
      output = big;
	
    }
      
    resize(output, resized, Size(0, 0), g_map_zoom, g_map_zoom);

    if (g_display == DISPLAY_LAPLACE) {
      //resized = resized / 5 + cv::Scalar(0.5,0.5,0.5,0.5);
      cv::Mat tmp;
      resized.convertTo(tmp, CV_8UC1, 255.0 / 5.0, 127);
      resized = tmp;
    }

    imshow("lucas-kanade", resized);

  }

}

/*
 * ImageGetter is a class for receiving image and odometry messages from the robot
 *
 * the get() method blocks on both topics
 */
class ImageGetter
{
private:
  NodeHandle& nh_; // NodeHandle for creating Subscribers
  Mat *small_grayscale_; // output camera image
  Transform2D *world_; // output odometry coordinates
  bool img_done_, odom_done_; // flags for the spin loop
  Subscriber img_sub_, odom_sub_; // topic subscribers
  bool real_data_; // data source (true: topics, false: bag)
  Bag bag_; // bag, view, and iterator for reading messages out of a bag
  View *view_;
  View::iterator view_iter_;

public:
  // constructor: needs the node's NodeHandle to make Subscribers
  ImageGetter(NodeHandle& nh, string camtopic, string odomtopic, string bagfn = "") : nh_(nh), view_(NULL)
  {
    if (bagfn != "") // we have a bag! sensors are right out
      {
	real_data_ = false;
	bag_.open(bagfn, bagmode::Read);

	// make an iterator to enumate all the messages in the bag, in order
	vector<string> topics(2);
	topics[0] = camtopic;
	topics[1] = odomtopic;
	view_ = new View(bag_, TopicQuery(topics)); // WTF? private copy constructor and no way to initialize except the constructor, so we have to use a pointer
	view_iter_ = view_->begin();
      }
    else // no bag, use sensors
      {
	real_data_ = true;
	// subscribe to topics
	img_sub_ = nh_.subscribe(camtopic, 1, &ImageGetter::image_callback, this);
	odom_sub_ = nh_.subscribe(odomtopic, 1, &ImageGetter::odom_callback, this);
      }
  }

  ~ImageGetter()
  {
    delete view_;
  }

  // the main user-facing method, which gets stuff from the places
  // and returns it into references
  bool get(Mat& small_grayscale, Transform2D& world)
  {
    // initialize pointers
    small_grayscale_ = &small_grayscale;
    world_ = &world;
    // haven't received any data yet
    img_done_ = false;
    odom_done_ = false;

    if (real_data_) // read from topics
      {
	if (g_verbose)
	  {
	    ROS_INFO("waiting for image and odometry info");
	  }

	// wait for stuff
	while (!(img_done_ && odom_done_))
	  {
	    spinOnce();
	  }
      }
    else // read from bag
      {
	while (!(img_done_ && odom_done_))
	  {
	    // check for end of bag file
	    if (view_iter_ == view_->end())
	      {
		ROS_INFO("There's no more stuff in that bag!");
		return false;
	      }

	    const MessageInstance m = *view_iter_++; // grab the next message from the bag
	    Image::ConstPtr img = m.instantiate<Image>(); // is it an image?
	    if (img != NULL)
	      {
		image_callback(img);
	      }
	    else
	      {
		// maybe it's odometry?
		PoseWithCovarianceStamped::ConstPtr pose = m.instantiate<PoseWithCovarianceStamped>();
		if (pose != NULL)
		  {
		    odom_callback(pose);
		  }
		else
		  {
		    // halp
		    ROS_INFO("unrecognized message!");
		  }
	      }
	  }
      }
    return true;
  }

  // called when a camera image comes in
  void image_callback(const Image::ConstPtr& msg)
  {
    if (!img_done_)
      {
	cv::Mat tmp = cv_bridge::toCvCopy(msg, enc::MONO8)->image;
	*small_grayscale_ = tmp;
	img_done_ = true;
	if (g_verbose)
	  {
	    ROS_INFO_STREAM("got image: " << small_grayscale_->size().width << "x" << small_grayscale_->size().height);
	  }
      }
  }

  // called when an odometry message comes in 
  void odom_callback(const PoseWithCovarianceStamped::ConstPtr& msg)
  {
    if (!odom_done_)
      {
	world_->tx = msg->pose.pose.position.x;
	world_->ty = msg->pose.pose.position.y;

	double roll, pitch, yaw; // roll and pitch are throwaway variables
	Rotation::Quaternion(msg->pose.pose.orientation.x,
			     msg->pose.pose.orientation.y,
			     msg->pose.pose.orientation.z,
			     msg->pose.pose.orientation.w).GetRPY(roll, pitch, yaw);
	world_->theta = yaw;

	odom_done_ = true;
	if (g_verbose)
	  {
	    ROS_INFO("got odometry: %g %g %g", world_->tx, world_->ty, world_->theta*180.0/PI);
	  }
      }
  }
};

// sensing step
//      1. grab new data from camera + odometry
//      2. update map coordinates using the difference in odometry data
bool incorporate_sensors(ImageGetter& iggy, Transform2D& qm_prev, Transform2D& qw, Transform2D& qm, 
			 const Transform2D& T, Mat& small_grayscale, Mat& small_laplace,
			 int lblur, const cv::Size& small_size) 
{
  Transform2D qw_prev = qw;
  if (!iggy.get(small_grayscale, qw)) // iggy updates the latest image and world coordinates
    {
      return false;
    }

  laplace(small_grayscale, small_laplace, lblur, small_size);

  // update map coordinates using the difference between new and old world coordinates
  Transform2D delta = T * qw * qw_prev.inv() * T.inv();
  //printf("delta: %g %g %g %g\n", delta.scale, delta.tx, delta.ty, delta.theta*180/PI);
  //printf("qw_prev: %g %g %g %g\n", qw_prev.scale, qw_prev.tx, qw_prev.ty, qw_prev.theta*180/PI);
  //printf("qw: %g %g %g %g\n", qw.scale, qw.tx, qw.ty, qw.theta*180/PI);

  qm_prev = qm;
  qm = delta * qm;

  return true;
}

// localization step
//      1. run Lucas-Kanade on the map and latest camera image to correct the map coordinates
//      2. convert map coordinates back into world frame and publish them
void run_LK(const Transform2D& qw, Transform2D& qm, Transform2D& T, 
	    const Transform2D& offset, const float scale, const Mat& H, 
	    const std::vector<Mat>& big_pyramid, 
	    const Mat& small_laplace,
	    const std::vector<Mat>& mask_pyramid, 
	    Odometry& msg, Publisher& pub, 
	    const int max_pyramid,
	    const int min_pyramid, 
	    const double epsilon,
	    const Transform2D& inset)
{

  cv::Mat_<float> M = cv::Mat_<float>::eye(3,3);
  M(0,2) = inset.tx;
  M(1,2) = inset.ty;

  ROS_INFO_STREAM("qm.scale = " << qm.scale << ", inset.scale = " << inset.scale << "\n");

  qm = inset * qm;
  ROS_INFO_STREAM("qm.scale = " << qm.scale << "\n");
  qm.scale = 1;

  printf("%g %g %g\n", qm.tx, qm.ty, qm.theta*180.0/PI);
  TIME(pyramidLK(big_pyramid, small_laplace, mask_pyramid, 
		 M.inv() * H * M, qm, max_pyramid, min_pyramid, epsilon));
  printf("%g %g %g\n", qm.tx, qm.ty, qm.theta*180.0/PI);
  qm = inset.inv() * qm;
  
  // construct transform from meters to pixels (NB "backwards" due to matrix multiplication)
  T = qm * Transform2D(scale, 0, 0, 0) * offset * qw.inv();
  
  printf("T: %g %g %g %g\n", T.scale, T.tx, T.ty, T.theta*180/PI);

  // transform position back to world frame and publish it
  Transform2D qw_out = T.inv() * qm;
  msg.pose.pose.position.x = qw_out.tx;
  msg.pose.pose.position.y = qw_out.ty;
  Rotation::RPY(0, 0, qw_out.theta).GetQuaternion(msg.pose.pose.orientation.x,
						  msg.pose.pose.orientation.y,
						  msg.pose.pose.orientation.z,
						  msg.pose.pose.orientation.w);
  pub.publish(msg);
  spinOnce(); // make sure it actually gets published
}

std::string getNextImage(const std::string& dirname) {
  char buf[1024];
  for (int i=0; i<10000; ++i) {
    snprintf(buf, 1024, "image%04d.png", i);
    std::string fullpath = dirname + "/" + buf;
    struct stat sb;
    if (stat(fullpath.c_str(), &sb) == -1 && errno == ENOENT) {
      return buf;
    }
  }
  ROS_INFO("TOO MANY IMAGES IN DIR %s", dirname.c_str());
  exit(1);
}

void makeMapDir(const std::string& dirname) {
  struct stat sb;
  int ok = stat(dirname.c_str(), &sb);
  if (ok == 0) {
    // check if directory
    if (S_ISDIR(sb.st_mode)) {
      ROS_INFO("directory %s already exists", dirname.c_str());
    } else {
      ROS_INFO("NOT A DIRECTORY: %s", dirname.c_str());
      exit(1);
    }
  } else if (errno == ENOENT) {
    // check if path doesn
    ok = mkdir(dirname.c_str(), 0777);
    if (ok == 0) {
      ROS_INFO("created %s", dirname.c_str());
    } else {
      ROS_INFO("ERROR CREATING DIRECTORY %s", dirname.c_str());
      exit(1);
    }
  } else {
    ROS_INFO("STAT FAILED FOR %s", dirname.c_str());
    exit(1);
  }
}

// map update step
//      1. place latest camera image onto map at current coordinates, with blurred edges so it fits
//      2. expand mask to include new image
void expand_map(Mat& big_grayscale, Mat& big_laplace, Mat& mask, 
		const Mat& small_grayscale, const Transform2D& qm, 
		const Mat& H, int lblur, 
		std::vector<Mat>& big_pyramid,
		std::vector<Mat>& mask_pyramid,
		int max_pyramid,
		const std::string& mapdir)
{

  cv::Mat_<float> X = warp(qm, small_grayscale.size())*H;

  feather_place(big_grayscale, mask, 
		small_grayscale, X,
		25);

  laplace(big_grayscale, big_laplace, lblur, big_grayscale.size());

  ROS_INFO("burned in image to map at (%g, %g, %g)", qm.tx, qm.ty, qm.theta*180/PI);
 
  buildPyramid(big_laplace, big_pyramid, max_pyramid);
  buildPyramid(mask, mask_pyramid, max_pyramid);

  std::string imgfile = getNextImage(mapdir);
  cv::imwrite(mapdir + "/" + imgfile, small_grayscale);

  std::string datafile = mapdir + "/data.txt";
  std::ofstream ostr(datafile.c_str(), std::ios_base::app);

  ostr << imgfile << "\n" 
       << qm.scale << " " 
       << qm.tx << " " 
       << qm.ty << " "
       << qm.theta*180/M_PI << "\n"
       << H << "\n"
       << X << "\n\n";

}

// ./interactive {big image} {camera topic} {odom topic} [bag filename]
// use sliders to adjust tx/ty/theta, press enter to try fitting, space to take new picture, c to toggle continuous mode
int main(int argc, char *argv[])
{

  init(argc, argv, "ceiling_cat");

  NodeHandle nh;
  Publisher pub;
  pub = nh.advertise<Odometry>("odom_out", 10);
  Odometry msg;

  bool continuous = false, autobahn = false;

  Transform2D offset, qm_prev;

  Mat dummy = Mat::zeros(1, 1000, CV_8UC1);
  double scale = 400;

  Mat small_grayscale, small_laplace;
  Transform2D qm, qw, qw_prev, qw_out; // scale=1
  Transform2D T(scale, 0, 0, 0);

  string bagfn = "";
  if (argc > 4) {
    bagfn = argv[4];
  }

  ImageGetter iggy(nh, argv[2], argv[3], bagfn);

  // homography to transform from camera image (keystoned) to rectified (non-keystoned)
  Mat_<float> H(3, 3);


  int min_pyramid = 1;
  int max_pyramid = 5;
  int inset_x = 50;
  int inset_y = 50;
  int lblur = 17;

  double epsilon = 1e-5;
  param::get("/ceiling_cat/max_pyramid", max_pyramid);
  param::get("/ceiling_cat/min_pyramid", min_pyramid);
  param::get("/ceiling_cat/inset_x", inset_x);
  param::get("/ceiling_cat/inset_y", inset_y);
  param::get("/ceiling_cat/lblur", lblur);
  param::get("/ceiling_cat/epsilon", epsilon);

  if (param::has("/turtlebot_node/scale"))
    {
      double ptx, pty, ptheta, ox, oy;
      param::get("/turtlebot_node/tx", ptx);
      param::get("/turtlebot_node/ty", pty);
      param::get("/turtlebot_node/theta", ptheta);
      param::get("/turtlebot_node/scale", scale);
      param::get("/turtlebot_node/cam_off_x", ox);
      param::get("/turtlebot_node/cam_off_y", oy);
      qm.tx = ptx; qm.ty = pty; qm.theta = ptheta;
      offset.tx = ox;
      offset.ty = oy;

      ROS_INFO("camera offset: (%g, %g)", ox, oy);

      XmlRpc::XmlRpcValue keystone;
      param::get("/ceiling_cat/keystone", keystone);
      for (int i = 0; i < 3; ++i)
        {
	  for (int j = 0; j < 3; ++j)
            {
	      try
                {
		  H(i, j) = static_cast<double>(keystone[i][j]);
                }
	      catch (XmlRpc::XmlRpcException e)
                {
		  cout << "XmlRpcException while retrieving keystone matrix: " << i << "," << j << " " << e.getMessage() << endl;
                }
            }
        }
      ROS_INFO_STREAM("keystone matrix: " << keystone);
    }


  // make the map directory
  makeMapDir(argv[1]);
  
  Mat big_grayscale = imread(argv[1] + string(".png"), CV_LOAD_IMAGE_GRAYSCALE);
  Mat big_laplace;
  
  std::vector<Mat> big_pyramid, mask_pyramid;

  laplace(big_grayscale, big_laplace, lblur, big_grayscale.size());


  Mat mask = imread(argv[1] + string(".mask.png"), 0);
  if (mask.data == NULL) {
    mask = Mat::ones(big_grayscale.size(), CV_8UC1);
  }

  buildPyramid(big_laplace, big_pyramid, max_pyramid);
  buildPyramid(mask, mask_pyramid, max_pyramid);

  // initialize odometry message with the correct covariance and stuff
  msg.header.frame_id = "base_footprint";
  msg.pose.pose.position.x = msg.pose.pose.position.y = msg.pose.pose.position.z = 0;
  msg.pose.pose.orientation.x = 1;
  msg.pose.pose.orientation.y = msg.pose.pose.orientation.z = msg.pose.pose.orientation.w = 0;
  msg.twist.twist.linear.x = msg.twist.twist.linear.y = msg.twist.twist.linear.z = 0;
  msg.twist.twist.angular.x = msg.twist.twist.angular.y = msg.twist.twist.angular.z = 0;
  for (int i = 0; i < 6; ++i)
    {
      for (int j = 0; j < 6; ++j)
        {
	  if (i == j)
            {
	      if (i == 0 || i == 1 || i == 5) // we measure x/y pos, z orientation
                {
		  msg.pose.covariance[i*6+j] = 1e-10; // beat wheel odom by an order of magnitude
                }
	      else // other stuff is not measured
                {
		  msg.pose.covariance[i*6+j] = 1000000.0;
                }
	      msg.twist.covariance[i*6+j] = 1000000.0;
            }
	  else
            {
	      msg.pose.covariance[i*6+j] = 0;
	      msg.twist.covariance[i*6+j] = 0;
            }
        }
    }


  iggy.get(small_grayscale, qw);

  cv::Size small_laplace_size = small_grayscale.size();
  small_laplace_size.width -= 2*inset_x;
  small_laplace_size.height -= 2*inset_y;

  laplace(small_grayscale, small_laplace, lblur, small_laplace_size);

  Transform2D inset(1, inset_x, inset_y, 0);

  trackbar_info track_tx =    {&big_grayscale, &big_laplace, &small_grayscale, &small_laplace, &mask, &qm, H, TX};
  trackbar_info track_ty =    {&big_grayscale, &big_laplace, &small_grayscale, &small_laplace, &mask, &qm, H, TY};
  trackbar_info track_theta = {&big_grayscale, &big_laplace, &small_grayscale, &small_laplace, &mask, &qm, H, THETA};
  trackbar_info track_null =  {&big_grayscale, &big_laplace, &small_grayscale, &small_laplace, &mask, &qm, H, (ParamType)-1};

  int itx = qm.tx, ity = qm.ty, itheta = qm.theta*180/PI;

  if (g_window) {
    namedWindow("lucas-kanade");
    namedWindow("controls");
    
    int extra = sqrt(small_grayscale.rows*small_grayscale.rows + small_grayscale.cols*small_grayscale.cols);
    
    createTrackbar("tx   ", "controls", &itx, big_grayscale.cols + extra, track_callback, &track_tx);
    createTrackbar("ty   ", "controls", &ity, big_grayscale.rows + extra, track_callback, &track_ty);
    createTrackbar("theta", "controls", &itheta, 360, track_callback, &track_theta);
    imshow("controls", dummy);
    track_callback(0, &track_null);
  }

  bool loop = true;
  while (loop) {

    bool sense = false;
    bool match = false;
    bool burn = false;
    bool display = false;

    switch (waitKey(10)) {

    case '-':
      g_map_zoom /= 2;
      display = true;
      break;
    case '+':
    case '=':
      g_map_zoom *= 2;
      display = true;
      break;
    case 'd':
      g_display = DisplayType((g_display + 1) % 3);
      display = true;
      break;
    case 'e':
      g_display = DisplayType((g_display - 1) % 3);
      display = true;
      break;
    case 'k':
      g_overlay = !g_overlay;
      display = true;
      break;
    case 'h': // 'h' : print help
      printf("Interactive key commands:\n"
	     "\tSpace : fetch new camera image\n"
	     "\tEnter : run Lucas-Kanade\n"
	     "\tEscape: save map and exit\n"
	     "\td or e: change the current display mode (grayscale, laplace, mask)\n"
	     "\t k    : toggle overlay of current image\n"
	     "\t b    : burn in current image to map\n"
	     "\t c    : toggle continuous mode (Space/Enter loop)\n"
	     "\t m    : toggle autobahn mode (Space/Enter/b loop)\n"
	     "\t o    : print offset calibration info\n"
	     "\t+ or =: zoom in\n"
	     "\t -    : zoom out\n"
	     "\n"
	     "\tcontinuous localization: load a map, use sliders to find the initial position, press Enter, then c and drive around\n"
	     "\tautomatic map-making: load a blank image, move the first image to a good position, press b to start the map, then C and drive around\n"
	     "\n");
      break;
    case 13: //enter : run matching algorithm
    case 10:
      match = true;
      display = true;
      break;
    case 27: //escape : exit
      // save position for next time
      param::set("/turtlebot_node/tx", qm.tx);
      param::set("/turtlebot_node/ty", qm.ty);
      param::set("/turtlebot_node/theta", qm.theta);
      // save map
      //big.convertTo(derp, CV_8UC1, 255.0, 0);
      imwrite(argv[1] + string(".png"), big_grayscale);
      imwrite(argv[1] + string(".mask.png"), mask);
      loop = false;
      break;
    case 'm': // 'm' : auto-burn-in-to-map
      autobahn = !autobahn;
      // fall through to continuous
    case 'c': // 'c' : toggle continuous mode
      if (continuous) {
	continuous = false;
	g_verbose = true;
      } else {
	continuous = true;
	g_verbose = false;
      }
      break;
    case ' ': //space : take new picture
      ROS_INFO("say cheese");
      sense = true;
      break;
    case 'b': // 'b' : burn in to map
      ROS_INFO("burn baby burn");
      burn = true;
      break;
    case 'o': // 'o' : print offset calibration info
      {
	Transform2D delta = qm_prev.inv() * qm;

	ROS_INFO("calculating camera offset from a turn of %g degrees", delta.theta*180/M_PI);
	if (offset.tx != 0 || offset.ty != 0) {
	  ROS_INFO("Warning! An offset is already set. The following values are relative.");
	}

	Mat_<float> k(2, 1), I = Mat_<float>::eye(2, 2), R(2, 2);
	R(0,0) = cos(delta.theta); R(0,1) = -sin(delta.theta);
	R(1,0) = sin(delta.theta); R(1,1) = cos(delta.theta);
	k(0) = delta.tx;
	k(1) = delta.ty;

	Mat_<float> t = (R - I).inv() * k;
	ROS_INFO("cam_off_x : %g", -t(0)/scale);
	ROS_INFO("cam_off_y : %g", t(1)/scale);
      }
      break;
    default:
      break;
    }

    if (continuous) { // in continuous mode, take a new picture and match it
      sense = true;
      match = true;
    }

    if (autobahn) { 
      burn = true; 
    }


    if (sense) {
      if (!incorporate_sensors(iggy, qm_prev, qw, qm, T, 
			       small_grayscale, small_laplace, 
			       lblur, small_laplace_size)) {
	continuous = false;
	autobahn = false;
	match = false;
	burn = false;
      } else {
	display = true;
      }
    }

    if (match) {
      run_LK(qw, qm, T, offset, scale, H, big_pyramid, small_laplace, 
	     mask_pyramid, msg, pub, max_pyramid, min_pyramid, epsilon, inset);
      display = true;
    }


    if (burn) {
      expand_map(big_grayscale, big_laplace, mask, small_grayscale, \
		 qm, H, lblur, big_pyramid, mask_pyramid,
		 max_pyramid, argv[1]);
      display = true;
    }
    
    if (g_window && display) {
      track_callback(0, &track_null);
    }
    

  } 

  return 0;

}

