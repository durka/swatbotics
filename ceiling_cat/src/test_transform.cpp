#include <iostream>
#include <cstdlib>
#include <ctime>
#include "transform2d.h"

int main()
{
    srand(time(NULL));

    Transform2D T(rand()%500, rand()%100, rand()%100, rand()%6);
    float x = rand()%100, y = rand()%100, theta = rand()%6;

    cout << "T: " << T.scale << " " << T.tx << " " << T.ty << " " << (T.theta*180/3.14) << endl;

    cout << "x: " << x << ", y: " << y << ", theta: " << theta << endl;
    T.apply(x, y, theta);
    cout << "x: " << x << ", y: " << y << ", theta: " << theta << endl;
    T.inv().apply(x, y, theta);
    cout << "x: " << x << ", y: " << y << ", theta: " << theta << endl;

    cout << (cv::Mat)(T*T.inv()) << endl;
    cout << (cv::Mat)(T.inv()*T) << endl;

    cout << (cv::Mat)T << endl;
    cout << (cv::Mat)(Transform2D(1, T.tx, T.ty, 0) * Transform2D(1, 0, 0, T.theta)) * Transform2D(T.scale, 0, 0, 0) << endl;


    Transform2D T1(0.5, 1, 2, 0.05);
    Transform2D T2(0.6, 7, 4, 0.45);

    cout << "(cv::Mat)(T1 * T2) = " << (cv::Mat)(T1*T2) << "\n";
    cout << "((cv::Mat)T1) * ((cv::Mat)T2) = " << ((cv::Mat)T1) * ((cv::Mat)T2) << "\n";

    return 0;
}

