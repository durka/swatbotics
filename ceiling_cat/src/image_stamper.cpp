#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <boost/foreach.hpp>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Pose.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Image.h>
#include <kdl/frames.hpp>
#include <sstream>
#include <fstream>

using namespace ros;
using namespace cv;
namespace enc = sensor_msgs::image_encodings;
using geometry_msgs::PoseWithCovarianceStamped;
using geometry_msgs::Pose;
using sensor_msgs::Image;
using KDL::Rotation;

Pose g_pos;
map<string, Pose> g_places;

string decode_pose(Pose p)
{
    double x, y, theta, _roll, _pitch;

    x = p.position.x;
    y = p.position.y;

    Rotation::Quaternion(p.orientation.x,
                         p.orientation.y,
                         p.orientation.z,
                         p.orientation.w).GetRPY(_roll, _pitch, theta);

    ostringstream os;
    os << x << " " << y << " " << (theta*180/M_PI);

    return os.str();
}

class ImageCallback
{
    private:
        string m_name, m_prefix;
        Subscriber m_sub;
        int m_counter;

        static string itoa(int n)
        {
            ostringstream os;
            os << n;
            return os.str();
        }

    public:
        ImageCallback() {}
        
        void initialize(NodeHandle& nh, string p, string n)
        {
            ROS_INFO("subscribing to %s", n.c_str());
            m_name = n;
            m_prefix = p;
            //m_sub = nh.subscribe("/" + m_name + "/gscam/image_raw", 1, &ImageCallback::photo, this);
            m_counter = 0;
        }

        ~ImageCallback()
        {
            //m_sub.shutdown();
        }

        string name()
        {
            return m_name;
        }

        void photo(const Image::ConstPtr& msg)
        {
            //ROS_INFO("image received from %s at time ", m_name.c_str());
            ROS_INFO_STREAM("image received from " << m_name << " at time " << msg->header.stamp);
            Mat image = cv_bridge::toCvCopy(msg, enc::MONO8)->image;

            string filename = m_name + itoa(m_counter++);
            imwrite(m_prefix + filename + ".png", image);
            g_places[filename] = g_pos;
        }
};

void tick(const PoseWithCovarianceStamped::ConstPtr& msg)
{
    g_pos = msg->pose.pose;
    ROS_INFO_STREAM("new position: " << decode_pose(g_pos) << " at time " << msg->header.stamp);
}

int main(int argc, char *argv[])
{
    init(argc, argv, "image_stamper");
    NodeHandle nh;

    // arg parsing: ./image_stamper {out file prefix} {out image prefix} {bag filename} {# cams} {cam names...}
    const string outfn = argv[1];
    const string prefix = argv[2];
    const string bagfn = argv[3];
    const int cams = atoi(argv[4]);
    const int CAM_ARG_OFFSET = 5;

    rosbag::Bag bag;
    bag.open(bagfn, rosbag::bagmode::Read);
    vector<string> topics;
    topics.push_back("/robot_pose_ekf/odom_combined");
    for (int i = 0; i < cams; ++i)
    {
        topics.push_back("/" + string(argv[CAM_ARG_OFFSET+i]) + "/gscam/image_rect_throttle");
    }
    rosbag::View view(bag, rosbag::TopicQuery(topics));

    ImageCallback images[cams];
    for (int i = 0; i < cams; ++i)
    {
        images[i].initialize(nh, prefix, argv[CAM_ARG_OFFSET+i]);
    }

    BOOST_FOREACH(rosbag::MessageInstance const m, view)
    {
        // is it an image?
        Image::ConstPtr img = m.instantiate<Image>();
        if (img != NULL)
        {
            for (int i = 0; i < cams; ++i)
            {
                if (m.getTopic() == ("/" + images[i].name() + "/gscam/image_rect_throttle"))
                {
                    images[i].photo(img);
                }
            }
        }
        else
        {
            // maybe it's odometry?
            PoseWithCovarianceStamped::ConstPtr pose = m.instantiate<PoseWithCovarianceStamped>();
            if (pose != NULL)
            {
                tick(pose);
            }
            else
            {
                cout << "unrecognized message!" << endl;
            }
        }
    }

    cout << "finishing..." << endl;
    for (int i = 0; i < cams; ++i)
    {
        ofstream out((outfn + argv[CAM_ARG_OFFSET+i] + string(".map")).c_str());
        out << "initial 0 0 0" << endl;
        for (map<string, Pose>::iterator j = g_places.begin(); j != g_places.end(); ++j)
        {
            if (j->first.compare(0, strlen(argv[CAM_ARG_OFFSET+i]), argv[CAM_ARG_OFFSET+i]) == 0)
            {
                out << "\t" << j->first << ".png " << decode_pose(j->second) << endl;
            }
        }
    }
}

