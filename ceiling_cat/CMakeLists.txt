cmake_minimum_required(VERSION 2.4.6)
include($ENV{ROS_ROOT}/core/rosbuild/rosbuild.cmake)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries
set(ROS_BUILD_TYPE Debug)

rosbuild_init()

#set the default path for built executables to the "bin" directory
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
#set the default path for built libraries to the "lib" directory
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

#uncomment if you have defined messages
#rosbuild_genmsg()
#uncomment if you have defined services
#rosbuild_gensrv()

# binaries
#rosbuild_add_executable(catographer src/catographer.cpp)
rosbuild_add_executable(interactive src/interactive.cpp)
rosbuild_add_executable(rectifier src/rectifier.cpp)
#rosbuild_add_executable(keystone src/keystone.cpp)
#rosbuild_add_executable(test_transform src/test_transform.cpp)
#rosbuild_add_executable(image_stamper src/image_stamper.cpp)

rosbuild_add_library(parse src/parse.cpp)
rosbuild_add_library(warp src/transrot.cpp) # CHANGE THIS TO USE A DIFFERENT WARP
rosbuild_add_library(lk src/lk.cpp)

#target_link_libraries(catographer lk warp parse)
target_link_libraries(interactive lk warp)
#target_link_libraries(keystone lk warp)
#target_link_libraries(parse warp)
#target_link_libraries(lk warp)

#common commands for building c++ executables and libraries
#rosbuild_add_library(${PROJECT_NAME} src/example.cpp)
#target_link_libraries(${PROJECT_NAME} another_library)
#rosbuild_add_boost_directories()
#rosbuild_link_boost(${PROJECT_NAME} thread)
#rosbuild_add_executable(example examples/example.cpp)
#target_link_libraries(example ${PROJECT_NAME})
