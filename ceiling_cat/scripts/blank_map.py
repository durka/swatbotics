#!/usr/bin/env python

import os
import sys
import cv

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print 'Usage: %s {name} {height} {width}' % sys.argv[0]
        sys.exit(1)

    name = sys.argv[1]
    height = int(sys.argv[2])
    width = int(sys.argv[3])

    if os.path.exists('data/%s.png' % name):
        if os.path.exists('data/%s.mask.png' % name):
        	print 'Map and mask already exist. Not overwriting.'
        else:
            print 'data/%s.png already exists. Creating data/%s.mask.png.' % (name, name)
            map = cv.LoadImage('data/%s.png' % name)
            if map.height != height or map.width != width:
                print 'Warning! given dimensions do not match dimensions of data/%s.png (%d x %d). Creating data/%s.mask.png with dimensions given on command line.' % (name, map.height, map.width, name)
            mask = cv.CreateMat(height, width, cv.CV_8UC1)
            cv.Set(mask, 1)
            cv.SaveImage('data/%s.mask.png' % name, mask)
    else:
    	print 'Creating data/%s.png and data/%s.mask.png' % (name, name)
    	img = cv.CreateMat(width, height, cv.CV_8UC1)
    	cv.Set(img, 0)
        if os.path.exists('data/%s.png' % name):
        	print 'Map already exists. Not overwriting.'
        else:
            cv.SaveImage('data/%s.png' % name, img)
        if os.path.exists('data/%s.mask.png' % name):
        	print 'Mask already exists. Not overwriting.'
        else:
            cv.SaveImage('data/%s.mask.png' % name, img)

