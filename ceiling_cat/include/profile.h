#ifndef _PROFILE_H_
#define _PROFILE_H_

#ifdef PROFILE
    clock_t start__, startl__, *timel__;
    int timel_n__, timel_i__;
#   define TIME(s,x) start__ = clock(); x; printf("%s %g s\n", (s), (float)(clock() - start__)/CLOCKS_PER_SEC)
    inline void TIMEl_init(int n)
    {
        timel_n__ = n;
        timel_i__ = 0;
        timel__ = (clock_t*)malloc(sizeof(clock_t) * n);
        for (int i = 0; i < n; ++i)
        {
            timel__[i] = 0;
        }
    }
    inline void TIMEl_loop() { timel_i__ = 0; }
#   define TIMEl(x) startl__ = clock(); x; timel__[timel_i__++] += (clock() - startl__)
    inline void TIMEl_end(const char *s...)
    {
        va_list ss;
        va_start(ss, s);

        printf("\t%s %g s\n", s, (float)timel__[0]/CLOCKS_PER_SEC);
        for (int i = 1; i < timel_n__; ++i)
        {
            s = va_arg(ss, const char*);
            printf("\t%s %g s\n", s, (float)timel__[i]/CLOCKS_PER_SEC);
        }

        va_end(ss);
        timel_n__ = 0;
        free(timel__);
    }
#else
#   define TIME(s,x) x
    inline void TIMEl_init(int n) {}
    inline void TIMEl_loop() {}
#   define TIMEl(x) x
    inline void TIMEl_end(const char *s...) {}
#endif

#endif

