#ifndef _WARP_H_
#define _WARP_H_

#include "transform2d.h"

Transform2D warp(Transform2D p, const Size& s);
Mat warp(const Mat& p, const Size& s);
void jacobian(const Size& s, Mat* jx, Mat* jy);
Mat comp(const Mat& p1, const Mat& p2, const Size& s);
Mat invcomp(const Mat& p1, const Mat& p2);
Mat param_mat(float tx, float ty, float theta);

#endif

