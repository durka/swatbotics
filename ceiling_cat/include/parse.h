#ifndef _PARSE_H_
#define _PARSE_H_

int parse_map(string filename, float scale, float xcam, float ycam, Size s, vector<string>& filenames, vector<Mat_<float> >& params);

#endif

