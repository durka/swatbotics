#ifndef _LK_H_
#define _LK_H_

#include <opencv2/opencv.hpp>
using namespace cv;

#include "transform2d.h"
#include "warp.h"

void pyramidLK(const std::vector<Mat>& big, 
	       const Mat& templ, 
	       const std::vector<Mat>& mask, 
	       const Mat& camera, 
	       Transform2D& params, 
	       int levels, 
	       int minlevels=0, 
	       const double epsilon=1e-4);

void displayScaledHappy(const char* name, const Mat& image);
Mat laplace_read(const char *filename);

void laplace(const Mat& src, Mat& dst, 
	     int blur_size,
	     const Size& dst_size);

void feather_place(Mat& map, Mat& mask, const Mat& image, const Mat& WH, int fsize);

#endif

