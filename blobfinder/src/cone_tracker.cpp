/*cone_tracker by Jackie Kay
Last updated 6/7/11
State machine-based solution to following a sequence of colored cones
*/

#include <ros/ros.h>
#include <rosbag/bag.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/Odometry.h>
#include <opencv2/opencv.hpp>
#include <opencv/highgui.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <kdl/frames.hpp>
#include <vector>
#include "ColorLUT.cpp"
#include "transform2d.h"

namespace enc = sensor_msgs::image_encodings;
typedef vector<vector<cv::Point> > PointVecs;
typedef vector<cv::Point> PointVec;
typedef pcl::PointCloud<pcl::PointXYZRGB> PointCloud;

enum{
minarea = 200, //Minimum area for an object to be considered a cone
};

enum statetype {
  go_forward = 0,
  turn = 1,
  idle = 2,
  go_around = 3
};

ros::Publisher pub;
ColorLUT lookup;
Transform2D odom;
cv::Point2f conetarget; //Point to the right of the cone
cv::Point2f tangent; //Tangent vector
cv::Point2f P;
vector<string> color_list;

bool showContourWindow=false;
bool loopColors=false;
bool passLeft=true;
bool verbose=false;

const float rcone = 0.044;  //Radius of cone
const float rbot =  0.1575; 
const float safety = 0.05;
const float d = 1;
double hz = 10;
double kp = 0.005;
double alpha = 0.8;
static int idx=0; //Current index in the vector of colors to visit
static int size=0;  //Size of colors vector
static char s=go_forward;  //State counter

/*Convert RGB image to thresholded image*/
void convertImage(cv::Mat img, PointVecs& contours){
  string color = color_list[idx];
  cv::Mat result(img.rows, img.cols, CV_8U);
  cv::Mat element=cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(6,6));

  lookup.getImageColor(img, lookup.lookupColor(color), result);
  cv::morphologyEx(result, result, cv::MORPH_OPEN, element);
  cv::findContours(result, contours, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
  if(showContourWindow){
    cv::drawContours(result, contours, -1, CV_RGB(255,255,255));
    cv::imshow("contour window", result);
    cv::waitKey(5);
  }
}

double area(PointVec vec){
  double area;
  cv::Moments m = cv::moments(cv::Mat(vec));
  area = m.m00;
  return area;
}

/*Analyze the contours. Returns by reference area of largest blob (or -1 if no
contours of the right size found) and xbar of the blob*/
void analyzeContours(PointVecs contours, float& xpos, float& ypos){
  xpos = 0;
  if(contours.size() == 0){
    //ROS_INFO("No contours found!");
    ypos = -1;
    return;
  }
  PointVec c=contours[0];
  double prev = area(c);
  //Choose the largest contour

  for(unsigned int i = 1; i<contours.size(); i++){
  	if(area(contours[i]) > prev){
  		c=contours[i];
  		prev = area(c);
  	}
  }

  if(area(c) > minarea){
    cv::Moments m = cv::moments(cv::Mat(c));
    xpos = m.m10 / m.m00;
    ypos = m.m01 / m.m00;
  } else {
    ypos = -1;
  }
}


void convertAnalyze(cv::Mat img, float& xpos, float& ypos){
  PointVecs contours;
  convertImage(img, contours);
  analyzeContours(contours, xpos, ypos);
}

bool getNextColor(){
  if(verbose) ROS_INFO("Getting next color");
  if(idx==(size-1)){
    return false;
  } else {
    idx++;
    return true;
  }
}

void publishForward(float xpos){
  geometry_msgs::Twist out;
  out.linear.x = 0.1;
   out.angular.z = -0.0045*(xpos-320.0);
   pub.publish(out);
}

/*When no cone is found, turn in place*/
void publishTurn(){
  if(verbose) ROS_INFO("Lost cone. Turning...");
  geometry_msgs::Twist out;
  out.angular.z = -0.8;
  pub.publish(out);
}


void getPclData(const PointCloud::ConstPtr& msg, cv::Mat& image, cv::Mat& posdata)
{
  int offset = 0;
  for (uint32_t y=0; y<msg->height; ++y) {
   for (uint32_t x=0; x<msg->width; ++x) {
     const pcl::PointXYZRGB& pt = msg->points[offset];
     cv::Vec3b* v = (cv::Vec3b*)(&pt.rgb);
     image.at<cv::Vec3b>(y, x) = *v;
     posdata.at<cv::Vec3f>(y, x) = cv::Vec3f(pt.x, pt.y, pt.z);
     ++offset;
   }
  }
}

/*Get entire XYZ position of cone*/
cv::Vec3f getPos(float xpos, float ypos, const cv::Mat& posdata){
  return posdata.at<cv::Vec3f>((int) ypos, (int) xpos);
}

void goForward(const cv::Mat& img, const cv::Mat& posdata,
              cv::Vec3f& conepos, bool& foundCone, bool& closeEnough){
  float xpos, ypos;

  convertAnalyze(img, xpos, ypos);
  if(ypos==-1){
    foundCone = false;
    return;
  } else {
    foundCone = true;
    conepos = getPos(xpos, ypos, posdata);
    if(verbose) ROS_INFO("Depth: %f", conepos[2]);
    closeEnough = (conepos[2]<d);
  }
  publishForward(xpos);
}

bool lookForCone(const cv::Mat& img){
  float xpos, ypos;
  publishTurn();
  convertAnalyze(img, xpos, ypos);
  return (ypos!=-1);
}

void calcPoints(cv::Vec3f conepos){
  float r = rcone + rbot + safety;
  
  if(passLeft){
    conepos[0] = -conepos[0] + r;
  } else {
    conepos[0] = -conepos[0] - r;
  }
  conetarget = odom * cv::Point2f(conepos[2], conepos[0]);

  cv::Mat_<float> M = cv::Mat(odom);
  tangent = cv::Point2f(M(0,0), M(1,0) );
  if(verbose){
   ROS_INFO("Cone position in world frame: [ %f, %f ]",
           conetarget.x, conetarget.y);
   ROS_INFO("Tangent vector in world frame: [ %f, %f ]",
            tangent.x, tangent.y);
  }
}

bool goAround(){
  cv::Point2f Q(odom.tx, odom.ty);

  float k = tangent.dot(Q-conetarget);
  if(verbose)ROS_INFO("k = %f", k);
  if(k>0){
    if(verbose) ROS_INFO("Passed cone");
    return false;
  }
  //publish movement
  cv::Point2f Pc = conetarget + k*tangent;
  cv::Point2f Pa = Pc + alpha*tangent;
  cv::Point2f r = odom.inv()*Pa;
  float y = r.y;
  if(verbose){
    ROS_INFO("y = %f", y);
    ROS_INFO("Projection of P onto line t: [ %f, %f ]", Pc.x, Pc.y);
    ROS_INFO("Pa=[%f, %f]", Pa.x, Pa.y);
    ROS_INFO("r= [%f, %f]", r.x, r.y);
  }
  geometry_msgs::Twist out;
  out.linear.x = 0.1;
  out.angular.z = kp*y;
  pub.publish(out);
  return true;
}

void callBack(const PointCloud::ConstPtr& msg){
  bool closeEnough, foundCone;
  cv::Vec3f conepos;
  cv::Mat img = cv::Mat::zeros(msg->height, msg->width, CV_8UC3);
  cv::Mat posdata =cv::Mat::zeros(msg->height, msg->width, CV_32FC3);
  getPclData(msg, img, posdata);

  switch(s){
    case go_forward:
      goForward(img, posdata, conepos, foundCone, closeEnough);
      if(!foundCone){
        s=turn;
      } else if (closeEnough){
        s = go_around;
        calcPoints(conepos);
      }
      break;
    case turn:
      if(lookForCone(img)){
        s=go_forward;
      }
      break;
    case idle:
      if(verbose) ROS_INFO("Idle state reached");
      break;
    case go_around:
      if(!goAround()){
  			if(!getNextColor()){
  				if(loopColors){
  					idx = 0;
  					s = go_forward;
  				} else { 
  			    s = idle;
  				}
  			} else {
  				s = go_forward;
  			}
  		}
      break;
  }
}

void getOdometry(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
{
  float tx, ty, theta;
  tx = msg->pose.pose.position.x;
  ty = msg->pose.pose.position.y;

  double roll, pitch, yaw;
  KDL::Rotation::Quaternion(msg->pose.pose.orientation.x,
                       msg->pose.pose.orientation.y,
                       msg->pose.pose.orientation.z,
                       msg->pose.pose.orientation.w).GetRPY(roll, pitch, yaw);
  theta = yaw;
  odom.tx = tx;
  odom.ty = ty;
  odom.theta = theta;
  if(verbose) ROS_INFO("Odometry: x = %g, y = %g, theta = %g",
            odom.tx, odom.ty, odom.theta);
}

void getParams(ros::NodeHandle nh){
  string data;
  XmlRpc::XmlRpcValue cone_list;
  if( nh.getParam("conedatafile", data) &&
               nh.getParam("coneslist", cone_list) ) {
    lookup.load(data);
    ROS_ASSERT(cone_list.getType() == XmlRpc::XmlRpcValue::TypeArray);
    for(int i = 0; i<cone_list.size(); i++){
      ROS_ASSERT(cone_list[i].getType() == XmlRpc::XmlRpcValue::TypeString);
      color_list.push_back(cone_list[i]);
      size++;
    }
  } else {
    ROS_INFO("Couldn't load data files! Check your rosparams.");
    return;
  }
  if(nh.getParam("camerahz", hz)){
    ROS_INFO("hz=%f", hz);
  }
  if(nh.getParam("kp", kp)){
    ROS_INFO("kp=%f", kp);
  }
  if(nh.getParam("alpha", alpha)){
    ROS_INFO("alpha=%f", alpha);
  }
  if(nh.getParam("show_window", showContourWindow)&&showContourWindow){
  	ROS_INFO("Showing contour window");
  }
  if(nh.getParam("loop_colors", loopColors)&&loopColors){
  	ROS_INFO("Looping through color list");
  }
  if(nh.getParam("pass_left", passLeft)){
    if(passLeft){
  	  ROS_INFO("Will pass cone on left side");
    } else {
  	  ROS_INFO("Will pass cone on right side");
    }
  }
  if(nh.getParam("verbose", verbose)&&verbose){
  	ROS_INFO("Verbose output on");
  }
}

int main(int argc, char** argv){
  ros::init(argc, argv, "cone_tracker");
  ros::NodeHandle nh;

  ros::Subscriber sub = nh.subscribe("/camera/rgb/points", 1, callBack);
  ros::Subscriber odom_sub = nh.subscribe("/robot_pose_ekf/odom_combined", 1,
    getOdometry);
  pub = nh.advertise<geometry_msgs::Twist>(
                      "turtlebot_node/cmd_vel", 1);
  getParams(nh);
  if(showContourWindow){
    cv::namedWindow("contour window");
  }
  ros::spin();
}
