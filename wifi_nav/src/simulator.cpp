/*Particle filter for wifi navigation simulator
Written by Jackie Kay
TODO: grid scaling--better solution than making an n*s by n*s matrix

Output file format:
Grid[i][j] = [ ];

Iteration [N]:
Initial particle positions:

Measured values:
x_sim=
z_sim=
Weights:
weights[i]=
...
weights[k]=
New particle positions:
newparticles[i]=
...
newparticles[k]=

*/

#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

const int s = 5; //cellsize
const int numrnd = 40;
const int k = 500; //number of particles
const float sigma_x = 1;
const float sigma_z = 5;
const Point2f u_cur = Point2f(0, 0);
const CvScalar black = CV_RGB(0, 0, 0);
const CvScalar red = CV_RGB(255, 0, 0);
const string filename = "map.jpg";
string outfile = "out.txt";
const int randrep = 3;

void initialize_grid(string filename, Mat& grid){
  Mat img = imread(filename);
  grid = Mat::zeros(img.rows*s, img.cols*s, CV_8UC3);
  
  //find better grid scaling technique! disgusting quadruply-nested loop
  //is disgusting!
  for(int j=0; j<img.cols; j++){
    for(int i=0; i<img.rows; i++){
      Vec3b cur = img.at<Vec3b>(j, i);
      for(int l=s*j; l<s*(j+1); l++){
        for(int m=s*i; m<s*(i+1); m++){
          grid.at<Vec3b>(l, m) = cur;
        }
      }
    }
  }

}

vector<float> normrand(const vector<float>& mu, const float sigma){
  vector<float> v;
  int N = mu.size();

  RNG rand;
  for(int i = 0; i<N; i++){
    v.push_back( rand.gaussian(sigma) + mu[i]);
    //printf("v at %d = %f\n", i, v[i]);
  }
  return v;
}

Point2f normrand(Point2f mu, const float sigma){
  /*vector<float> mu2;
  mu2.push_back(mu.x);
  mu2.push_back(mu.y);
  vector<float> v = normrand(mu2, sigma);
  mu = Point2f(v[0], v[1]);
  v.clear();
  mu2.clear();*/
  RNG rand;
  float x, y;
  for(int i=0; i<randrep; i++){
    x = rand.gaussian(sigma)+mu.x;
  }
  for(int i=0; i<randrep; i++){
    y = rand.gaussian(sigma)+mu.y;
  }
  Point2f ans(x, y);
  return ans;
}

vector<float> normrand(const Vec3b mu, const float sigma){
  vector<float> mu2;
  for(unsigned int i = 0; i < 3; i++){
    mu2.push_back( (float) mu[i]);
  }
  vector<float> v = normrand(mu2, sigma);
  return v;
}

/*I pulled the return line directly from the Matlab source code for normpdf*/
float normpdf(const float x, const float mu, const float sigma){
  return exp(-0.5* pow((x - mu)/sigma, 2)) / (sqrt(2*M_PI)*sigma);
}

/*To be replaced by actual measurements*/
vector<float> sample_measurement(const Point2f& x, const Mat& grid){
  /*int i = floor(x.x);
  int j = floor(x.y);*/
  //printf("x,y=%f, %f\n", x.x, x.y);
  Vec3b v = grid.at<Vec3b>((int) x.y, (int) x.x);
  vector<float> z = normrand(v, sigma_z);
  return z;
}

float measurement(const vector<float> z, const Point2f& x, const Mat& grid){
 /* int i = floor(x.x);
  int j = floor(x.y);*/
  Vec3b v = grid.at<Vec3b>((int) x.y, (int) x.x);
  float p_zi;

  float p_z = 1;
  int N = z.size();
  for(int i=0; i<N; i++){
    //printf("zi=%f, vi=%d\n", z[i], v[i]);
    p_zi = normpdf(z[i], (float) v[i], sigma_z);
    //printf("p_zi=%f\n", p_zi);
    p_z *= p_zi;
  }
  //printf("p_z=%f\n", p_z);
  return p_z;
}

/*To be replaced by actual odometry*/
Point2f sample_motion(const Point2f x_prev, const Point2f u_cur,
                      const float sigma_x, const int grid_x, const int grid_y){
  /*Border constraints*/
  Point2f x_cur = normrand(x_prev + u_cur, sigma_x);

  if(x_cur.x<0){
    x_cur.x = grid_x-1;
  }
  if(x_cur.y<0){
    x_cur.y = grid_y-1;
  }
  if(x_cur.x>=grid_x || isnan(x_cur.x)){
    x_cur.x = 0;
  }
  if(x_cur.y>=grid_y || isnan(x_cur.y)){
    x_cur.y = 0;
  }
  return x_cur;
}

int sample_weighted(const vector<float> p_x){
  int N = p_x.size();
  float r = (float) rand() / (float) RAND_MAX;
  float sum = 0;
  for(int i=0; i<N; i++){
    sum+=p_x[i];
    if(sum>r){
      return i;
    }
  }
  return N-1;
}

void draw(Mat display, const Point2f x_sim, const vector<Point2f> particles,
          const int grid_x, const int grid_y){
  //Mat display = Mat::zeros(grid_y, grid_x, CV_8UC3);
  //printf("Grid x=%d, grid y = %d\n", grid_x, grid_y);
  int N = particles.size();
  float x, y;
  for(int i=0; i<N; i++){
    x = particles[i].x;
    y = particles[i].y;
    if(y<grid_y && x < grid_x && x >= 0 && y >= 0){
      display.at<Vec3b>( (int) y, (int) x ) = Vec3b(255, 255, 255);
    } else {
      throw runtime_error("attempting to access out-of-bounds matrix entry");
    }
  }
  circle(display, x_sim, 2, red, 1);
  imshow("Display", display);
  waitKey(500);
}

void simulate(const Mat& grid){
  //fstream out(outfile, fstream::out);
  int grid_x = grid.cols;
  int grid_y = grid.rows;
  int j=0;
  bool do_measurement=false;
  Mat display(grid_y, grid_x, CV_8UC3);
  Point2f x_sim(50, 50); //Initialize position
  vector<float> z_sim;
  vector<float> weights;
  vector<Point2f> particles;
  vector<Point2f> newparticles;

  for(int i=0; i<k; i++){
    particles.push_back(sample_motion(x_sim, Point2f(0,0), sigma_x*100,
                                      grid_x, grid_y));
    newparticles.push_back(Point2f(0,0));
    weights.push_back(0);
  }
  display=black;
  draw(display, x_sim, particles, grid_x, grid_y);
  int randno = rand() % k;

  //Main loop
  while(true){
    printf("before sample: particles[%d]=%f, %f\n", randno,
            particles[randno].x, particles[randno].y);
    for(int i=0; i<k; i++){
      particles[i] = sample_motion(particles[i], u_cur, sigma_x,
                                    grid_x, grid_y);
    }
    printf("after sample: particles[%d]=%f, %f\n", randno,
            particles[randno].x, particles[randno].y);
    x_sim = sample_motion(x_sim, u_cur, sigma_x, grid_x, grid_y);
    //printf("x_sim= %f, %f\n", x_sim.x, x_sim.y);

    if(do_measurement){
      z_sim = sample_measurement(x_sim, grid);
      float sum=0;
      for(int i=0; i<k; i++){
        weights[i] = measurement(z_sim, particles[i], grid);
        sum+=weights[i];
      }
      for(int i=0; i<k; i++){
        weights[i] /= sum;
        //printf("weights at %d: %f\n", i, weights[i]);
      }
      //clear new vectors
      for(int i=0; i<numrnd; i++){
        newparticles[i] = Point2f(rand()%grid_x, rand()%grid_y);
      }
      for(int i=numrnd; i<k; i++){
        //printf("i=%d\n",i);
        j = sample_weighted(weights);
        //printf("j=%d\n", j);
        newparticles[i] = particles[j];
      }
      particles = newparticles;
    }
    //plot some stuff
    display=black;
    draw(display, x_sim, particles, grid_x, grid_y);
  }
}

int main(int argc, char** argv){
  namedWindow("Display");
/*  string filename;
  int startx;
  int starty;

  cout<<"Enter name of map file."<<endl;
  cin>>filename;
  
  cout<<"Enter initial position (x, enter, y)"<<endl;
  cin>>startx;
  cin>>starty;*/
  Mat grid;
  initialize_grid(filename, grid);

  imshow("Display", grid);
  waitKey();
  simulate(grid);
  
}
