#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/Odometry.h>
#include <kdl/frames.hpp>
#include <opencv2/opencv.hpp>
#include <fstream>
#include <iostream>
#include <exception>
#include <stdexcept>
#include <unistd.h>
#include "transform2d.h"

using namespace std;
typedef geometry_msgs::PoseWithCovarianceStamped::ConstPtr Pose;

ros::Publisher pub;
static float tx, ty;
char* filename="data.csv";
vector<string> essid;
vector<int> levels;
fstream out;

void readScan(int fd){
  int level=0;
  unsigned int j=0;
  char line[100];
  char* tok;

  while(line!=NULL && read(fd, line, 100) > 0){
    //Read level
    tok = strtok(line, " =\"");
    while(tok!=NULL && strcmp(tok, "level")!=0){
      tok = strtok(NULL, " =\"");
    }
    tok = strtok(NULL, " =\"");
    if(tok!=NULL){
      level = abs( atoi(tok) );
    }
    //Read ESSID
    while(tok!=NULL && strcmp(tok, "ESSID:")!=0){
      tok = strtok(NULL, " =\"");
    }
    tok = strtok(NULL, " =\"");
    if(tok!=NULL){
      for(unsigned int i=0; i<essid.size(); i++){
        if(strcmp(essid[i].c_str(), tok)==0){
          if(levels.size() <= i){
            levels.resize(i+1);
          }
          levels[i] = level;
          printf("Added levels\n");
          j++;
          if(j==essid.size()) return;
        }
      }
    }
  }
}

void getScan(){
  pid_t nPid;
  int iopipe[2];
  if(pipe(iopipe) != 0 ){
    throw runtime_error("Pipe failed");
  }
  nPid = fork();
  if(nPid<0){
    throw runtime_error("Fork-exec failed");
  } else if(nPid==0){ //Child process
    close(iopipe[0]);
    dup2(iopipe[1], STDOUT_FILENO);
    execl("wifiscan", "wifiscan", NULL);
    throw runtime_error("Could not call wifiscan");
  } else {  //Parent process
    close(iopipe[1]);
    readScan(iopipe[0]);
  }

}

void getInfo(){
  getScan();
  string location;
  printf("Enter name for current location\n");
  cin>>location;
  for(unsigned int i=0; i<essid.size(); i++){
    out<<essid[i]<<","<<location<<","<<levels[i]<<endl;
  }
  cout<<"Added data"<<endl;
};


void getOdometry(const Pose& msg, Transform2D odom)
{
  float theta, x, y;
  x = msg->pose.pose.position.x;
  y = msg->pose.pose.position.y;

  double roll, pitch, yaw;
  KDL::Rotation::Quaternion(msg->pose.pose.orientation.x,
                       msg->pose.pose.orientation.y,
                       msg->pose.pose.orientation.z,
                       msg->pose.pose.orientation.w).GetRPY(roll, pitch, yaw);
  theta = yaw;
  odom.tx = x;
  odom.ty = y;
  odom.theta = theta;
  ROS_INFO("Odometry: x = %g, y = %g, theta = %g",
            odom.tx, odom.ty, odom.theta);
  tx = odom.tx;
  ty = odom.ty;
}

void callBack(const Pose& msg){
  Transform2D odom;
  getOdometry(msg, odom);
}

int main(int argc, char** argv){
  ros::init(argc, argv, "getwifi");
  
  if(argc>1){
    filename = argv[1];
  }

  ros::NodeHandle nh;
  ros::Subscriber sub = nh.subscribe("/robot_pose_ekf/odom_combined", 10,
                                      callBack);
  out.open(filename, ios::out);
  if(!out.is_open()){
    throw runtime_error("Couldn't open file for writing");
  }
  XmlRpc::XmlRpcValue essid_list;
  if(  nh.getParam("essidlist", essid_list) ) {
    ROS_ASSERT(essid_list.getType() == XmlRpc::XmlRpcValue::TypeArray);
    for(unsigned int i = 0; i<essid_list.size(); i++){
      ROS_ASSERT(essid_list[i].getType() == XmlRpc::XmlRpcValue::TypeString);
      essid.push_back(essid_list[i]);
    }
  } else {
    ROS_INFO("Couldn't load essid list! Check your rosparams.");
  }

 /* essid.push_back("robotlab");
  essid.push_back("SwatWireless");
  essid.push_back("Loomis");
  essid.push_back("World");
  essid.push_back("DigitalMedia");
  essid.push_back("IECA");*/

  cv::namedWindow("Window");
  cv::imshow("Window", cv::Mat(100, 100, CV_8U));
  
  char input;

  while(true){
    ros::spinOnce();
    input = cv::waitKey(5) & 0xff;
    if(input=='r'){
      getInfo();
    }
  }
}
