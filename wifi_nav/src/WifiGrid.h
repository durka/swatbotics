#ifndef __WIFIGRID_H__
#define __WIFIGRID_H__

#include "OcpGrid.h"

class WifiGrid : public OcpGrid{
  private:
    cv::Mat levels;
    void growLevelGrid(unsigned int dir);
  public:
    WifiGrid(float size=0.1, int intial_height=50, int initial_width=50);
    ~WifiGrid();
    void addLevel(float level);
    float levelAt(int x, int y);
    float levelAt(cv::Point2f pos);
};

#endif
