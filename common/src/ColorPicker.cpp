/*
Click on a pixel in the image and name the color to add it to the ColorLUT
Or click on a pixel and get the names of the colors it belongs to

Controls are:
Left click stores the color of the clicked pixel for the current color
Ctrl-left click clears the clicked pixel for the current color
Right click stores the clicked color as a new color
-/= cycles through colors
a/z increments Y radius
s/x increments Cr radius
d/c increments Cb radius
r gets input for radius
enter quits and saves mask/LUT data
escape quits without saving
m masks the image with the current color
i prompts user for name of an imagefile  to load
I prompts the user for a name to save current mask image
k prompts user for name of a datafile to load
K prompts user for a name to save current datafile
*/
#include "ColorPicker.h"
#include <cv_bridge/cv_bridge.h>
#include <opencv/highgui.h>
#include <cstring>
#include <sensor_msgs/image_encodings.h>

namespace enc = sensor_msgs::image_encodings;

void mouseCallBack(int event, int x, int y, int flags, void* param){
  string input;
  ColorPicker* c = (ColorPicker*) param;
  if( x>=0 && x<c->img.cols && y>=0 && y < c->img.rows){
    ColorLUT::pixel p = c->img.at<ColorLUT::pixel>(y,x);

    if(event==CV_EVENT_RBUTTONUP && flags&CV_EVENT_FLAG_RBUTTON){
      c->addNewColor(p);

    } else if (event==CV_EVENT_LBUTTONUP &&
               flags&(CV_EVENT_FLAG_CTRLKEY)){
      c->pushUndo();
      c->lookup().clearColor(p, c->curindex, c->radius);
      cout<<"Cleared data\n";

    } else if(event==CV_EVENT_LBUTTONUP && flags&CV_EVENT_FLAG_LBUTTON){
      if(c->curindex==ColorLUT::npos){
        printf("Current index: %d\n", (int)c->curindex);
        c->addNewColor(p);
      } else{
	      c->pushUndo();
        c->lookup().setColor(p, c->curindex, c->radius);
        cout<<"Added data\n";

      }
    }
  }
}

ColorPicker::ColorPicker(string lutfile, string imgfile, bool vid){

  lookups.push_back(ColorLUT());

  for(int i = 0; i < 3; i++){
    radius[i] = 0;
  }
  curindex = -1;
  loadLUT(lutfile);
  if(!vid){
    setImageFile(imgfile);
  }
  setMouseCallback("Image Window", mouseCallBack, (void*) this);
  setMouseCallback("Mask Window", mouseCallBack, (void*) this);
  elemradius = 3;
  element=cv::getStructuringElement(cv::MORPH_ELLIPSE,
                                    cv::Size(elemradius,elemradius));
}

ColorPicker::~ColorPicker(){
}

void ColorPicker::addNewColor(ColorLUT::pixel p){
  string input;
  cout<<"Enter a name for the new color: ";
  cin>>input;
  pushUndo();
  size_t s = lookup().lookupColor(input);
  if(s==size_t(-1)){
    s = lookup().addColor(input);
  }
  lookup().setColor(p, s, radius);
  curindex = s;
  printf("Current index: %d\n", (int)curindex);
  cout<<"Created color "<<input<<endl;
}

void ColorPicker::loadLUT(string lutfile){
  if(lutfile.compare(" ")==0){
    /*cout<<"Enter filename of data file to load: ";
    cin>>lutfile;*/
    return;
  }
  lookup().load(lutfile);
  clearUndo();
}
void ColorPicker::saveLUT(string lutfile){
  if(lutfile.compare(" ")==0){
    cout<<"Enter name for saved data:";
    cin>>lutfile;
  }
  lookup().save(lutfile);
}

void ColorPicker::setImageFile(string imgfile){
  if(imgfile.compare(" ")==0){
    cout<<"Enter filename of img file to load: ";
    cin>>imgfile;
  }
  img = imread(imgfile);
  imshow("Image Window", img);
  cvtColor(img, img, CV_RGB2YCrCb);
}

void ColorPicker::saveImage(string imgfile){
  if(imgfile.compare(" ")==0){
    cout<<"Enter name for saved mask image: ";
    cin>>imgfile;
  }
  imwrite(imgfile, img);
}


void ColorPicker::cycleColor(uchar c){
  if(c=='+'){
    if(curindex<lookup().colornames.size()-1){
      curindex++;
    } else { curindex = 0; }
  } else if (c=='-'){
    if(curindex>0){
      curindex--;
    } else if(curindex!=ColorLUT::npos) { curindex = lookup().colornames.size()-1; }
  }
  cout<<"Changed current color to "<<lookup().colornames[curindex]<<endl;
}

void ColorPicker::setRadius(uchar c){
  if(c=='a'){
    radius[2]++;
  } else if(c=='z'){
    if(radius[2]>0){
      radius[2]--;
    }
  } else if(c=='s'){
    ++radius[1];
  } else if(c=='x'){
    if(radius[1]>0){
      --radius[1];
    }
  } else if(c=='d'){
    ++radius[0];
  } else if(c=='c'){
    if(radius[0]>0){
      --radius[0];
    }
  } else if(c=='r'){
    cout<<"Enter a new value for the Y radius: ";
    cin>>radius[2];
    cout<<"Enter a new value for the Cr radius: ";
    cin>>radius[1];
    cout<<"Enter a new value for the Cb radius: ";
    cin>>radius[0];
  }
  printf("Changed radii to (%d, %d, %d)\n", radius[2], radius[1], radius[0]);
}

void ColorPicker::setMask(){
  Mat element=cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(7,7));
  mask = CV_RGB(0,0,0);
  lookup().getImageColor(img, curindex, mask);
  if(toggle_mask){
    cv::morphologyEx(mask, mask, cv::MORPH_OPEN, element,
		     cv::Point(-1,-1), 1, cv::BORDER_REPLICATE);
  }
  imshow("Mask Window", mask);
}

ColorLUT& ColorPicker::lookup() {
  return lookups.back();
}

void ColorPicker::undo() {
  if (lookups.size() > 1) {
    printf("UNDO!\n");
    lookups.pop_back();
  } else {
    printf("CAN'T UNDO\n");
  }
}

void ColorPicker::clearUndo() {
  lookups.resize(1);
}

void ColorPicker::pushUndo() {
  lookups.push_back(lookups.back());
  if (lookups.size() > MAX_UNDO) {
    lookups.pop_front();
  }
}

void ColorPicker::setElemRadius(uchar c){
  if(c=='f'){
    elemradius++;
  } else if(c=='v'){
    elemradius--;
  }
  element=cv::getStructuringElement(cv::MORPH_ELLIPSE,
                                    cv::Size(elemradius,elemradius));
}
