#include "ColorPicker.cpp"
#include "ImageConverter.cpp"

#include <deque>

ColorPicker* c;

//std::deque<ColorPicker*> cstack;

void introText(){
  cout<<"Welcome to ColorPicker.\n"
      <<"Right click to select a new color.\n"
      <<"+/- cycle through colors.\n"
      <<"R deletes the current color.\n"
      <<"a/z increments the Y radius. \n" 
      <<"s/x increments the Cr radius. \n" 
      <<"d/c increments the Cb radius. \n"
      <<"u undo\n"
      <<"m creates a mask of the image with the current color.\n"
      <<"t toggles morphological operators in the mask image.\n"
      <<"i loads an image file; I saves the current mask image.\n" 
      <<"k loads a ColorLUT data file; K saves the current data file.\n"
      <<"ENTER quits and saves the data, ESC quits without saving.\n"; 
}

void keyInput(char k, ColorPicker* c){
    if(k=='+' || k=='-'){
      c->cycleColor(k);
    } else if(k=='a' || k=='z' || k=='s' || k=='x' || k=='d' || k=='c'  ||
              k == 'r'){
      c->setRadius(k);
    } else if (k=='m'){
      c->setMask();
    } else if (k=='t'){
      if(c->toggle_mask){
        c->toggle_mask = false;
      } else { c->toggle_mask = true; }
    } else if (k=='i'){
      c->setImageFile(" ");
    } else if (k=='I'){
      c->saveImage(" ");
    } else if (k=='k'){
      c->loadLUT(" ");
    } else if (k=='K'){
      c->saveLUT(" ");
    } else if (k=='R'){
      c->lookup().removeColor(c->curindex);
    } else if (k == 'u') {
      c->undo();
    } else if (k == 'f' ||  k== 'v') {
      c->setElemRadius(k);
    } else if (k == '?') {
      introText();
    }

}

void callBack(sensor_msgs::Image msg){
  cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(msg, enc::BGR8);
  c->img = cv_ptr->image;
  imshow("Image Window", c->img);
  c->setMask();
  imshow("Mask Window", c->mask);
}

int main(int argc, char** argv){
  ros::init(argc, argv, "colorpicker");
  ros::NodeHandle nh;
  ros::Subscriber sub;
  string lutfile = " ";
  string imgfile = " ";
  char k;

  bool vid = false;
  if(argc>1){
    imgfile = argv[1];
    if(argc>2){
      lutfile = argv[2];
    }
  }
  
  namedWindow("Image Window");
  namedWindow("Mask Window");

  introText();
  try{
    c = new ColorPicker(lutfile, imgfile);
  } catch(Exception){
    cout<<"Caught exception: not an image file! Using image as a rostopic."<<endl;
    vid = true;
    c = new ColorPicker(lutfile, imgfile, vid);
  }
  if(vid){
    sub = nh.subscribe(imgfile, 1, callBack);
    while(true){
      ros::spinOnce();
      k = waitKey(30) & 0xff;
      keyInput(k, c);
      if (k=='\n'){
       c->saveLUT(" ");
        break;
      } else if (k==27){
        break;
      }
    }
  } else {
    while(true){
      k = waitKey() & 0xff;
      keyInput(k, c);
      if (k=='\n'){
	c->saveLUT(" ");
	break;
      } else if (k==27){
	break;
      }
    }
  }
  delete c;
  return 0;
  

}
