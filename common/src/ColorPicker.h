#ifndef __COLORPICKER_H__
#define __COLORPICKER_H__

#include "ColorLUT.h"
#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <sensor_msgs/Image.h>
#include <deque>

using namespace cv;
using namespace std;

typedef unsigned char uchar;

class ColorPicker{

public:
  size_t curindex;
  int radius[3];
  int elemradius;
  bool toggle_mask;
  Mat img, mask, element;

  enum {
    MAX_UNDO = 8
  };


  deque<ColorLUT> lookups;

  ColorPicker(string lutfile, string imgfile, bool vid=false);
  ~ColorPicker();

  ColorLUT& lookup();

  void loadLUT(string lutfile);
  void saveLUT(string lutfile);
  void setImageFile(string imgfile);
  void saveImage(string imgfile);
  void addNewColor(ColorLUT::pixel p);
  void cycleColor(uchar c);
  void setRadius(uchar c);
  void setMask();
  void setImage(Mat newimg);
  void undo();
  void pushUndo();
  void clearUndo();
  void setElemRadius(uchar c);
  
};

#endif
