#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/opencv.hpp>

class ImageConverter{
public:
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  ColorPicker* c;

ImageConverter(ColorPicker* color) : it_(nh_)
  {
    c = color;
    image_sub_ = it_.subscribe("/camera/rgb/image_color", 1, &ImageConverter::imageConvert, this);
  }

  ~ImageConverter()
  {
  }

  void imageConvert(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, enc::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
    c->img = CV_RGB(0,0,0);
    c->img = cv_ptr->image;
    imshow("Image Window", c->img);
  }

};
