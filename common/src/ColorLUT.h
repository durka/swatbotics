#ifndef __COLORLUT_H__
#define __COLORLUT_H__

#include <opencv2/opencv.hpp>
#include <vector>
#include <string>

class ColorLUT {
public:

  enum {
    ybits = 4,
    cbits = 6,
    numcolors = 8,
    npos = size_t(-1)
  };
  
  typedef unsigned char colorflags;
  typedef cv::Vec3b pixel;

  std::vector<colorflags> lutdata;

  std::vector<std::string> colornames;

  ColorLUT();

  // converts a pixel to an index

  // pixel should be a pixel in YCrCb space
  // cidx should be from 0 to numcolors-1
  // radius specifies how many neighbors of pixel we will set in each dir
  //
  void setColor(const pixel& YCrCb, size_t cidx,
                int radius[]=NULL);

  // same as above
  void clearColor(const pixel& YCrCb, size_t cidx,
                  int radius[]=NULL);

  colorflags getColors(const pixel& YCrCb) const;
  
  size_t addColor(const std::string& name);

  string removeColor(int index); 

  // this will return size_t(-1) if not found
  size_t lookupColor(const std::string& name) const;

  // saves this entire LUT to a file
  void save(const std::string& filename) const;

  // load this entire LUT from a file
  void load(const std::string& filename);
	
  // take a YCrCb image and replace each pixel with a colorflags entry.
  // the colorflags matrix will have same size as source, but different type
  void getImageColors(const cv::Mat& image, 
		      cv::Mat& colorflags);

  // take a YCrCb image and set the destination 8-bit mask image to 255
  // wherever the source image is of the color with the given cidx
  void getImageColor(const cv::Mat& image, 
		     size_t cidx, 
		     cv::Mat& mask);

  void colorFlagsToMask(const cv::Mat& colorflags, 
			size_t cidx,
			cv::Mat& mask);

};

#endif

