#!/usr/bin/env python

import serial
import sys

if __name__ == '__main__':
	# bytes.py [device file] [baud rate]
	argc = len(sys.argv)
	dev = (argc > 1) and sys.argv[1] or '/dev/ttyUSB0'
	baud = (argc > 2) and int(sys.argv[2]) or 57600
	ser = serial.Serial(dev, baud, timeout=1)

	while True:
		print 'byte>',
		a = raw_input()

		if a == 'quit':
			break

		if a != '':
			for i in a.split(' '):
				ser.write(chr(int(i)))

		print repr(ser.read(100))
	
	ser.close()

