#!/usr/bin/env python

from __future__ import division
import serial
import sys
from time import sleep

if __name__ == '__main__':
	# bytes.py [device file] [baud rate]
	ser = serial.Serial("/dev/ttyUSB0", 57600, timeout=1)

	# Start Charging
	ser.write(chr(128))
	ser.write(chr(132))
	ser.write(chr(147))
	ser.write(chr(0))
	sleep(3)
	ser.write(chr(147))
	ser.write(chr(1))
	ser.write(chr(128))
	
	# Initialize the charge mode reader
	a = chr(2)

	while (a!=chr(4)):
		ser.write(chr(142))
		ser.write(chr(21))
		a = ser.read(100)
		ser.write(chr(142))
		ser.write(chr(22))
		b = ser.read(100)
		print repr(a),
		if len(b) >= 2:
			print (ord(b[0])*16*16+ord(b[1]))/1000
		else:
			print
	
	# Charge complete, turn off create and relay
	ser.write(chr(128))
	ser.write(chr(132))
	ser.write(chr(147))
	ser.write(chr(2))


	ser.close()

