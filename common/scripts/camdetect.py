#!/usr/bin/env python

import xml.dom.minidom as x
import subprocess
import glob
import sys

RULES = ( ('lifecam',  'Zoom, Absolute'),
          ('portable', 'Focus'),
          ('logitech', 'Exposure, Auto Priority'),
          ('builtin', 'Gamma') )

def run(*args):
    return subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0]

# ./camdetect [launch file]
if __name__ == '__main__':
    if (len(sys.argv) == 2):
        launch = x.parse(sys.argv[1])
    else:
        launch = False
    cams = glob.glob('/dev/video*')

    print 'Detected cameras: ' + ', '.join(map(lambda c: c.split('/')[2], cams))

    for cam in cams:
        params = map(lambda s: s.strip(), run('uvcdynctrl', '-d', cam, '-c').split('\n')[1:-1])
        for rule in RULES:
            if rule[1] in params:
                print 'Found %s at %s' % (rule[0], cam)
                if launch:
                    for include in launch.getElementsByTagName('include'):
                        if include.getAttribute('file') == '$(find turtle_cli)/cam.launch':
                            for arg in include.getElementsByTagName('arg'):
                                if arg.getAttribute('name') == 'cam' and arg.getAttribute('value') == rule[0]:
                                    for arg in include.getElementsByTagName('arg'):
                                        if arg.getAttribute('name') == 'device':
                                            arg.setAttribute('value', cam)
                                            print '\tUpdated XML'
                break
    
    if launch:
        launch.writexml(open(sys.argv[1], 'w'))
