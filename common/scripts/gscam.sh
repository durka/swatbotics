#!/bin/bash

# scripts/gscam.sh [sleep time] [device] [white balance] [exposure] [name]

sleep $1

if command -v uvcdynctrl &>/dev/null; then
	echo "Turning off AWB"
	uvcdynctrl -d $2 -s "White Balance Temperature, Auto" 0 # no auto white balance
	if uvcdynctrl -d $2 -c | grep -q "Focus, Auto"; then
		echo "Turning off AF"
		uvcdynctrl -d $2 -s "Focus, Auto" 0 # no auto focus
	fi
	if uvcdynctrl -d $2 -c | grep -q "Exposure, Auto Priority"; then
		echo "Turning off AE (priority)"
		uvcdynctrl -d $2 -s "Exposure, Auto Priority" 0 # no auto exposure
	fi
	echo "Turning off AE"
	uvcdynctrl -d $2 -s "Exposure, Auto" 1 # really, manual mode for exposure
	if [ $4 != "no" ]; then
		echo "Setting exposure to " $4
		uvcdynctrl -d $2 -s "Exposure (Absolute)" $4
	fi
	if [ $3 != "no" ]; then
		echo "Setting white balance to " $3
		uvcdynctrl -d $2 -s "White Balance Temperature" $3
	fi
fi

#echo "Loading camera parameters"
#rosservice call /$5/set_camera_info <<<`rospack find common`/data/$5.yaml

