#!/usr/bin/env python

# run this script to find out what value waitKey will return for a specific key press
# run the script, select the window, press the key

import cv

if __name__ == '__main__':
	cv.NamedWindow('SELECT ME')
	dummy = cv.CreateMat(400, 400, cv.CV_8UC1)
	cv.SetZero(dummy)
	cv.ShowImage('SELECT ME', dummy)
	print cv.WaitKey()

