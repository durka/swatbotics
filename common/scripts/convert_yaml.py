#!/usr/bin/env python

import yaml
import sys

TRANSLATE = ( # srv          yaml
             ('height', 'image_height'),
             ('width',  'image_width'),
             ('distortion_model', 'distortion_model')
            )

MATRICES =  ( # srv          yaml                   dims
             ('D',      'distortion_coefficients', (1, 5)),
             ('K',      'camera_matrix',           (3, 3)),
             ('R',      'rectification_matrix',    (3, 3)),
             ('P',      'projection_matrix',       (3, 4)),
            )

def to_srv(y):
    s = {}

    # translation step
    for sk, yk in TRANSLATE:
        try:
            s[sk] = y[yk]
        except KeyError:
            print 'Error! Key %s not found in YAML. Rolling back' % yk
            return y

    # convert matrices
    for sm, ym, d in MATRICES:
        try:
            s[sm] = y[ym]['data']
        except KeyError:
            print 'Error! Could convert matrix %s. Rolling back' % ym
            return y

    return s

def from_srv(s):
    y = {}

    # translation step
    for sk, yk in TRANSLATE:
        try:
            y[yk] = s[sk]
        except KeyError:
            print 'Error! Key %s not found in srv. Rolling back' % sk
            return s

    # convert matrices
    for sm, ym, (r, c) in MATRICES:
        try:
            y[ym] = {'rows': r, 'cols': c, 'data': s[sm]}
        except KeyError:
            print 'Error! Could not convert matrix %s. Rolling back' % sm

    return y

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print 'Usage: %s {input YAML}' % sys.argv[0]
        sys.exit(1)

    f = file(sys.argv[1], 'r')
    y =  yaml.load(f)
    f.close()

    if 'image_height' in y:
        print 'Converting to srv format'
        f = file(sys.argv[1], 'w')
        f.write(yaml.dump(to_srv(y)))
        f.close()
    else:
        print 'Converting from srv format'
        f = file(sys.argv[1], 'w')
        f.write(yaml.dump(from_srv(y)))
        f.close()

