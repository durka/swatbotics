#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>
#include <ctime>
#include "sensor_msgs/Image.h"
#include "sensor_msgs/PointCloud2.h"

using namespace ros;
using namespace rosbag;
using namespace cv;
using namespace std;
namespace enc = sensor_msgs::image_encodings;
using sensor_msgs::Image;
using sensor_msgs::PointCloud2;
typedef pcl::PointCloud<pcl::PointXYZ> PointCLoud;

vector<string> keys(map<string,string> m)
{
    vector<string> k;

    for (map<string,string>::iterator i = m.begin(); i != m.end(); ++i)
    {
        k.push_back(i->first);
    }

    return k;
}
vector<string> values(map<string,string> m)
{
    vector<string> v;

    for (map<string,string>::iterator i = m.begin(); i != m.end(); ++i)
    {
        v.push_back(i->second);
    }

    return v;
}

Mat pcl2img(PointCLoud::ConstPtr ppcl)
{
    Mat m(ppcl->height, ppcl->width, CV_8UC3);

    return m;
}

int main(int argc, char *argv[])
{
    init(argc, argv, "inthebag");

    NodeHandle nh;

    if (argc < 2)
    {
        cerr << "Usage: " << argv[0] << " {bag filename}\n";
        exit(1);
    }

    // pre-processing to get bag info
    Bag bag(argv[1], bagmode::Read);
    View everything;
    everything.addQuery(bag);

    Time start  = everything.getBeginTime(),
         end    = everything.getEndTime();

    Duration length = end - start,
             step = length * 0.01;

    cout << "Bag duration: " << length << " seconds"
         << " (steps of " << step.toSec() << " seconds)" << endl;

    cout << endl;
    map<string, string> topics;
    BOOST_FOREACH(const ConnectionInfo* c, everything.getConnections())
    {
        cout << c->topic << " [" << c->datatype << "]" << endl;
        topics[c->topic] = c->datatype;
    }

    cout << endl;

    // user interface
    bool quit = false;
    while (!quit)
    {
        int per1, per2;
        cout << "Choose begin and end slices (1-100): ";
        cin >> per1 >> per2;

        Time slicestart = start + step*(per1-1),
             sliceend   = start + step*per2;
        if (per2 == 100) sliceend = end;
        View slice(bag, TopicQuery(keys(topics)), slicestart, sliceend);

        int choice;
        cout << "What to do? (1=play, 2=write, 3=choose again) ";
        cin >> choice;
        switch (choice)
        {
            case 1:
                {
                    int topic = 0;
                    cout << "Choose topic to visualize:" << endl;
                    for (map<string,string>::iterator i = topics.begin();
                         i != topics.end();
                         ++i, ++topic)
                    {
                        if (i->second == "sensor_msgs/PointCloud2" || i->second == "sensor_msgs/Image")
                        {
                            cout << "\t" << topic << " " << i->first << endl;
                        }
                    }
                    cin >> topic;

                    vector<string> k = keys(topics), v = values(topics);
                    namedWindow("In The Bag", 0);
                    BOOST_FOREACH(MessageInstance const m, slice)
                    {
                        cout << m.getTopic() << k[topic] << endl;
                        if (m.getTopic() == k[topic])
                        {
                            // maybe it's an Image
                            Image::ConstPtr pimg = m.instantiate<Image>();
                            if (pimg != NULL)
                            {
                                imshow("In The Bag", cv_bridge::toCvCopy(pimg, pimg->encoding)->image);
                                waitKey(10);
                            }

                            // maybe it's a PointCloud
                            PointCLoud::ConstPtr ppcl = m.instantiate<PointCLoud>();
                            if (ppcl != NULL)
                            {
                                imshow("In The Bag", pcl2img(ppcl));
                                waitKey(10);
                            }
                        }
                    }
                }
                break;
            case 2:
                {
                    string filename;
                    cout << "Enter filename: ";
                    cin >> filename;

                    Bag slicebag(filename, bagmode::Write);
                    BOOST_FOREACH(MessageInstance const m, slice)
                    {
                        slicebag.write(m.getTopic(), m.getTime(), m);
                    }
                    slicebag.close();
                }
                break;
            case 3:
                break;
            default:
                quit = true;
                break;
        }
    }

    bag.close();

    return 0;
}

