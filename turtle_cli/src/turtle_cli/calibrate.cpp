#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "sensor_msgs/Image.h"
#include "std_msgs/String.h"
#include "kdl/frames.hpp"
#include "turtlebot_swarthmore/path.h"
#include "cv_bridge/cv_bridge.h"
#include "opencv2/opencv.hpp"
#include <iostream>
#include <sstream>
#include <string>
#include <queue>

using namespace ros;
using namespace std;
using namespace cv_bridge;
using geometry_msgs::Twist;
using geometry_msgs::PoseWithCovarianceStamped;
using sensor_msgs::Image;
using std_msgs::String;
using KDL::Rotation;

struct Pose
{
	double x;
	double y;
	double yaw;
} g_pose, g_offset = {0, 0, 0};

typedef enum { DONTCARE, ABOVE, BELOW } Goal;

struct Target
{
	double x, y, yaw;
	Goal xd, yd, yawd;
} g_target;

NodeHandle *g_nh;
Publisher g_vel_pub, g_cmd_pub;
Subscriber g_img_sub;
Twist g_vel;
queue<string> g_commands;
string g_filename;
TurtlePath g_path;
bool g_stopped = false, g_following = false;

#define PI 3.1415926535

double angle_add(double a, double b)
{
	double result = a + b;
	
	if (result > PI)
    {
    	result -= 2*PI;
    }
    else if (result < -PI)
    {
    	result += 2*PI;
    }

    return result;
}

Point rotate(float theta, Point p)
{
	float c = cos(theta), s = sin(theta);

	return Point(c*p.x - s*p.y, s*p.x + c*p.y);
}

Point to_robot_frame(Point p, Pose pose)
{
	return rotate(-pose.yaw, p - Point(pose.x, pose.y));
}

// distance between two points
float dist(Point p1, Point p2, bool squared=false)
{
	float dsqr = pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2);

	return squared ? dsqr : sqrt(dsqr);
}

// distance from point to a line
float dist(Point q, Point p1, Point p2, Point *out, bool squared=false)
{
	Point a ( p2.x - p1.x, p2.y - p1.y );
	Point b ( q.x - p1.x, q.y - p1.y );

	float u = (a.x*b.x + a.y*b.y) / (a.x*a.x + a.y*a.y);

	out->x = p1.x + u*a.x;
	out->y = p1.y + u*a.y;

	if (u > 0 && u < 1)
	{
		return dist(q, *out, squared);
	}
	else
	{
		return INFINITY;
	}
}

Point closest_on_path(TurtlePath path, Point q, int *before=NULL)
{
	float mindist = INFINITY;
	Point minpt;
	int mini = 0;

	float d;
	Point temppt;
	unsigned int i;
	for (i = 0; i < path.size(); ++i)
	{
		// check p_i
		d = dist(q, path[i]);
		if (d < mindist)
		{
			mindist = d;
			minpt = path[i];
			mini = i;
		}

		// check segment p_i-p_i+1
		if (i+1 < path.size())
		{
			d = dist(q, path[i], path[i+1], &temppt);
			if (d != INFINITY && d < mindist)
			{
				mindist = d;
				minpt = temppt;
				mini = i;
			}
		}
	}

	if (before) *before = mini;
	return minpt;
}

Point add_on_path(TurtlePath path, Point q, unsigned int before, float d)
{
	// wrap around at end of path?
	if (before+1 == path.size())
	{
		return add_on_path(path, path[0], 0, d - dist(q, path[before]));
	}

	// add along segment
	Point
		pi = path[before],
		pj = path[before+1],
		a = norm(pj - pi),
		end = q + a*d;
	
	// fell off end of segment?
	if (dist(pi, end) > dist(pi, pj))
	{
		return add_on_path(path, pj, before+1, d - dist(q, pj));
	}

	return end;
}

void reset_target()
{
	g_target.xd = g_target.yd = g_target.yawd = DONTCARE;
}

void set_target(string which, double x, double y, double yaw)
{
	reset_target();

	if (which.find('x') != string::npos)
    {
    	g_target.x = x;
    	g_target.xd = (g_pose.x > g_target.x) ? BELOW : ABOVE;
	ROS_INFO("x target is %s %g\n", ((g_target.xd == BELOW) ? "below " : "above "), g_target.x);
    }
    if (which.find('y') != string::npos)
    {
    	g_target.y = y;
    	g_target.yd = (g_pose.y > g_target.y) ? BELOW : ABOVE;
	ROS_INFO("y target is %s %g\n", ((g_target.yd == BELOW) ? "below " : "above "), g_target.y);
    }
    if (which.find('a') != string::npos)
    {
    	g_target.yaw = yaw;
    	g_target.yawd = (g_pose.yaw > g_target.yaw) ? BELOW : ABOVE;
	ROS_INFO("yaw target is %s %g\n", ((g_target.yawd == BELOW) ? "below " : "above "), g_target.yaw);
    }
}

bool met_target()
{
	if (g_following)
		return false;

	if (g_target.xd == DONTCARE && g_target.yd == DONTCARE && g_target.yawd == DONTCARE)
		return false;

	if (g_target.xd == ABOVE && g_pose.x <= g_target.x)
		return false;
	if (g_target.xd == BELOW && g_pose.x >= g_target.x)
		return false;
	
	if (g_target.yd == ABOVE && g_pose.y <= g_target.y)
		return false;
	if (g_target.yd == BELOW && g_pose.y >= g_target.y)
		return false;

	if (g_target.yawd == ABOVE && g_pose.yaw <= g_target.yaw)
		return false;
	if (g_target.yawd == BELOW && g_pose.yaw >= g_target.yaw)
		return false;
	
	return true;
}

void move(double linear, double angular)
{
	g_stopped = false;
	g_vel.linear.x = linear;
	g_vel.angular.z = angular;
}

void schedule_command(string input)
{
	g_commands.push(input);
}

void img_callback(const Image::ConstPtr &msg)
{
	CvImage::ConstPtr ptr = toCvShare(msg);

	char filename[100];
	sprintf(filename, "%s-x%.2fy%.2fa%.2f.png", g_filename.c_str(), g_pose.x, g_pose.y, g_pose.yaw);
	cv::imwrite(filename, ptr->image);
	ROS_INFO("Wrote image %s\n", filename);
	
	g_img_sub.shutdown();
	g_stopped = true;
}

void interpret_command(string input)
{
    stringstream stream(input);
    string command, param;
    double linear, angular, x, y, yaw;
    int n;

    stream >> command;
    if (command == "seq")
    {
    	while (getline(stream, command, ';'))
        {
        	schedule_command(command);
        }
        param = g_commands.front();
        g_commands.pop();
        interpret_command(param);
    }
    else if (command == "for")
    {
    	stream >> n;
    	getline(stream, param);
	ROS_INFO("%d times %s: ", n, param.c_str());
    	string cmd = "seq " + param;
    	for (int i = 1; i < n; ++i)
        {
        	cmd += ";" + param;
        }
	ROS_INFO("%s\n", cmd.c_str());
        interpret_command(cmd);
    }
    else if (command == "echo")
    {
    	getline(stream, param);
	ROS_INFO("%s\n", param.c_str());
    	if (!g_commands.empty())
        {
        	param = g_commands.front();
        	g_commands.pop();
        	interpret_command(param);
        }
    }
    else if (command == "stop")
    {
    	reset_target();
    	g_path.clear();
    	while (!g_commands.empty())
        {
        	g_commands.pop();
        }
        move(0, 0);
        g_following = false;
        g_stopped = true;
    }
    else if (command == "origin")
    {
    	g_offset.x += g_pose.x;
    	g_offset.y += g_pose.y;
    	g_offset.yaw = angle_add(g_offset.yaw, g_pose.yaw);
    	g_pose.x = g_pose.y = g_pose.yaw = 0;
    }
    else if (command == "move")
    {
        reset_target();
        stream >> linear >> angular;
        move(linear, angular);
        if (linear == 0 && angular == 0)
        {
        	g_stopped = true;
        }
    }
    else if (command == "until")
    {
        stream >> linear >> angular >> param;
        if (param.find('x') != string::npos)
        {
            stream >> x;
        }
        if (param.find('y') != string:: npos)
        {
            stream >> y;
        }
        if (param.find('a') != string::npos)
        {
            stream >> yaw;
        }
        set_target(param, x, y, yaw);
        move(linear, angular);
    }
    else if (command == "untilrel")
    {
        stream >> linear >> angular >> param;
        if (param.find('x') != string::npos)
        {
            stream >> x;
        }
        if (param.find('y') != string:: npos)
        {
            stream >> y;
        }
        if (param.find('a') != string::npos)
        {
            stream >> yaw;
        }
        set_target(param, g_pose.x + x, g_pose.y + y, angle_add(g_pose.yaw, yaw));
        move(linear, angular);
    }
    else if (command == "straight")
    {
    	stream >> linear >> x;
    	set_target("xy", g_pose.x + x*cos(g_pose.yaw), g_pose.y + x*sin(g_pose.yaw), 0);
    	move(linear, 0);
    }
    else if (command == "turn")
    {
    	stream >> angular >> yaw;
    	set_target("a", 0, 0, angle_add(g_pose.yaw, yaw));
    	move(0, angular);
    }
    else if (command == "pursue")
    {
    	move(0, 0);
    	g_following = false;
    	g_path.clear();
    	while (stream.good())
        {
        	stream >> x >> y;
        	g_path.push_back(Point(x, y));
        }
        g_following = true;
    }
    else if (command == "photo")
    {
    	stream >> g_filename;
	ROS_INFO("taking photo...\n");
    	g_stopped = false;
    	g_img_sub = g_nh->subscribe("image", 1, img_callback);
    }
}

void cmd_callback(const String::ConstPtr &msg)
{
	interpret_command(msg->data);
}


void odom_callback(const PoseWithCovarianceStamped::ConstPtr &msg)
{
	g_pose.x = msg->pose.pose.position.x - g_offset.x;
	g_pose.y = msg->pose.pose.position.y - g_offset.y;
	
	double roll, pitch; // ignored
	Rotation::Quaternion(msg->pose.pose.orientation.x,
                         msg->pose.pose.orientation.y,
                         msg->pose.pose.orientation.z,
	                     msg->pose.pose.orientation.w).GetRPY(roll, pitch, g_pose.yaw);
	g_pose.yaw = angle_add(g_pose.yaw, -g_offset.yaw);

	if (met_target() || g_stopped)
    {
    	if (!g_stopped && g_commands.empty())
        {
        	move(0, 0);
        	g_stopped = true;
        }
        else if (g_stopped && !g_commands.empty())
        {
        	string cmd = g_commands.front();
        	g_commands.pop();
        	interpret_command(cmd);
        }
    }

    if (g_following)
    {
    	const float alpha = 0.25, cx = 0.1, cth = 3;
    	int i;
    	Point p = closest_on_path(g_path, Point(g_pose.x, g_pose.y), &i);
    	Point g = add_on_path(g_path, p, i, alpha);

	ROS_INFO("(%.2f, %.2f) -> (%.2f, %.2f) -> %f\n", p.x, p.y, g.x, g.y, cth * to_robot_frame(g, g_pose).y);

    	move(cx, cth * to_robot_frame(g, g_pose).y);
    }

    String strmsg;
    char buf[100];
    sprintf(buf, "(%.2f, %.2f, %.2f)", g_pose.x, g_pose.y, g_pose.yaw*180/PI);
    strmsg.data = buf;
    g_cmd_pub.publish(strmsg);
}

int main(int argc, char *argv[])
{
	init(argc, argv, "calibrator");

	g_nh = new NodeHandle();
	Subscriber odom_sub = g_nh->subscribe("/robot_pose_ekf/odom_combined", 1000, odom_callback);
	Subscriber cmd_sub = g_nh->subscribe("calib_in", 1000, cmd_callback);
	g_vel_pub = g_nh->advertise<Twist>("cmd_vel", 1000);
	g_cmd_pub = g_nh->advertise<String>("calib_out", 1000);

    Rate loop_rate(100);
	g_vel.linear.x = g_vel.angular.z = 0;
	while (ok())
    {
    	g_vel_pub.publish(g_vel);

    	spinOnce();
    	loop_rate.sleep();
    }

    delete g_nh;
}

