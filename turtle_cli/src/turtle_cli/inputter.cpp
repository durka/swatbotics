#include "ros/ros.h"
#include "std_msgs/String.h"
#include <iostream>
#include <string>

using namespace ros;
using namespace std;
using std_msgs::String;

string g_str;

void callback(const String::ConstPtr &msg)
{
	g_str = msg->data;
}

int main(int argc, char *argv[])
{
	init(argc, argv, "inputter");

	NodeHandle nh;
	Subscriber sub = nh.subscribe("input", 1000, callback);
	Publisher pub = nh.advertise<String>("output", 1000);

    g_str = "";
    String msg;
	while (ok())
    {
    	spinOnce();
    	cout << g_str << "> ";
    	getline(cin, msg.data);
    	if (msg.data == "quit") break;
    	pub.publish(msg);
    	spinOnce();
    }

    return 0;
}

