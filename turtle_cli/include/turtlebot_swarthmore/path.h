#ifndef _TURTLESIM_PATH_H_
#define _TURTLESIM_PATH_H_

#include <vector>
#include <ostream>
#include <iomanip>

struct Point
{
	Point() : x(0), y(0) {}
	Point(float x_, float y_) : x(x_), y(y_) {}

	float x;
	float y;
};

Point operator+(Point a, Point b) { return Point(a.x + b.x, a.y + b.y); }
Point operator-(Point a, Point b) { return Point(a.x - b.x, a.y - b.y); }
Point operator*(Point a, float b) { return Point(a.x * b, a.y * b); }
Point operator/(Point a, float b) { return Point(a.x / b, a.y / b); }
Point norm(Point a) { return a / sqrt(a.x*a.x + a.y*a.y); }

std::ostream& operator<<(std::ostream& os, Point p) { os << "(" << std::setw(2) << p.x << ", " << std::setw(2) << p.y << ")"; return os; }

typedef std::vector<Point> TurtlePath;

#endif

