#include "EviGrid.h"

/*enum {
  UNKNOWN=0,
  OCCUPIED=127,
  EMPTY=255}; // For data in the grid*/

/*enum {
  NORTH=1,
  NORTHWEST=2,
  WEST=3,
  SOUTHWEST=4,
  SOUTH=5,
  SOUTHEAST=6,
  EAST=7,
  NORTHEAST=8}; // For direction of growth*/

EviGrid::EviGrid(float size, int initial_height, int initial_width){
  cellSize = size;
  grid = cv::Mat::zeros(initial_height, initial_width, CV_8UC1);
  grid = 127;
  //Set origin to be in the middle
  origin_x = initial_width/2;
  origin_y = initial_height/2;
  disp = false;
  displayMatrix = cv::Mat(640, 640, CV_8UC1);
}

EviGrid::EviGrid(EviGrid* old_Grid){
  origin_x = old_Grid->getOriginX();
  origin_y = old_Grid->getOriginY();
  cellSize = old_Grid->getCellSize();
  (old_Grid->getGrid()).copyTo(grid);
}

EviGrid::EviGrid(cv::Mat map, float size){
  grid = map;
  cellSize = size;
  disp = false;
  origin_x = grid.cols/2;
  origin_y = grid.rows/2;
} 

/*double grid size*/
void EviGrid::grow(){

  cv::Mat newgrid = cv::Mat::zeros(2*grid.rows, 2*grid.cols, CV_8UC1);
  newgrid = 127;
  ROS_INFO("Growing from %d", grid.rows); 
  int ystart = grid.rows/2;
  int xstart = grid.cols/2;

  for(int y=0; y<grid.rows; y++){
    for(int x=0; x<grid.cols; x++){
      newgrid.at<unsigned char>(y+ystart, x+xstart) =
          grid.at<unsigned char>(y, x);
    }
  }

  origin_x += xstart;
  origin_y += ystart;

  grid = newgrid;

}
/*void EviGrid::growSparse(unsigned int dir){
  if(dir > 8){
    cout<<"Incorrect direction at EviGrid::grow!"<<endl;
    return;
  }
  if(dir%2==0){
    grow(dir);
    return;
  }
  cv::Mat newgrid;
  if(dir==SOUTH || dir==NORTH){
    newgrid = cv::Mat(grid.rows*2, grid.cols, CV_8U);
  }
  if(dir==EAST || dir==WEST){
    newgrid = cv::Mat(grid.rows, grid.cols*2, CV_8U);
  }
}*/

int EviGrid::addInfo(sensor_msgs::LaserScan scan, geometry_msgs::PoseWithCovarianceStamped odom){
  return addInfo(scan, odom.pose.pose);
}

int EviGrid::addInfo(sensor_msgs::LaserScan scan, nav_msgs::Odometry odom){
  return addInfo(scan, odom.pose.pose);
}

int EviGrid::addInfo(sensor_msgs::LaserScan scan, geometry_msgs::Pose odom){
// Return value is the key input for waitKey

 // Convert odom and scan to points
    double curr_x = odom.position.x;
    double curr_y = odom.position.y;
    double roll, pitch, yaw;
    KDL::Rotation::Quaternion(odom.orientation.x,
                         odom.orientation.y,
                         odom.orientation.z,
                         odom.orientation.w).GetRPY(roll, pitch, yaw);
    double curr_theta = yaw;
    Transform2D transMatrix(1, curr_x, curr_y, curr_theta);
    vector<cv::Point2f> endPoints;
    cv::Point2f pt;
    for (unsigned int i=0; i<scan.ranges.size(); i++){
      if (scan.ranges[i]>scan.range_min && scan.ranges[i]<scan.range_max){
        pt.x = scan.ranges[i]*cos(scan.angle_min+i*scan.angle_increment);
        pt.y = scan.ranges[i]*sin(scan.angle_min+i*scan.angle_increment);
        
        endPoints.push_back(transMatrix*pt);
      }
    }
  // Grow map if needed
    for (unsigned int i=0; i<endPoints.size(); i++){
      while (fabs(endPoints[i].x)>cellSize*grid.cols/2.0 
               || fabs(endPoints[i].y)>cellSize*grid.rows/2.0 ){
        grow();
      }
    }
  // Add the info
    vector<cv::Point> end_cells;
    cv::Point curr_cell=pt2cell(cv::Point2f(curr_x, curr_y));
    for (unsigned int i=0; i<endPoints.size(); i++){
      end_cells.push_back(pt2cell(endPoints[i]));
      cv::LineIterator it(grid, curr_cell, end_cells[i]);
      for (int j=0; j<it.count; j++, ++it){
        if (*(*it)>254){
          *(*it) = 255;
        } else {
          *(*it) += 1;
        }
      }
    }
    while (end_cells.size()){
      if (grid.at<unsigned char>(end_cells.back())<10){
        grid.at<unsigned char>(end_cells.back()) = 0;
      } else {
        grid.at<unsigned char>(end_cells.back()) -= 10;
      }
      end_cells.pop_back();
    }
  // Show map in window if asked for
    if (disp){
      cv::Mat newgrid;
      grid.copyTo(newgrid);
      circle(newgrid, curr_cell, 3, 0);
      cv::Point2f curr_direction;
      //curr_direction.x = (int)(cos(curr_theta)*3.0+0.5);
      //curr_direction.y = (int)(sin(curr_theta)*3.0+0.5);
      curr_direction.x = 0.5;
      curr_direction.y = 0.0;
      curr_direction = transMatrix*curr_direction;
      line(newgrid, curr_cell,pt2cell(curr_direction),0);
      displayMatrix = cv::Mat::zeros(640,640,CV_8UC1);
      cv::resize(newgrid, displayMatrix,displayMatrix.size());
      cv::imshow("grid",displayMatrix);
      return cv::waitKey(3);
    }
    return -1; 
}

void EviGrid::saveGrid(string filename){
  ROS_INFO("saving grid");
  imwrite(filename,grid);
}

cv::Point EviGrid::pt2cell(cv::Point2f pointfloat){
  cv::Point pt;
  pt.x = (int)(pointfloat.x/cellSize+0.5) + origin_x;
  pt.y = -(int)(pointfloat.y/cellSize+0.5) + origin_y;
  return pt;
}

cv::Point2f EviGrid::cellCenter(cv::Point cell){
  cv::Point2f pt;
  pt.x = (cell.x - origin_x) * cellSize;
  pt.y = -(cell.y - origin_y) * cellSize;
  return pt;
}

unsigned int EviGrid::getOriginX(){
  return origin_x;
}

unsigned int EviGrid::getOriginY(){
  return origin_y;
}

float EviGrid::getCellSize(){
  return cellSize;
}

/*void EviGrid::setGridSize(float size){
  cellSize = size;
}*/

cv::Mat EviGrid::getGrid(){
  return grid;
}

void EviGrid::setDisp(bool setting){
  disp = setting;
}

unsigned char EviGrid::valueAtCell(int x, int y){
  if(x>=0 && x<grid.cols && y>=0 && y<grid.cols){
    return grid.at<unsigned char>(y, x);
  }
  return 127;
}

unsigned char EviGrid::valueAtCell(cv::Point pt){
  return valueAtCell(pt.x, pt.y);
}

unsigned char EviGrid::valueAtPoint(cv::Point2f pt){
  return valueAtCell(pt2cell(pt));
}

/*
unsigned char EviGrid::valueAt(cv::Point2f pos){
  if(pos.x>0 && pos.x<grid.cols && pos.y>0 && pos.y<grid.cols){
    int x = pos.x;
    int y = pos.y;
    return valueAt(x, y);
  }
  return UNKNOWN;
}

bool EviGrid::isOccupied(int x, int y){
  if(x>0 && x<grid.cols && y>0 && y<grid.cols){
    return (valueAt(x, y) == OCCUPIED);
  }
  return false;
}

bool EviGrid::isOccupied(cv::Point2f pos){
  if(pos.x>0 && pos.x<grid.cols && pos.y>0 && pos.y<grid.cols){
    int x = pos.x;
    int y = pos.y;
    return isOccupied(x, y);  
  }
  return false;
}                             
                              
bool EviGrid::isKnown(int x, int y){    
  if(x>0 && x<grid.cols && y>0 && y<grid.cols){
    return valueAt(x, y) != UNKNOWN;    
  }
  return false;
}                                       

bool EviGrid::isKnown(cv::Point2f pos){
  if(pos.x>0 && pos.x<grid.cols && pos.y>0 && pos.y<grid.cols){
    int x = pos.x;
    int y = pos.y;
    return valueAt(x, y) != UNKNOWN;
  }
  return false;
}

unsigned char EviGrid::at(int x, int y){
  x = x+origin_x;
  y = y-origin_y;
  return valueAt(x, y);
}

unsigned char EviGrid::at(cv::Point2f pos){
  int x = pos.x+origin_x;
  int y = pos.y-origin_y;
  return valueAt(x, y);
}

bool EviGrid::occupiedAt(int x, int y){
  x = x+origin_x;
  y = y-origin_y;
  return isOccupied(x, y);
}

bool EviGrid::occupiedAt(cv::Point2f pos){
  int x = pos.x+origin_x;
  int y = pos.y-origin_y;
  return isOccupied(x, y);
}

bool EviGrid::knownAt(int x, int y){
  x = x+origin_x;
  y = y-origin_y;
  return isKnown(x, y); 
}

bool EviGrid::knownAt(cv::Point2f pos){
  int x = pos.x+origin_x;
  int y = pos.y-origin_y;
  return isKnown(x, y);
}
*/
