/*EviGrid: Evidence grid class written by Keliang He and Jackie Kay*/

#ifndef __OCPGRID_H__
#define __OCPGRID_H__

#include <ros/ros.h>
#include <opencv2/opencv.hpp>
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Pose.h>
#include <kdl/frames.hpp>
#include <opencv/highgui.h>
#include "transform2d.h"


class EviGrid{

  private: 
    // Position of the origin of the grid in the matrix
    unsigned int origin_x, origin_y; 
    float cellSize; // Dimension of each grid, in meters
    cv::Mat grid;
    cv::Mat displayMatrix;
    bool disp; // Control whether the grid is displayed at every update
    
    void grow(); // For resizing grid dimensions
    //void growSparse(unsigned int dir); //TODO for jackie: Memory conservative
    
    // Convert the position of a point in meters to the position on the grid
    cv::Point pt2cell(cv::Point2f pointfloat);
    cv::Point2f cellCenter(cv::Point cell);

  public:
    EviGrid(float size=0.1,
            int initial_height=50, int initial_width=50); // Constructor 
    EviGrid(EviGrid* old_grid); // Copy Constructor
    EviGrid(cv::Mat map, float size);
    ~EviGrid() {} // Destructor
    // For updating map
    int addInfo(sensor_msgs::LaserScan scan, geometry_msgs::PoseWithCovarianceStamped odom);
    int addInfo(sensor_msgs::LaserScan scan, nav_msgs::Odometry odom);
    int addInfo(sensor_msgs::LaserScan scan, geometry_msgs::Pose odom);
    unsigned int getOriginX();
    unsigned int getOriginY();
    float getCellSize();
    //void setGridSize(float size); /*TODO: size rescaling*/
    cv::Mat getGrid();
    void saveGrid(string filename);
    void setDisp(bool setting);
    //Cell accessors
    unsigned char valueAtCell(int x, int y);
    unsigned char valueAtCell(cv::Point pt);
    unsigned char valueAtPoint(cv::Point2f pt);
/*
    unsigned char valueAtGrid(cv::Point2f pos);
    bool occupiedAtGrid(int x, int y);
    bool occupiedAtGrid(cv::Point2f pos);
    bool knownAtGrid(int x, int y);
    bool knownAtGrid(cv::Point2f pos);
    unsigned char at(int x, int y);
    unsigned char at(cv::Point2f pos);
    bool occupiedAt(int x, int y);
    bool occupiedAt(cv::Point2f pos);
    bool knownAt(int x, int y);
    bool knownAt(cv::Point2f pos);
*/
};
#endif 
