#include <ros/ros.h>
#include "EviGrid.h"
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <nav_msgs/Odometry.h>

EviGrid m_grid;// = EviGrid(0.05, 50, 50);
/*nav_msgs::Odometry*/ geometry_msgs::PoseWithCovarianceStamped odom;
bool has_odom = false;

void odomCallback(/*nav_msgs::Odometry*/  geometry_msgs::PoseWithCovarianceStamped odom_msg){
  odom = odom_msg;
  has_odom = true;
}

void laserCallback(sensor_msgs::LaserScan laser_msg){
  int keyInput = -1;
  string name;
  if (has_odom){
    keyInput = m_grid.addInfo(laser_msg, odom);
  }
  switch (keyInput){
    case 27:
      cout<<"Enter name of file: ";
      cin>>name;
      cout<<endl;
      m_grid.saveGrid("data/"+name+".png");
    break;
  }
}


int main (int argc, char** argv){
  ros::init(argc, argv, "gridTester");
  ros::NodeHandle nh;
  if (argc>1){
    cv::Mat grid = cv::imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
    if (grid.data!=NULL){
      m_grid = EviGrid(grid,0.05);
    }
    else {
      m_grid = EviGrid(0.05, 50, 50);
    }
  }
  else { m_grid = EviGrid(0.05, 50, 50);}
  m_grid.setDisp(true);
  ros::Subscriber odomSub = nh.subscribe("robot_pose_ekf/odom_combined",1000,odomCallback);
  //ros::Subscriber odomSub = nh.subscribe("ceiling_cat/odom_out",1000,odomCallback);
  ros::Subscriber laserSub = nh.subscribe("scan",1000,laserCallback);
  ros::spin();
  return 0;
  
}
