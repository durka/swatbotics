#include <ros/ros.h>
#include "OcpGrid.h"
#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>

OcpGrid m_grid = OcpGrid(0.05, 50, 50);
geometry_msgs::PoseWithCovarianceStamped odom;
bool has_odom = false;

void odomCallback(geometry_msgs::PoseWithCovarianceStamped odom_msg){
  odom = odom_msg;
  has_odom = true;
}

void laserCallback(sensor_msgs::LaserScan laser_msg){
  if (has_odom){
    m_grid.addInfo(laser_msg, odom);
  }
}


int main (int argc, char** argv){
  ros::init(argc, argv, "gridTester");
  ros::NodeHandle nh;
  m_grid.setDisp(true);
  ros::Subscriber odomSub = nh.subscribe("robot_pose_ekf/odom_combined",1000,odomCallback);
  ros::Subscriber laserSub = nh.subscribe("scan",1000,laserCallback);
  ros::spin();
  return 0;
  
}
